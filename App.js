import React, { useEffect, useRef } from 'react';
import {Alert, AsyncStorage, Vibration} from "react-native";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import * as Notifications from 'expo-notifications';
import moment from "moment";
import Constants from 'expo-constants';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {ApolloProvider} from '@apollo/react-hooks';
import * as RootNavigation from './utils/RootNavigation';
import LoginScreen from './screens/LoginScreen';
import PreRequisitosScreen from "./screens/PreRequisitosScreen";
import CadastroScreen from "./screens/CadastroScreen";
import MainScreen from "./screens/MainScreen";
import EditarUsoConflitoScreen from "./screens/EditarUsoConflitoScreen";
import WaitingScreen from "./screens/WaitingScreen";
import * as Sentry from 'sentry-expo';
import client, {
    AuthContext,
    persisteSettings,
    carregaCurrentUser,
    persisteCurrentUser,
    carregaListaTerritorios,
    sair,
    serverLogin,
    setCachedToken,
    LOGIN_ERROR
} from "./db/api";
import {createAnexosDir, limpaTodosAnexos} from "./db/arquivos";
import colors from "./assets/colors";
import STATUS from './bases/status';
import MENSAGEM from './bases/mensagem';
import TELAS from './bases/telas';

const RootStack = createStackNavigator();

const theme = {
    ...DefaultTheme,
    roundness: 4,
    colors: {
        ...DefaultTheme.colors,
        primary: colors.primary,
        accent: colors.secondary,
        background: colors.background,
        text: colors.text,
    },
};

Notifications.setNotificationHandler({
    handleNotification: async () => ({
      shouldShowAlert: true,
        shouldPlaySound: false,
        shouldSetBadge: false,
    }),
});

export default function App() {
    useEffect(() => {
        Sentry.init({
            dsn: Constants.manifest.extra.sentryDns,
            enableInExpoDevelopment: true,
            debug:true
        });
      }, []);

    //Fonte para entender o fluxo de autenticação: https://reactnavigation.org/docs/auth-flow
    const [state, dispatch] = React.useReducer(
        (prevState, action) => {
            switch (action.type) {
                case 'RESTORE_TOKEN':
                    return {
                        ...prevState,
                        userToken: action.token,
                        isLoading: false,
                    };
                case 'SIGN_IN':
                    return {
                        ...prevState,
                        isSignout: false,
                        userToken: action.token,
                        isLoading: false
                    };
                case 'IS_SIGNING_IN':
                    return {
                        ...prevState,
                        isSignout: false,
                        userToken: null,
                        isLoading: true
                    };
                case 'SIGN_OUT':
                    return {
                        ...prevState,
                        isSignout: true,
                        userToken: null,
                    };
                case 'SIGN_IN_ERROR':
                    return {
                        ...prevState,
                        userToken: null,
                        isLoading: false
                    };
            }
        },
        {
            isLoading: true,
            isSignout: false,
            userToken: null,
        }
    );

    const notificationSubscription = useRef(null);

    const handleNotification = notification => {
        // Vibration.vibrate();
        /*DEBUG*/ //console.log('handleNotification')
        /*DEBUG*/ //console.log(notification.request.content.data);
        if (Platform.OS === 'android') {
            Notifications.dismissAllNotificationsAsync();
        } else {
            Notifications.setBadgeCountAsync(0);
        }
        carregaCurrentUser().then(currentUser => {
            for (let mensagem of currentUser.currentTerritorio.mensagens) {
                if (notification.request.content.data.id == mensagem.id) {
                    return;
                }
            }
            if (notification.isMultiple) {
                setTimeout(() => {
                    RootNavigation.navigate('Home', {tela: TELAS.MAPA, gotNotification: {fetchPolicy: 'network-only'}});
                }, 1500);
                return;
            }
            const data = notification.request.content.data;
            if (parseInt(data.territorioId) == parseInt(currentUser.currentTerritorio.id)) {
                const newMensagem = {
                    ...MENSAGEM,
                    id: data.id,
                    texto: data.texto,
                    extra: data,
                    dataEnvio: data.dataEnvio ? new Date(moment(data.dataEnvio).valueOf()) : '',
                    dataRecebimento: new Date(),
                    originIsDevice: data.originIsDevice
                }
                /*DEBUG*/ //console.log('Notification message', newMensagem);
                const newCurrentTerritorio = {
                    ...currentUser.currentTerritorio,
                    mensagens: [
                        ...currentUser.currentTerritorio.mensagens,
                        newMensagem
                    ]
                };
                persisteCurrentUser({currentTerritorio: newCurrentTerritorio}).then(res => {
                    setTimeout(() => {
                        RootNavigation.navigate('Home', {tela: TELAS.MAPA, gotNotification: data});
                    }, 1500);
                });
            } else {
                setTimeout(() => {
                    RootNavigation.navigate('Home', {tela: TELAS.MAPA, gotNotification: data});
                }, 1500);
            }
        });
    };

    const initNotification = async () => {
        let pushToken;
        const { status: existingStatus } = await Notifications.getPermissionsAsync();
        let finalStatus = existingStatus;
        if (existingStatus !== 'granted') {
            const { status } = await Notifications.requestPermissionsAsync();
            finalStatus = status;
        }
        if (finalStatus !== 'granted') {
            console.log('Não há permissões para receber push notifications');
            return;
        }

        if (Constants.isDevice) {
            pushToken = (await Notifications.getExpoPushTokenAsync()).data;
            await AsyncStorage.setItem('pushToken', pushToken);
            /*DEBUG*/ //console.log("Device token", pushToken);

            if (Platform.OS === 'android') {
                Notifications.setNotificationChannelAsync('default', {
                    name: 'default',
                    importance: Notifications.AndroidImportance.MAX
                });
            }
        } else {
            console.log('Erro ao tentar pegar o pushToken. Tem que ser um dispositivo físico!');
        }

        return pushToken;
    };

    //Roda só na primeira vez
    React.useEffect(() => {
        // Fetch the token from storage then navigate to our appropriate place
        /*DEBUG*/ //console.log('App rendered', notificationSubscription.current);

        const initAsync = async () => {
            await createAnexosDir();

            await initNotification();
            notificationSubscription.current = {
                foregrounded: Notifications.addNotificationReceivedListener(handleNotification),
                backgrounded: Notifications.addNotificationResponseReceivedListener(response => {
                    // console.log('backgrounded fired', response);
                })
            };

            let userToken;
            try {
                userToken = await AsyncStorage.getItem('token');
                /*DEBUG*/ //console.log({userToken});
                dispatch({type: 'RESTORE_TOKEN', token: userToken});
            } catch (e) {
                console.log('Restoring token failed', e);
            }
        };

        initAsync();

        return () => {
            Notifications.removeNotificationSubscription(notificationSubscription.current.foregrounded);
            Notifications.removeNotificationSubscription(notificationSubscription.current.backgrounded);
            /*DEBUG*/ //console.log('Exited App', notificationSubscription.current);
        };
    }, []);

    const authContext = React.useMemo(
        () => ({
            signIn: async data => {
                dispatch({type: 'IS_SIGNING_IN'});
                /*DEBUG*/ //console.log("signIn");

                // In a production app, we need to send some data (usually username, password) to server and get a token
                // We will also need to handle errors if sign in failed
                // After getting token, we need to persist the token using `AsyncStorage`
                // In the example, we'll use a dummy token
                const onLoginSuccess = async (userToken) => {
                    await AsyncStorage.setItem('token', userToken);

                    await limpaTodosAnexos();
                    /*DEBUG*/ //console.log('Clears all local data');

                    try {
                        const currentUser = await carregaCurrentUser({fetchPolicy: 'network-only'});
                        dispatch({type: 'SIGN_IN', token: userToken});
                    } catch (error) {
                            console.log("LOGIN: error in getting user info", error);
                            // TODO: Tratar este erro!
                    }
                };

                const onLoginError = (errorCode) => {
                    let actions, title, message;
                    console.log("onLoginError", errorCode);
                    switch (errorCode) {
                        case LOGIN_ERROR.NO_USER_AND_NO_PASSWORD:
                            title = "Telefone em branco";
                            message = "Você precisa fornecer o seu telefone para entrar ou se cadastrar";
                            actions = [
                                {
                                    text: 'OK', onPress: () => {
                                        data.navigation.navigate('Login', {username: data.username, password: data.password});
                                    }
                                }
                            ]
                            break;
                        case LOGIN_ERROR.NO_PASSWORD:
                            title = "Cadastre-se no Tô no mapa";
                            message = "Se você nunca usou o app Tô no mapa, cadastre-se! Deseja se cadastrar?";
                            actions = [
                                {
                                    text: 'Não', onPress: () => {
                                        data.navigation.navigate('Login', {
                                            username: data.username,
                                            password: data.password
                                        });
                                    }
                                },
                                {
                                    text: 'Sim, quero me cadastrar', onPress: () => {
                                        data.navigation.navigate('Cadastro', {username: data.username});
                                    }
                                },
                            ]
                            break;
                        case LOGIN_ERROR.USER_NOT_FOUND:
                            title = "Telefone ou código não encontrado";
                            message = "Não foi possível entrar com este telefone e código de segurança. Se você não tem o código de segurança, cadastre-se! Se tem o código, por favor tente novamente";
                            actions = [
                                {
                                    text: 'Quero tentar de novo', onPress: () => {
                                        data.navigation.navigate('Login', {
                                            username: data.username,
                                            password: data.password
                                        });
                                    }
                                },
                                {
                                    text: 'Sim, quero me cadastrar', onPress: () => {
                                        data.navigation.navigate('Cadastro', {username: data.username});
                                    }
                                },
                            ]
                            break;
                        case LOGIN_ERROR.MISFORMATTED_USER:
                            title = "Número de telefone mal formatado";
                            message = "Você digitou o número corretamente? Por favor verifique.";
                            actions = [
                                {
                                    text: 'OK', onPress: () => {
                                        data.navigation.navigate('Login', {username: data.username, password: data.password});
                                    }
                                }
                            ]
                            break;
                        case LOGIN_ERROR.NETWORK:
                            title = "Erro de rede";
                            message = "Não foi possível conectar. Verifique sua conexão com a Internet. Caso ela esteja boa, pode estar acontecendo algum problema em nossos servidores; neste caso, por favor tente novamente mais tarde.";
                            actions = [
                                {
                                    text: 'OK', onPress: () => {
                                        data.navigation.navigate('Login', {username: data.username, password: data.password});
                                    }
                                }
                            ]
                            break;
                    };
                    dispatch({type: 'SIGN_IN_ERROR'});
                    Alert.alert(
                        title,
                        message,
                        actions,
                        {cancelable: false}
                    );
                };
                serverLogin(data.username, data.password, onLoginSuccess, onLoginError);
            },
            signOut: () => {
                sair();
                dispatch({type: 'SIGN_OUT'})
            },
            signUp: async userToken => {
                /*DEBUG*/ //console.log("signUp");
                await AsyncStorage.setItem('token', userToken);
                setCachedToken(userToken);
                dispatch({type: 'SIGN_IN', token: userToken});
            },
        }),
        []
    );

    return (
        <AuthContext.Provider value={authContext}>
        <ApolloProvider client={client}>
        <PaperProvider theme={theme}>
        <NavigationContainer ref={RootNavigation.navigationRef} onReady={() => {/*console.log('ooooi');*/}}>
        <RootStack.Navigator
        screenOptions={{
            headerShown: false,
        }}>
            {state.isLoading &&
                <RootStack.Screen name="Waiting" component={WaitingScreen}
                options={{headerShown: false}}/>
            }
            {!state.isLoading && state.userToken == null &&
                <React.Fragment>
                    <RootStack.Screen name="Login" component={LoginScreen}/>
                    <RootStack.Screen name="PreRequisitos" component={PreRequisitosScreen}/>
                    <RootStack.Screen name="Cadastro" component={CadastroScreen}/>
                </React.Fragment>
            }
            {!state.isLoading && state.userToken &&
                <React.Fragment>
                    <RootStack.Screen name="Main" component={MainScreen}/>
                    <RootStack.Screen name="EditarUsoConflito" component={EditarUsoConflitoScreen}/>
                    <RootStack.Screen name="CadastroTerritorio" component={CadastroScreen}/>
                </React.Fragment>
            }
        </RootStack.Navigator>
        </NavigationContainer>
        </PaperProvider>
        </ApolloProvider>
        </AuthContext.Provider>
    );
}
