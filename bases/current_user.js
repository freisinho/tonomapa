const CURRENT_USER = {
    __typename: 'Usuaria',
    id: null,
    username: null,
    fullName: null,
    codigoUsoCelular: null,
    cpf: null,
    email: null,
    organizacoes: [],
    currentTerritorio: {
        __typename: 'Territorio',
        id: null,
        nome: null,
        poligono: {
            type: 'MultiPolygon',
            coordinates: []
        },
        publico: false,
        enviadoPtt: false,
        anoFundacao: null,
        qtdeFamilias: null,
        statusId: null,
        tiposComunidade: [],
        municipioReferenciaId: null,
        conflitos: [],
        areasDeUso: [],
        anexos: [],
        mensagens: [],
        cep: null,
        descricaoAcesso: null,
        descricaoAcessoPrivacidade: false,
        zonaLocalizacao: null,
        outraZonaLocalizacao: null,
        autoIdentificacao: null,
        segmentoId: null,
        historiaDescricao: null,
        ecNomeContato: null,
        ecLogradouro: null,
        ecNumero: null,
        ecComplemento: null,
        ecBairro: null,
        ecCep: null,
        ecCaixaPostal: null,
        ecMunicipioId: null,
        ecEmail: null,
        ecTelefone: null
    }
};

export const CURRENT_USER_DADOS_PTT = {
    email: null,
    currentTerritorio: {
        cep: null,
        descricaoAcesso: null,
        descricaoAcessoPrivacidade: false,
        zonaLocalizacao: null,
        outraZonaLocalizacao: null,
        autoIdentificacao: null,
        segmentoId: null,
        historiaDescricao: null,
        ecNomeContato: null,
        ecLogradouro: null,
        ecNumero: null,
        ecComplemento: null,
        ecBairro: null,
        ecCep: null,
        ecCaixaPostal: null,
        ecMunicipioId: null,
        ecEmail: null,
        ecTelefone: null
    }
};

export default CURRENT_USER;
