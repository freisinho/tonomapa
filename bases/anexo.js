const ANEXO = {
    __typename: 'Anexo',
    id: null,
    nome: null,
    descricao: null,
    tipoAnexo: null,
    mimetype: null,
    arquivo: null,
    thumb: null,
    criacao: null,
    publico: false
};

export const TIPOS_ANEXO = {
    ANEXO: 'ANEXO',
    FONTE_INFORMACAO: 'FONTE_INFORMACAO',
    ICONE: 'ICONE'
};

export const isAnexoPTT = (tipoAnexo) => {
    if (!tipoAnexo) {
        return false;
    }
    return TIPOS_ANEXO.hasOwnProperty(tipoAnexo);
};

export default ANEXO;
