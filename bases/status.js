import {Alert} from 'react';
import colors from '../assets/colors';
import tipos from './tipos';
import Constants from 'expo-constants';

export const STATUS = {
    // NADA_PREENCHIDO: 1,
    // PREENCHIDO: 6,
    // TELEFONE_NAO_REGISTRADO: 7,
    EM_PREENCHIMENTO: 2,
    ENVIADO_PENDENTE: 3,
    OK_TONOMAPA: 4,
    REVISADO_COM_PENDENCIAS: 8,
    REJEITADO_TONOMAPA: 9,
    OK_PTT: 5,
    EM_PREENCHIMENTO_PTT: 10,
    ENVIO_PTT_NAFILA: 11,
    ENVIO_ARQUIVOS_PTT_NAFILA: 12
};
const tiposStatus = tipos.data.territorioStatus;

export const pegaMensagem = (status) => {
    let mensagem = {};
    for (let i = 0; i < tiposStatus.length; i++) {
        if (tiposStatus[i].id == status) {
            mensagem = tiposStatus[i];
            break;
        }
    }
    return mensagem;
}

export const buildStatusMessage = (status) => {
    let statusMessage = pegaMensagem(status);
    switch (status) {
        case null:
            statusMessage = {
                ...statusMessage,
                icon: "circle",
                color: colors.danger,
            };
            break;
        case STATUS.REVISADO_COM_PENDENCIAS:
            statusMessage = {
                ...statusMessage,
                icon: "circle-slice-2",
                color: colors.warning,
            };
            break;
        case STATUS.EM_PREENCHIMENTO:
        case STATUS.EM_PREENCHIMENTO_PTT:
            statusMessage = {
                ...statusMessage,
                icon: "circle-slice-4",
                color: colors.warning,
            };
            break;
        case STATUS.ENVIADO_PENDENTE:
        case STATUS.ENVIO_PTT_NAFILA:
        case STATUS.ENVIO_ARQUIVOS_PTT_NAFILA:
            statusMessage = {
                ...statusMessage,
                icon: "circle-slice-6",
                color: colors.warning,
            };
            break;
        case STATUS.OK_TONOMAPA:
            statusMessage = {
                ...statusMessage,
                icon: "checkbox-marked-circle",
                color: colors.primary,
            };
            break;
        case STATUS.REJEITADO_TONOMAPA:
            statusMessage = {
                ...statusMessage,
                icon: "close-circle",
                color: colors.danger,
            };
            break;
        case STATUS.OK_PTT:
            statusMessage = {
                ...statusMessage,
                icon: "checkbox-marked-circle",
                color: colors.primary,
            };
            break;
    }
    return statusMessage;
};

export const alertStatusId = (status, statusChanged = false) => {
    const statusMessage = buildStatusMessage(status);
    const title = (statusChanged)
        ? `Atenção: ${statusMessage.nome}`
        : statusMessage.nome;

    let mensagem = statusMessage.mensagem.replace(/\\n/g, '\n');

    // TODO: Remove this when PTT is definitive
    if (Constants.manifest.extra.isPTTDisabled && status == STATUS.OK_TONOMAPA) {
        mensagem = `Parabéns, sua comunidade foi aprovada e já está na plataforma Tô no Mapa com sucesso!\n\nObservação: Os dados da comunidade não podem mais ser alterados agora. Se quiser mudar alguma informação, fale com a gente pelo site tonomapa.org.br`;
    }

    const body = (statusChanged)
        ? mensagem
        : mensagem;

    return {title: title, body: body, urlReferencia: statusMessage.urlReferencia};
};

export const isEnviarEnabled = (currentUser) => {
    if (!currentUser || !currentUser.currentTerritorio) {
        return true;
    }

    return ![STATUS.REJEITADO_TONOMAPA, STATUS.OK_TONOMAPA, STATUS.ENVIADO_PENDENTE, STATUS.OK_PTT, STATUS.ENVIO_PTT_NAFILA, STATUS.ENVIO_ARQUIVOS_PTT_NAFILA].includes(currentUser.currentTerritorio.statusId);
};

export const isCurrentTerritorioEditable = (currentUser) => {
    if (!currentUser || !currentUser.currentTerritorio) {
        return true;
    }

    return [STATUS.EM_PREENCHIMENTO, STATUS.REVISADO_COM_PENDENCIAS, STATUS.EM_PREENCHIMENTO_PTT].includes(currentUser.currentTerritorio.statusId);
};

export const isPTTMode = (currentUser) => {
    if (!currentUser || !currentUser.currentTerritorio) {
        return false;
    }

    return [STATUS.EM_PREENCHIMENTO_PTT, STATUS.ENVIO_PTT_NAFILA, STATUS.ENVIO_ARQUIVOS_PTT_NAFILA].includes(currentUser.currentTerritorio.statusId);
};

export const isEnviarPTTEnabled = (currentUser) => {
    if (!currentUser || !currentUser.currentTerritorio || currentUser.currentTerritorio.enviadoPtt == true || Constants.manifest.extra.isPTTDisabled) {
        return false;
    }

    return (
        (isPTTMode(currentUser) && isEnviarEnabled(currentUser)) ||
        (currentUser.currentTerritorio.statusId == STATUS.OK_TONOMAPA)
    );
}

export default STATUS;
