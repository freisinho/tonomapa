const DADOS_BASICOS_TERRITORIO = {
    nome: {type: 'None'},
    poligono: {type: 'None'},
    publico: {type: 'Boolean'},
    enviadoPtt: {type: 'Boolean'},
    anoFundacao: {type: 'Number'},
    qtdeFamilias: {type: 'Number'},
    statusId: {type: 'Number'},
    tiposComunidade: {type: 'Array', __typename: 'tipoComunidade'},
    municipioReferenciaId: {type: 'String'},
    cep: {type: 'String'},
    descricaoAcesso: {type: 'String'},
    descricaoAcessoPrivacidade: {type: 'Boolean'},
    zonaLocalizacao: {type: 'String'},
    outraZonaLocalizacao: {type: 'String'},
    autoIdentificacao: {type: 'String'},
    segmentoId: {type: 'String'},
    historiaDescricao: {type: 'String'},
    ecNomeContato: {type: 'String'},
    ecLogradouro: {type: 'String'},
    ecNumero: {type: 'String'},
    ecComplemento: {type: 'String'},
    ecBairro: {type: 'String'},
    ecCep: {type: 'String'},
    ecCaixaPostal: {type: 'String'},
    ecMunicipioId: {type: 'String'},
    ecEmail: {type: 'String'},
    ecTelefone: {type: 'String'},
};

export const DADOS_BASICOS_TERRITORIO_TONOMAPA = {
    nome: {type: 'None'},
    anoFundacao: {type: 'Number'},
    qtdeFamilias: {type: 'Number'},
    tiposComunidade: {type: 'Array', __typename: 'tipoComunidade'},
    municipioReferenciaId: {type: 'String'},
};

export const adjustTerritorioFieldTypes = (currentTerritorio) => {
    for (let campo of Object.keys(DADOS_BASICOS_TERRITORIO)) {
        let valor;
        switch(DADOS_BASICOS_TERRITORIO[campo].type) {
            case 'Number':
                valor = currentTerritorio[campo]
                    ? parseInt(currentTerritorio[campo])
                    : null;
                break;
            case 'String':
                valor = currentTerritorio[campo]
                    ? currentTerritorio[campo].toString()
                    : null;
                break;
            case 'Array':
                valor = [];
                for (let id of currentTerritorio[campo]) {
                    if (typeof id == 'object') {
                        valor.push(id);
                        continue;
                    }
                    const item = {
                        id: id,
                        __typename: DADOS_BASICOS_TERRITORIO[campo].__typename
                    }
                    valor.push(item);
                }
                break;
            case 'Boolean':
            case 'None':
            default:
                valor = null;
                break;
        }
        if (valor) {
            currentTerritorio[campo] = valor;
        }
    }
    return currentTerritorio;
}

export default DADOS_BASICOS_TERRITORIO;
