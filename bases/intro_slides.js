import React from 'react';
import {View, Text, StyleSheet, Image, Dimensions, PixelRatio} from 'react-native';
import {Avatar} from 'react-native-paper';
import Balloon from "react-native-balloon";
import colors from "../assets/colors";
import metrics from "../utils/metrics";

export const slideStyles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'blue',
    },
    image: {

    },
    text: {
        color: colors.white,
        fontSize: metrics.tenWidth*1.8,
        textAlign: 'left',
    },
    title: {
        fontSize: metrics.tenWidth*2.2,
        color: 'white',
        textAlign: 'center',
        marginBottom: metrics.tenWidth*2
    },
});

const DEFAULT_SLIDE = {
    title: '',
    backgroundColor: "#000",
    image: require('../assets/intro/tour.jpg'),
    triangleDirection: 'bottom',
    triangleOffset: '10%',
    balloonWidth: metrics.tenWidth*27,
    balloonTextColor: colors.white,
    balloonBackgroundColor: colors.primary,

};

export const SLIDES = [
    {
      key: "1",
      ...DEFAULT_SLIDE,
      text: 'Com o Tô no Mapa, sua comunidade pode se mapear e identificar usos e conflitos',
      triangleDirection: 'bottom',
      balloonStyles: {position:'absolute', top: 0, left: 0},
      triangleOffset: '48%',
      balloonBackgroundColor: colors.warning
    },
    {
        key: "2",
        ...DEFAULT_SLIDE,
        text: 'Aqui você pode colocar informações da comunidade, como o nome, tipo de comunidade, número de famílias, data de fundação, etc.',
        balloonStyles: {position:'absolute', bottom: metrics.tenWidth*3.5, left: 0},
        triangleOffset: '9%',
    },
    {
        key: "3",
        ...DEFAULT_SLIDE,
        text: 'Este botão é para você mapear a comunidade! Veja como fazer...',
        balloonStyles: {position:'absolute', bottom: metrics.tenWidth*3.5, left: 0},
        balloonBackgroundColor: colors.warning,
        triangleOffset: '28%',
    },
    {
        key: "4",
        ...DEFAULT_SLIDE,
        text: 'Mapear o território é fácil. Basta tocar na tela! Precisa pelo menos 3 pontos.',
        balloonStyles: {position:'absolute', top: metrics.tenWidth*27, right: 0},
        balloonBackgroundColor: colors.background,
        balloonTextColor: colors.text,
        triangleOffset: '80%',
        triangleDirection: 'top',
        image: require('../assets/intro/tourMapearTerritorioEditando.jpg'),
        balloonWidth: metrics.tenWidth*25
    },
    {
        key: "5",
        ...DEFAULT_SLIDE,
        text: 'Ou você pode usar a localização do celular (GPS)...',
        balloonStyles: {position:'absolute', top: metrics.tenWidth*3.5, right: -metrics.tenWidth*2.5},
        balloonBackgroundColor: colors.primary,
        triangleOffset: '79%',
        triangleDirection: 'top',
        image: require('../assets/intro/tourMapearTerritorioGPS.jpg'),
        balloonWidth: metrics.tenWidth*24
    },
    /*{
        key: "6",
        ...DEFAULT_SLIDE,
        text: 'Basta se mover e clicar na sua posição e adicionar o ponto!',
        balloonStyles: {position:'absolute', top: 90, right: -40},
        balloonBackgroundColor: colors.primary,
        triangleOffset: '50%',
        triangleDirection: 'left',
        image: require('../assets/intro/tourMapearTerritorio.jpg'),
        balloonWidth: 130
    },*/
    {
        key: "7",
        ...DEFAULT_SLIDE,
        text: 'Aqui é onde você registra locais de uso e de conflito',
        balloonStyles: {position:'absolute', bottom: metrics.tenWidth*3.5, left: 0},
        balloonBackgroundColor: colors.warning,
        triangleOffset: '48%',
    },
    {
        key: "8",
        ...DEFAULT_SLIDE,
        text: 'Você também pode mandar fotos e documentos sobre a comunidade!',
        balloonStyles: {position:'absolute', bottom: metrics.tenWidth*3.5, left: 0},
        balloonBackgroundColor: colors.primary,
        triangleOffset: '67%',
    },
    {
        key: "9",
        ...DEFAULT_SLIDE,
        text: 'No menu lateral você vê estas e outras opções',
        balloonStyles: {position:'absolute', bottom: 0, left: -metrics.tenWidth*4},
        balloonBackgroundColor: colors.warning,
        triangleDirection: 'right',
        triangleOffset: '80%',
        image: require('../assets/intro/tourMenuLateral.jpg'),
        balloonWidth: metrics.tenWidth*10
    },
    {
        key: "10",
        ...DEFAULT_SLIDE,
        text: 'Quando terminar, não se esqueça de enviar os dados clicando em "Enviar ao Tô no Mapa!!"',
        balloonStyles: {position:'absolute', top: metrics.tenWidth*33, left: 0},
        balloonBackgroundColor: colors.primary,
        triangleDirection: 'top',
        triangleOffset: '60%',
        image: require('../assets/intro/tourMenuLateral.jpg'),
    },
    {
        key: "11",
        ...DEFAULT_SLIDE,
        text: 'Acompanhe a situação das informações cadastradas da comunidade e enviadas ao Tô no Mapa. Basta clicar aqui para saber se o mapeamento da sua comunidade foi aprovado!',
        triangleDirection: 'top',
        balloonStyles: {position:'absolute', top: metrics.tenWidth*4, left: -metrics.tenWidth},
        balloonBackgroundColor: colors.background,
        balloonTextColor: colors.text,
    },
];

const {windowWidth, windowHeight} = Dimensions.get('window');

// ratio is 720x1197 -> 0,6332
const imageDimensions = {
    width: metrics.tenWidth*30,
    height: metrics.tenWidth*47.4
};
/*
// TODO: Make higher resolution images
if (windowWidth >= 500 && windowHeight >= 760) {
    imageDimensions.width = 450;
    imageDimensions.height = 705;
}
*/

export const SlideWidget = ({type}) => {
    let icon, label;
    switch(type) {
        case 'Next':
            icon = 'arrow-right-thick';
            break;
        case 'Prev':
            icon = 'arrow-left-thick';
            break;
        case 'Done':
            icon = 'check';
            break;
        case 'Skip':
            icon = '';
            label = 'Pular';
            break;
    }
    return (
        <View style={{ height:metrics.tenWidth * 2.6, flexDirection: 'row', alignContent: 'center', alignItems: 'center' }}>
            {icon == '' &&
                <Text >{label}</Text>
            }
            {icon &&
                <Avatar.Icon
                    size={metrics.tenWidth * 2.6}
                    color={colors.text}
                    style={{ backgroundColor: colors.white }}
                    icon={icon}
                />
            }
        </View>
    );
}

export const renderSlide = ({ item }) => {
    return (
        <View style={{...slideStyles.slide, backgroundColor: item.backgroundColor}}>
            <View style={{position: 'relative', ...imageDimensions}}>
                <Image source={item.image} style={imageDimensions}/>
                <View style={item.balloonStyles}>
                    <Balloon
                        borderColor="#999"
                        backgroundColor={item.balloonBackgroundColor}
                        width={item.balloonWidth}
                        borderWidth={metrics.tenWidth*0.4}
                        borderRadius={metrics.tenWidth*1.2}
                        triangleSize={metrics.tenWidth*1.5}
                        triangleOffset={item.triangleOffset}
                        triangleDirection={item.triangleDirection}
                        containerStyle={{padding: metrics.tenWidth*0.8}}
                    >
                        <Text style={{...slideStyles.text, color: item.balloonTextColor}}>{item.text}</Text>
                    </Balloon>
                </View>
            </View>
        </View>
    );
};

export const keyExtractor = (item) => item.title;
