export const ICONES = {
    USOS: {
        ROCA: require("../assets/images/usos/1/icone_uso_1.png"),
        CRIACAO_DE_ANIMAIS: require("../assets/images/usos/2/icone_uso_2.png"),
        PESCA: require("../assets/images/usos/3/icone_uso_3.png"),
        EXTRATIVISMO: require("../assets/images/usos/4/icone_uso_4.png"),
        TURISMO: require("../assets/images/usos/5/icone_uso_5.png"),
        CONSERVACAO: require("../assets/images/usos/6/icone_uso_6.png"),
        PRODUCAO_AGROECOLOGICA: require("../assets/images/usos/7/icone_uso_7.png"),
        REFLORESTAMENTO: require("../assets/images/usos/11/icone_uso_11.png"),
        OUTRO: require("../assets/images/usos/8/icone_uso_8.png")
    },
    CONFLITOS: {
        INVASAO: require("../assets/images/conflitos/1/icone_conflito_1.png"),
        GARIMPO: require("../assets/images/conflitos/2/icone_conflito_2.png"),
        TERRA: require("../assets/images/conflitos/3/icone_conflito_3.png"),
        QUEIMADAS_NAO_CONTROLADAS: require("../assets/images/conflitos/4/icone_conflito_4.png"),
        CONTAMINACAO_POR_AGROTOXICOS: require("../assets/images/conflitos/5/icone_conflito_5.png"),
        AGUA: require("../assets/images/conflitos/6/icone_conflito_6.png"),
        DESMATAMENTO: require("../assets/images/conflitos/8/icone_conflito_8.png"),
        MINERACAO: require("../assets/images/conflitos/9/icone_conflito_9.png"),
        OUTRO: require("../assets/images/conflitos/7/icone_conflito_7.png")
    }
};

export const geraIconeTipoUsoOuConflito = (tipo, id) => {
    switch (tipo + id.toString()) {
        case 'areaDeUso1':
            return ICONES.USOS.ROCA;
            break;
        case 'areaDeUso2':
            return ICONES.USOS.CRIACAO_DE_ANIMAIS;
            break;
        case 'areaDeUso3':
            return ICONES.USOS.PESCA;
            break;
        case 'areaDeUso4':
            return ICONES.USOS.EXTRATIVISMO;
            break;
        case 'areaDeUso5':
            return ICONES.USOS.TURISMO;
            break;
        case 'areaDeUso6':
            return ICONES.USOS.CONSERVACAO;
            break;
        case 'areaDeUso7':
            return ICONES.USOS.PRODUCAO_AGROECOLOGICA;
            break;
        case 'areaDeUso8':
            return ICONES.USOS.OUTRO;
            break;
        case 'areaDeUso11':
            return ICONES.USOS.REFLORESTAMENTO;
            break;
        case 'conflito1':
            return ICONES.CONFLITOS.INVASAO;
            break;
        case 'conflito2':
            return ICONES.CONFLITOS.GARIMPO;
            break;
        case 'conflito3':
            return ICONES.CONFLITOS.TERRA;
            break;
        case 'conflito4':
            return ICONES.CONFLITOS.QUEIMADAS_NAO_CONTROLADAS;
            break;
        case 'conflito5':
            return ICONES.CONFLITOS.CONTAMINACAO_POR_AGROTOXICOS;
            break;
        case 'conflito6':
            return ICONES.CONFLITOS.AGUA;
            break;
        case 'conflito7':
            return ICONES.CONFLITOS.OUTRO;
            break;
        case 'conflito8':
            return ICONES.CONFLITOS.DESMATAMENTO;
            break;
        case 'conflito9':
            return ICONES.CONFLITOS.MINERACAO;
            break;
    }
};

export default ICONES;
