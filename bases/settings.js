import {MAP_TYPES} from 'react-native-maps';
import STATUS from './status';
import {UNIDADES_AREA} from '../maps/mapsUtils';

const SETTINGS = {
    appIntro: true,
    useGPS: false,
    mapType: MAP_TYPES.HYBRID,
    editaPoligonos: true,
    appFiles: {
        anexos: {},
    },
    editing: null,
    territorios: {},
    unidadeArea: UNIDADES_AREA.HECTARE
};

export default SETTINGS;
