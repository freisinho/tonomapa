## Documentação do aplicativo Tô no Mapa

O Tô no Mapa é um aplicativo desenvolvido para que povos, comunidades tradicionais e agricultores familiares brasileiros realizem o automapeamento de seus territórios.

Uma ferramenta acessível e gratuita, construída a partir do diálogo entre diversas comunidades e organizações sociais.

Com mais esse instrumento político, você fortalece a sua luta por direitos territoriais ainda não reconhecidos. É a oportunidade de mostrar que você também está no mapa!

Esta é uma iniciativa do Instituto de Pesquisa Ambiental da Amazônia (IPAM) junto ao Instituto Sociedade, População e Natureza (ISPN), com apoio da Rede Cerrado.

### Índice

* 
