import colors from '../assets/colors';
import * as Layout from './layout';
import metrics from "../utils/metrics";

export const flatList = {
    margin: 0,
    padding: 0
};

export const itemSeparatorComponent = {
    borderColor: colors.warning,
    borderBottomWidth: 1,
    marginBottom: 20
};

export const item = {
    paddingTop: 0,
    paddingBottom: 20,
    paddingLeft: 16,
    paddingRight: 16,
    marginHorizontal: 8,
};

export const itemTitle = {
    ...Layout.bold,
    ...Layout.textParagraph,
    marginBottom: 0
};
export const itemDescricao = {
    ...Layout.italic
};
export const itemTamanho = {
    fontSize: metrics.tenWidth*1.2
};

export const itemImageWrapper = {
    height: 130,
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'stretch',
    marginBottom: 4
};

export const itemWithoutImageWrapper = {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
};

export const itemWithoutImageDetails = {
    flex: 1
};

export const itemIconWrapper = {
    marginRight: 20
};

export const formIconWrapper = {
    alignItems: 'flex-start',
    marginTop: 8
};

export const itemImage = {
    flex: 1
};

export const itemActions = {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 16
};

export const introTextWrapper = {
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 8,
    paddingRight: 8
};

export const introText = {
    ...Layout.textParagraph,
    ...Layout.italic,
    marginBottom: 0
}

export const fab = {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
    backgroundColor: colors.primary
};

export const playIcon = {
    position: 'absolute',
    right: 5,
    top: 0
};
