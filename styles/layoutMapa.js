import colors from '../assets/colors';
import {StyleSheet} from 'react-native';
import metrics from '../utils/metrics';
export const map = {
    ...StyleSheet.absoluteFillObject,
    flex: 1
};
export const appbarBottom = {
    position: 'absolute',
    height: metrics.tenWidth*5,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: "space-around",
    backgroundColor: colors.background
};
export const appbarBottomPTT = {
    ...appbarBottom,
    backgroundColor: colors.backgroundPTT
};
export const appbarBottomCircleEnviar = {
    position:'absolute',
    bottom: metrics.tenWidth * 1.7,
    right: metrics.tenWidth * 1.3
};
export const appbarBottomCircleEnviarPTT = {
    position:'absolute',
    bottom: metrics.tenWidth * 1.7,
    right: metrics.tenWidth * 0.5
};

export const mapearButtonsContainer = {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 0,
    paddingTop: 0,
    paddingBottom: metrics.tenWidth*0.8,
    paddingRight: metrics.tenWidth*0.8,
    paddingLeft: metrics.tenWidth*0.8,
};
export const mapearButtonsContainerRow = {
    flexDirection: 'row',
    flexGrow: 1,
    justifyContent: 'space-between',
    padding: metrics.tenWidth*0.8,
}
export const mapearButtons = {
    borderRadius: metrics.tenWidth*0.4,
    padding: 0,
};
export const mapearButtonsLeft = {
    ...mapearButtons,
    marginRight: metrics.tenWidth*0.4
};
export const mapearButtonsRight = {
    ...mapearButtons,
    marginLeft: metrics.tenWidth*0.4,
};
export const mapearButtonsMiddle = {
    ...mapearButtons,
    marginLeft: metrics.tenWidth*0.4,
    marginRight: metrics.tenWidth*0.4
};
export const mapearButtonsLabel = {
    marginLeft: metrics.tenWidth*3,
    alignSelf:'center',
    fontSize: metrics.tenWidth*1.6,
    color:colors.white,

    textTransform: "none"
};
export const avatarButtonCancel = {
    color: colors.text,
    backgroundColor: colors.background,
};
export const avatarButtonExit = {
    color: colors.text,
    backgroundColor: colors.white,
};
export const avatarButtonClean = {
    color: colors.text,
    backgroundColor: colors.white,
};
export const avatarButtonDone = {
    color: colors.primary,
    backgroundColor: colors.background,
};
export const avatarButtonAtivo = {
    color: colors.primary,
    backgroundColor: colors.background,
};
export const avatarButtonModoInativo = {
    color: colors.white,
    backgroundColor: colors.text,
};
export const avatarButtonModoAtivo = {
    color: colors.text,
    backgroundColor: colors.white,
};
export const mapearLogoImage = {
    alignSelf: 'center',
    width: metrics.tenWidth*8,
    height: metrics.tenWidth*8,
};
export const mapearStatusButton = {
};
export const mapearStatusButtonClaro = {
    backgroundColor: colors.white
};
