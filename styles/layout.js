import colors from '../assets/colors';
import metrics from "../utils/metrics";

export const italic = {
    fontStyle: 'italic'
};
export const bold = {
    fontWeight: 'bold'
};

export const container = {
    flex: 1,
    flexDirection: "column",
    justifyContent: 'flex-start',
};
export const containerCentered = {
    ...container,
    alignItems: 'center',
};
export const containerStretched = {
    ...container,
    alignItems: 'stretch'
};

export const icon = {
    height: metrics.tenWidth * 5
};

export const vertice = {
    height: metrics.tenWidth * 2,
    alignContent: 'center',
    alignItems: 'center',
    width: metrics.tenWidth * 2,
    borderRadius: metrics.tenWidth * 2,
    backgroundColor: 'white'
};

export const body = {
    paddingLeft: metrics.tenWidth*0.8,
    paddingRight: metrics.tenWidth*0.8,
};
export const bodyConfiguracoes = {
    ...body,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20
};
export const bodyFlexEnd = {
    ...body,
    flex: 1,
    justifyContent: 'flex-end'
};

export const innerBody = {
    paddingBottom: 100
};

export const topBar = {
    width: '100%'
};

export const appOverlay = {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: colors.appOverlay,
};
export const appOverlayOpaque = {
    ...appOverlay,
    backgroundColor: colors.appOverlayOpaque,
}

export const topBarTitle = {
    fontSize: metrics.tenWidth*1.6,
    textTransform: 'uppercase',
};

export const row = {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: metrics.tenWidth*0.8,
};
export const rowCenter = {
    ...row,
    justifyContent: 'center'
};
export const rowConfiguracoes = {
    ...row,
    paddingBottom: metrics.tenWidth*2
};
export const secaoConfiguracoes = {
    borderTopWidth: 1,
    borderColor: colors.divider,
    paddingTop: metrics.tenWidth*1.6,
    paddingBottom: metrics.tenWidth*1.2
};
export const rowConfiguracoesInner = {
    ...row,
    alignItems: 'baseline'
};
export const rowConfiguracoesComDesc = {
    paddingBottom: metrics.tenWidth*2
};
export const descConfiguracoes = {
    ...text,
    ...italic
};

export const rowAnexoForm = {
    ...row,
    marginTop: metrics.tenWidth*2,
    marginBottom: metrics.tenWidth*2
};

export const rowWrapper = {
    flexDirection: 'row',
    justifyContent: 'space-between',
};

export const col = {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
};

export const colLeft = {
    ...col,
    alignItems: 'flex-start'
};

export const buttonRight = {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: metrics.tenWidth*0.8,
};
export const buttonLeft = {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: metrics.tenWidth*0.8,
};
export const buttonPrimaryLabel = {
    color: colors.white
};
export const buttonCenter = {
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: metrics.tenWidth*3
};
export const buttonsBottomWrapperScroll = {
    marginTop: metrics.tenWidth*2
}
export const buttonsBottomWrapper = {
    flex:1,
    justifyContent:'flex-end',
    marginBottom: metrics.tenWidth*2
};

export const subRow = {
    ...row,
    paddingTop: 0,
    paddingLeft: metrics.tenWidth*0.8
};

export const alignCenter = {
    justifyContent: 'center'
};

export const drawer = {
    width: metrics.tenWidth*32
};
export const drawerLabel = {
    color: colors.text,
    fontSize: metrics.tenWidth*1.6
};
export const drawerHeader = {
    ...row,
    justifyContent: 'flex-start',
    paddingRight: metrics.tenWidth*1.2,
    paddingLeft: metrics.tenWidth*1.8,
    paddingTop: metrics.tenWidth*0.4,
    paddingBottom: metrics.tenWidth*0.8,
    borderBottomWidth: 1,
    borderColor: colors.divider
};
export const drawerHeaderLabel = {
    ...drawerLabel,
    ...bold,
    paddingLeft: metrics.tenWidth*3,
    flex: 1,
    flexWrap: 'wrap'
};
export const drawerLabelDisabled = {
    ...drawerLabel,
    color: colors.disabled
};
export const drawerIcon = {
    color: colors.text,
    fontSize: metrics.tenWidth*3.0
};
export const drawerIconDisabled = {
    ...drawerIcon,
    color: colors.disabled
};
export const drawerItem = {
    backgroundColor: 'transparen'
};
export const drawerItemSobre = {
    color: colors.text,
    fontSize: metrics.tenWidth*1.6
};

export const drawerEnviar = {
    backgroundColor: colors.warning
};
export const drawerEnviarLabel = {
    color: colors.white,
    fontSize: metrics.tenWidth*1.6
};
export const drawerEnviarIcon = {
    color: colors.white,
    fontSize: metrics.tenWidth*3.0
};
export const drawerLogoImage = {
    height: metrics.tenWidth*3,
    width: metrics.tenWidth*3,
};

export const text = {
    fontSize: metrics.tenWidth*1.6,
    color: colors.text,
    flexShrink: 1
};
export const textPeq = {
    ...text,
    fontSize: metrics.tenWidth*1.3,
};

export const textLeftFromSwitch = {
    ...text,
    flex: 0.8
};
export const switchRight = {
    flex: 0.2
};

export const textParagraph = {
    ...text,
    alignSelf: 'flex-start',
    marginBottom: metrics.tenWidth*0.8
};

export const calloutDescription = {
    ...text,
    fontSize: metrics.tenWidth*1.4
};
export const calloutDescriptionSub = {
    ...text,
    ...italic,
    alignSelf: 'flex-start',
    fontSize: metrics.tenWidth*1.2
};
export const calloutTitle = {
    fontSize: metrics.tenWidth*1.6,
    ...bold,
};
export const enviarTitle = {
    ...text,
    textTransform: 'uppercase',
    paddingBottom: metrics.tenWidth*1.2
}
export const calloutAction = {
    marginTop: metrics.tenWidth*0.8,
    marginBottom: metrics.tenWidth*0.8,
    paddingBottom: 0,
    fontSize: metrics.tenWidth*1.2,
    color: colors.action,
    textTransform: 'uppercase'
};
export const calloutActionCenter = {
    color: colors.action,
    alignSelf: 'center'
};
export const logoImageCadastro = {
    height: metrics.tenWidth*12,
    width: metrics.tenWidth*10,
    alignSelf: 'center'
};

export const statusBar = {
    width: '100%',
    alignItems: 'center',
    padding: metrics.tenWidth*0.8,
    backgroundColor: colors.warning
};
export const statusBarText = {
    color: colors.white,
    fontSize: metrics.tenWidth*1.6,
    paddingTop: metrics.tenWidth*0.6,
    paddingBottom: metrics.tenWidth*0.6
};
export const statusBarTextTitle = {
    alignSelf: 'flex-start',
    ...statusBarText,
    ...bold
};
export const statusBarTextDesc = {
    ...statusBarText,
    ...italic,
    fontSize: metrics.tenWidth*1.5
};
export const dialogButtonsMultiLine = {
    flexDirection: "column",
    alignItems: 'flex-end'
}
export const dialogButtons = {
}
