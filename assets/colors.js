
const colors = {
    text: "#333333",
    background: "#F0F0F0",
    backgroundPTT: "#A1C736",
    divider: "#00000029",
    primary: "#A1C736",
    secondary: "#414141",
    secondaryDark: "#333333",
    danger: "#AE0600",
    warning: "#D87F3E",
    white: "#FFF",
    actionPTT: "#FFFFFF",
    markerUso: "blue",
    markerConflito: "tomato",
    polygonBackgroundHybrid: "#FFFFFF55",
    polygonBackgroundStandard: "#A1C73655",
    action: "#3333FF",
    appOverlay: "#D87F3E66",
    appOverlayOpaque: "#FFF",
    disabled: '#AAAAAA'
}

export default colors
