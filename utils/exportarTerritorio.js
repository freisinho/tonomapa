import { Image, Platform } from 'react-native';
import * as Print from 'expo-print';
import * as FileSystem from 'expo-file-system';
import * as MediaLibrary from "expo-media-library";
import * as Sharing from "expo-sharing";
import dayjs from 'dayjs';
import 'dayjs/locale/pt-br';
import logo from '../assets/images/logo/colorida/80/logo.png';
import {pegaMunicipioReferencia} from '../bases/municipios';
import {pegaEstado} from '../bases/estados';
import tipos from '../bases/tipos';

const itemHTML = (titulo, valor) => {
    return (valor)
        ? `
            <p style="margin-bottom: 0cm; line-height: 100%">
                <b>${titulo}:</b>
            </p>
            <p style="margin-top: 10px">
                ${valor}
            </p>
        `
        : '';
};

const listaHTML = (titulo, valores, tipo, campo = null) => {
    if (valores.length == 0) {
        return '';
    }

    let retHTML = `
        <p style="margin-bottom: 0.1cm; line-height: 100%">
                <b>${titulo}:</b>
        </p>
        <ul>
    `;
    for (let valorRaw of valores) {
        const valor = (campo == null) ? valorRaw : valorRaw[campo];
        if (tipo) {
            let tipoTxt = '';
            for (let tipoObjeto of tipos.data[tipo]) {
                if (tipoObjeto.id.toString() == valor.toString()) {
                    tipoTxt = tipoObjeto.nome;
                    break;
                }
            };
            const valorNome = (valorRaw.nome) ? valorRaw.nome + ' (' + tipoTxt + ')' : tipoTxt;
            const descricaoTxt = (valorRaw.descricao)
                ? `: <i>${valorRaw.descricao}</i>`
                : '';
            retHTML += `
            <li>
                ${valorNome}${descricaoTxt}
            </li>
            `;
        }
    }
    retHTML += `
        </ul>
    `;
    return retHTML;
};

const mapaHTML = (uri, logoUri, currentUser) => {
    /*DEBUG*/ //console.log({uri, logoUri});
    const pageSize = Platform.OS === 'ios' ? '1127' : '900';
    if (!uri) {
        return '';
    }
    const imgSrc = `data:image/png;base64, ${uri}`;

    return `
        <div class="mapWrapper">
            <img style=" float:right;width:40px;height:40px;margin-top:-15px" src="${logoUri}"/>
            <h1 class="western" align="left" style="margin-top: 20px; margin-bottom: 0cm; border-top: none; border-bottom: 1px solid #666666; border-left: none; border-right: none; padding-top: 0cm; padding-bottom: 0.07cm; padding-left: 0cm; padding-right: 0cm">
                ${currentUser.currentTerritorio.nome} - Mapa
            </h1>
            <div style="width:100%; text-align: center">
                <img height=${pageSize} src="${imgSrc}" style="margin-top: 20px; "/>
            </div>
        </div>
    `;
};

export const exportarTerritorio = async ({currentUser, areaTxt, statusMessage, uri}) => {
    /*DEBUG*/ //console.log('entrei em exportarTerritorio', {areaTxt, statusMessage, uri});
    dayjs.locale('pt-br');
    const data = dayjs().format('DD [de] MMMM [de] YYYY');
    const logoUri = Image.resolveAssetSource(logo).uri;
    let municipioHTML = '';
    if (currentUser.currentTerritorio.municipioReferenciaId) {
        try {
            const municipio = pegaMunicipioReferencia(currentUser.currentTerritorio.municipioReferenciaId);
            const estado = pegaEstado(currentUser.currentTerritorio.municipioReferenciaId.toString().substring(0,2));
            municipioHTML = itemHTML('Município de referência', municipio.label + ' / ' + estado.uf);
        } catch (e) {
            console.log('Erro resgatando municipio', {erro: e, municipio: municipio, estado: estado, currentUser: currentUser});
        }
    }
    let tipoComunidadeNomes = [];
    if (currentUser.currentTerritorio.tiposComunidade.length > 0) {
        for (let tipoComunidadeId of currentUser.currentTerritorio.tiposComunidade) {
            for (let tipoComunidade of tipos.data.tiposComunidade) {
                if (tipoComunidade.id == tipoComunidadeId) {
                    tipoComunidadeNomes.push(tipoComunidade.nome);
                    break;
                }
            }
        }
    }

    let html = `
    <html>
    <head>
        <style>
            .mapWrapper {
                break-before: page;
            }

            p {
                margin-bottom: 0.25cm;
                line-height: 115%
            }

            h1 {
                margin-bottom: 0.21cm
            }

            h1.western {
                font-family: "Liberation Sans", sans-serif;
                font-size: 18pt
            }
        </style>
    </head>
    <body>
            <img style="float:right;width:40px;height:40px;margin-top:-15px" src="${logoUri}" />
            <h1 class="western" align="left"
                style="margin-top: 20px; margin-bottom: 0cm; border-top: none; border-bottom: 1px solid #666666; border-left: none; border-right: none; padding-top: 0cm; padding-bottom: 0.07cm; padding-left: 0cm; padding-right: 0cm">
                ${currentUser.currentTerritorio.nome} - Informações básicas
            </h1>
            <p style="margin-top: 10px; text-align: right; font-style: italic; font-size: 10pt">
                Relatório gerado pelo aplicativo Tô no Mapa em ${data}.
            </p>
            ${itemHTML('Área aproximada', areaTxt)}
            ${municipioHTML}
            ${listaHTML('Tipo de comunidade', currentUser.currentTerritorio.tiposComunidade, 'tiposComunidade', 'id')}
            ${itemHTML('Ano de criação', currentUser.currentTerritorio.anoFundacao)}
            ${itemHTML('Famílias', currentUser.currentTerritorio.qtdeFamilias)}
            ${listaHTML('Locais de uso', currentUser.currentTerritorio.areasDeUso, 'tiposAreaDeUso', 'tipoAreaDeUsoId')}
            ${listaHTML('Conflitos', currentUser.currentTerritorio.conflitos, 'tiposConflito', 'tipoConflitoId')}
            <div style="margin-top: 30px; border: 2px solid #777; text-align: left; padding: 10px">
                <div style="text-align: center; font-size: 120%; font-weight: bold">
                    Situação: ${statusMessage.nome}
                </div>
                <p style="font-style: italic; ">
                    ${statusMessage.mensagem}
                </p>
            </div>
        ${mapaHTML(uri, logoUri, currentUser)}
    </body>
    </html>
    `;

    /*DEBUG*/ //console.log(html);

    const pdfUri = await Print.printToFileAsync({
        html: html,
        width: 595,
        height: 842
    });

    /*DEBUG*/ //console.log(html);

    await Sharing.shareAsync(pdfUri.uri, { UTI: '.pdf', mimeType: 'application/pdf' });
};
