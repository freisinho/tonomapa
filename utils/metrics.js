import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

const screenWidth = wp("100%");
const screenHeight = hp("100%");

const tenWidth = screenWidth / 41.142857142857144;

const tenHeight = screenHeight / 73.14285714285714;

export default {
    tenWidth,
    tenHeight,
};
