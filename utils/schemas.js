import * as yup from 'yup';

export const PTT_TERRITORIO_VALIDADOR = yup.object().shape(
    {
        email: yup.string().email('E-mail tem que ser válido').required('· Seu email (cadastrante)').nullable(),
        currentTerritorio: yup.object().shape({
            cep: yup.string().matches(/^\d{5}-\d{3}$/, '· Cep da comunidade').nullable(),
            descricaoAcesso: yup.string().required('· Descrição de acesso').nullable(),
            zonaLocalizacao: yup.string().required('· Localização').nullable(),
            outraZonaLocalizacao: yup.string().when("zonaLocalizacao", {
                is: 'OUTRO',
                then: yup.string().required('· Outra localização').nullable()
            }).nullable(),
            autoIdentificacao: yup.string().nullable(),
            segmentoId: yup.number().required('· Tipo de comunidade (segmento)').nullable(),
            historiaDescricao: yup.string().nullable(),
            ecNomeContato: yup.string().nullable(),
            ecLogradouro: yup.string().nullable(),
            ecNumero: yup.string().nullable(),
            ecComplemento: yup.string().nullable(),
            ecBairro: yup.string().nullable(),
            ecCep: yup.string().matches(/^\d{5}-\d{3}$/, '· Cep do contato').nullable(),
            ecCaixaPostal: yup.string().nullable(),
            ecMunicipioId: yup.number().required('· Município do contato').nullable(),
            ecEmail: yup.string().email('· E-mail tem que ser válido!').required('· E-mail do contato').nullable(),
            ecTelefone: yup.string().matches(/\(\d{2}\) \d{5}-\d{4}/, '· Telefone do contato').required('· Telefone do contato').nullable()
        })
    });


export const ANEXO_VALIDADOR = yup.object().shape(
    {
        nome: yup.string().required('· Nome do arquivo/imagem').nullable(),
        pttTipo: null,
        descricao: yup.string().when("pttTipo", {
            is: true,
            then: yup.string().required('· Descrição').nullable()
        }).nullable(),
        tipoAnexo: yup.string().when("pttTipo", {
            is: true,
            then: yup.string().oneOf(['FONTE_INFORMACAO', 'ICONE', 'ANEXO'], '· Tipo de anexo').required('· Tipo de anexo').nullable()
        }).nullable(),
        mimetype: yup.string().when("tipoAnexo", {
            is: 'FONTE_INFORMACAO',
            then: yup.string().oneOf(['application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'], '. Arquivos nos formatos: (.pdf, .doc e .docx)').nullable()
        }).when("tipoAnexo", {
            is: 'ICONE',
            then: yup.string().oneOf([null, 'image/png', 'image/jpeg'], '.Arquivos nos formatos: (.png e .jpeg)').nullable()
        }
        ).when("tipoAnexo", {
            is: 'ANEXO',
            then: yup.string().oneOf([null, 'image/png', 'image/jpeg', 'video/mp4', 'video/x-msvideo'], '. Arquivos nos formatos:  (.png, .jpeg, .avi, .mp4)').nullable()
        }).nullable()
    });

export const DADOSPTT_SCREEN_VALIDADOR = yup.object().shape(
    {
        email: yup.string().email('· Seu email (cadastrante)').nullable(),
        currentTerritorio: yup.object().shape({
            cep: yup.string().matches(/^\d{5}-\d{3}$/, '· Cep da comunidade').nullable(),
            descricaoAcesso: yup.string().nullable(),
            zonaLocalizacao: yup.string().nullable(),
            outraZonaLocalizacao: yup.string().when("zonaLocalizacao", {
                is: 'OUTRO',
                then: yup.string().nullable()
            }).nullable(),
            autoIdentificacao: yup.string().nullable(),
            segmentoId: yup.object().nullable(),
            historiaDescricao: yup.string().nullable(),
            ecNomeContato: yup.string().nullable(),
            ecLogradouro: yup.string().nullable(),
            ecNumero: yup.string().nullable(),
            ecComplemento: yup.string().nullable(),
            ecBairro: yup.string().nullable(),
            ecCep: yup.string().matches(/^\d{5}-\d{3}$/, '· Cep do contato').nullable(),
            ecCaixaPostal: yup.string().nullable(),
            ecMunicipioId: yup.number().nullable(),
            ecEmail: yup.string().email('· E-mail tem que ser válido!').nullable(),
            ecTelefone: yup.string().matches(/\(\d{2}\) \d{5}-\d{4}/, '· Telefone do contato').nullable()
        })
    });
