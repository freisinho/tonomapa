import {Linking} from 'react-native';
import municipios from "../bases/municipios";
import * as Localization from 'expo-localization';

export const pluralize = (val, word, plural = word + 's') => {
    const _pluralize = (num, word, plural = word + 's') =>
        [1, -1].includes(Number(num)) ? word : plural;
    if (typeof val === 'object') return (num, word) => _pluralize(num, word, val[word]);
    return _pluralize(val, word, plural);
};

export const pr = (param) => {
  if (param == null || param === void(0)) {
      return "NULL";
  } else {
      return param.toString();
  }
};

export const formataNumeroBR = (num, decimais = 2) => {
    if (!num) {
        return '';
    }
    return (
        num
            .toFixed(decimais)
            .replace('.', ',')
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    )
}

export const deepEqual = (a, b) => {
  if (a === b) return true;

  if (a == null || typeof a != "object" ||
      b == null || typeof b != "object") return false;

  let keysA = Object.keys(a), keysB = Object.keys(b);

  if (keysA.length != keysB.length) return false;

  for (let key of keysA) {
    if (!keysB.includes(key) || !deepEqual(a[key], b[key])) return false;
  }

  return true;
};

const tmpId = '__tmpId_';

export const ehDadoLocal = (obj) => {
    return typeof obj.id == 'string' && obj.id.includes(tmpId);
}

export const geraTmpId = (append = null) => {
    if (append == null) {
        append = Math.random().toString(16).substring(7);
    }
    return tmpId + append;
};

export const resgataEstadoMunicipioDoId = (municipioId) => {
    const estadoId = parseInt(municipioId.toString().substring(0, 2));
    const mun = municipios[estadoId].find(element => element.codigo_ibge === parseInt(municipioId));
    return {
        estadoId: estadoId,
        municipio: mun
    };
};

export function parseLocaleNumber(stringNumber) {
    const digitGroupingSeparator = (1000).toLocaleString(Localization.locale)[1] === "," ? "," : ".";
    const decimalSeparator = digitGroupingSeparator === "." ? "," : ".";
    const newStringNumber = stringNumber
        .replace(new RegExp(`\\${digitGroupingSeparator}`, "g"), "")
        .replace(new RegExp(`\\${decimalSeparator}`), ".");
    return Number(newStringNumber);
}

export function toFixedLocale(value, numDigits=2) {
    if (value === null || isNaN(value)) {
        return null;
    }
    const digitGroupingSeparator = (1000).toLocaleString(Localization.locale)[1] === "," ? "," : ".";
    const decimalSeparator = digitGroupingSeparator === "." ? "," : ".";

    const standardFixedString = value.toFixed(numDigits);

    if (decimalSeparator === ",") {
        return standardFixedString.replace(".", ",");
    }
    return standardFixedString; // Locale matches JavaScript default
}

export function toCleanLocale(valueString) {
    const digitGroupingSeparator = (1000).toLocaleString(Localization.locale)[1] === "," ? "," : ".";
    const decimalSeparator = digitGroupingSeparator === "." ? "," : ".";
    if (decimalSeparator === ",") {
        return valueString.replace(/[^\d,]+/g, '');
    }
    return valueString.replace(/[^\d\.]+/g, '');
}

export const openUrlInBrowser = (url) => {
    Linking.canOpenURL(url).then(supported => {
        if (supported) {
            Linking.openURL(url);
        } else {
            console.log("Don't know how to open URI: " + url);
        }
    });
};
