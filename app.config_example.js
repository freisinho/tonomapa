export default ({ config }) => {
    const version = {
        version: 0,
        build: 59
    };
    const versionName = version.version.toString() + "." + version.build.toString();
    config.version = versionName;
    config.ios.buildNumber = versionName;
    config.android.versionCode = version.build;
    config.extra = {
        dataDaVersao: 'VERSION DATE',
        testing: false,
        debugGraphQL: false,
        isPTTDisabled: false,
        dashboardEndpoint: "http://tonomapa.eita.org.br",
        maxFileSize: 10*1024*1024, // 10MB, change if you want
        sentryDns: "Your Sentry monitoring URL",
        termosDeUsoUri: 'URL of Terms Of Usage',
        TICCAsUri: 'URL of page explaining TICCAs',
        // to render XYZ raster tiles
        xyzRTiles: {
            // the value must be 'true' if you plan to use it
            using: false,
            urlTile: "YOUR XYZ TILE (ex.: http://c.tile.openstreetmap.org/{z}/{x}/{y}.png)"
        },
        pollingRate: 5 * 60 * 1000 // Time interval to poll server about statusId
    };

    // For rendering Google Maps:
    config.android.config.googleMaps.apiKey = "YOUR_API_KEY";
    // For receiving notifications through FireBase:
    config.android.googleServicesFile = "PATH_TO_YOUR_GOOGLESERVICES_FILE";

    return {
        ...config,
    };
};
