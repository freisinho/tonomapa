import React from 'react';
import {View, Text} from "react-native";
import Constants from 'expo-constants';
import {Layout} from "../styles";

export default function TextoSobre() {
    return (
        <View>
            <Text style={Layout.textParagraph}>
                <Text style={Layout.bold}>Tô no Mapa </Text>
                <Text>
                    é uma iniciativa de duas ONGs brasileiras, IPAM e ISPN, em parceria com a Rede Cerrado, construída a partir do diálogo com diversas organizações e comunidades, cujo objetivo é gerar um mapa inédito com informações sobre comunidades rurais no Brasil, que possa apoiar na busca pela garantia de seus direitos sociais e territoriais.
                </Text>
            </Text>
            <Text style={Layout.textParagraph}>
                O app permite que o usuário defina o contorno da comunidade de diferentes formas - marcar os pontos no mapa, enviar arquivos que já contenham essas informações ou andar pelo território e indicar pontos, como áreas de coleta de frutos, localização das moradias e outras características relevantes sobre o território. Também é possível indicar locais onde ocorre algum tipo de conflito, seja invasão, garimpo ou outra ameaça.
            </Text>
            <Text style={Layout.textParagraph}>
                É importante que as informações aqui inseridas sejam discutidas dentre os membros da comunidade e que haja um comum acordo acerca dos limites do território.
            </Text>
            <Text style={Layout.textParagraph}>
                Incluir a sua comunidade no app não significa, de forma alguma, a legalização, titulação ou demarcação do território pelo órgão competente, mas é um começo para que as comunidades, especialmente aquelas mais ameaçadas, passem a ser vistas pelas políticas públicas.
            </Text>
            <Text style={{...Layout.textParagraph, ...Layout.italic}}>
                Aplicativo Tô no mapa. Versão {Constants.manifest.version} ({Constants.manifest.extra.dataDaVersao}).
            </Text>
        </View>
    );
}
