import React from 'react';
import {View} from "react-native";
import {Appbar} from "react-native-paper";
import {Layout} from "../styles";
import colors from "../assets/colors";
import TELAS from "../bases/telas";
import metrics from '../utils/metrics'
/**
 *
 * @param navigation Navigation
 * @param title string
 * @param tela TELAS.*
 * @param rightAction
 * @param goBack Boolean
 * @param customGoBack Function
 * @returns {*}
 * @constructor
 */
export default function AppbarTonomapa({navigation, title, tela = TELAS.MAPA, rightAction = null, goBack = false, customGoBack = null}) {
    return (
            <Appbar.Header style={Layout.topBar} theme={{colors: {primary: colors.background}}}>
                    <Appbar.BackAction
                        size={metrics.tenWidth*2.5}
                        onPress={() => customGoBack ? customGoBack() : (goBack ? navigation.goBack() : navigation.navigate('Home', {tela:tela}))}
                    />
                    <Appbar.Content
                        title={title}
                        titleStyle={Layout.topBarTitle}
                    />
                    {rightAction &&
                        <Appbar.Action icon={rightAction.icon} color={rightAction.color} onPress={() => rightAction.fn()} />
                    }
            </Appbar.Header>
    );
}
