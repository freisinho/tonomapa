import React from 'react'
import { Inputs } from "../styles";
import { TextInput } from "react-native-paper";
import { StyleSheet } from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import { pickerStyles } from "../styles/pickers";


const pickerSelectStyles = StyleSheet.create(pickerStyles);

const GenericSelect = (props) => {
    const { label, onValueChange, selectedValue, items } = props;

    return <TextInput
        label={label}
        value={selectedValue}
        style={Inputs.textInput}
        labelStyle={Inputs.textInputLabel}
        render={() => (
            <RNPickerSelect
                onValueChange={(value) => onValueChange(value)}
                placeholder={{ label: '', value: null }}
                useNativeAndroidPickerStyle={false}
                style={pickerSelectStyles}
                items={items}
                value={selectedValue}
            />
        )}
    />
};

export default GenericSelect;
