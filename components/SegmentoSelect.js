import React, { useEffect, useState } from 'react'
import { MaterialIcons } from '@expo/vector-icons';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import tipos from "../bases/tipos";
import { View, Text } from "react-native";
import metrics from "../utils/metrics";

const SegmentoSelect = (props) => {
    const { selectedItems } = props
    const [selectedItem, setSelectedItems] = useState([]);
    const [items, setItems] = useState([]);


    useEffect(() => {
        setItems(getSegmentos())
    }, []);

    useEffect(() => {
        if (selectedItems)
            setSelectedItems(prev => [...prev, ...[selectedItems]])
    }, [selectedItems]);

    const onValueChange = (values) => {
        props.onValueChange(values);
    };

    const getSegmentos = () => {
        return tipos.data.segmentos.map(segmento => ({
            ...segmento,
            name: segmento.nome,
            id: segmento.id
        }));
    };

    const getSegmentById = (id) => {
        return tipos.data.segmentos.find(segmento => {
            if (id == segmento.id)
                return segmento.id;
        });
    };

    const NoResultsComponent = () => {
        return <View style={{ textAlign: 'center', flex: 1, margin: 10 }}><Text>Não há segmento com esta busca</Text></View>
    };

    return <SectionedMultiSelect
        items={items}
        IconRenderer={MaterialIcons}
        uniqueKey="id"
        selectText="Selecione o segmento"
        confirmText="Concluído"
        selectedText="tipos"
        searchPlaceholderText="Buscar"
        noResultsComponent={NoResultsComponent}
        styles={{ selectToggle: { marginBottom: 8 }, selectToggleText: { fontSize: metrics.tenWidth * 1.8 } }}
        single={true}
        alwaysShowSelectText={true}
        readOnlyHeadings={false}
        onSelectedItemsChange={(value) => {onValueChange(getSegmentById(value[0])) }}
        selectedItems={selectedItem}
    />
};

export default SegmentoSelect;
