import React, { useRef } from 'react';
import {TextInput} from "react-native-paper";
import {StyleSheet} from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import {Inputs} from "../styles";
import {pickerStyles} from "../styles/pickers";
import municipios from "../bases/municipios";
import estados from "../bases/estados";

const pickerSelectStyles = StyleSheet.create(pickerStyles);

const MunicipioSelectH = (props) => {
    const mudouEstado = useRef(false);

    return (
        <>
            <TextInput
                label= {props.labelEstado || "Estado"}
                value={props.estadoId}
                style={Inputs.textInput}
                labelStyle={Inputs.textInputLabel}
                render={(props2) => (
                    <RNPickerSelect
                        key="estados"
                        disabled={props.disabled}
                        onValueChange={(estadoId) => {
                            // console.log('entrou estadoOnValueChange', estadoId);
                            mudouEstado.current = true;
                            props.onValueChange({
                                estadoId: estadoId,
                                municipioId: null
                            });
                        }}
                        placeholder={{label: '', value: null}}
                        useNativeAndroidPickerStyle={false}
                        style={pickerSelectStyles}
                        items={estados}
                        value={props.estadoId}
                    />
                )}
            />
            <TextInput
                label={props.labelMunicipio || "Município"}
                value={props.municipioId}
                style={Inputs.textInput}
                labelStyle={Inputs.textInputLabel}
                render={(props2) => {
                    return (
                    <RNPickerSelect
                        key="municipios"
                        disabled={props.disabled || props.estadoId === null}
                        placeholder={{label: '', value: null}}
                        useNativeAndroidPickerStyle={false}
                        style={pickerSelectStyles}
                        items={(props.estadoId) ? municipios[props.estadoId] : []}
                        value={props.municipioId}
                        onValueChange={(municipioId) => {
                            // console.log('entrou municipioOnValueChange', municipioId, mudouEstado.current);
                            if (mudouEstado.current && municipioId === null) {
                                return;
                            }

                            mudouEstado.current = false;

                            props.onValueChange({
                                municipioId: municipioId
                            });
                        }}
                    />
                );}}
            />
        </>
    );
};

export default MunicipioSelectH;
