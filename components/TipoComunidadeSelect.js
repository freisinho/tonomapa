import React, {useState} from 'react'
import {MaterialIcons} from '@expo/vector-icons';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import tipos from "../bases/tipos";
import {TextInput} from "react-native-paper";
import {StyleSheet, View, Text} from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import {pickerStyles} from "../styles/pickers";
import {Inputs} from "../styles";
import metrics from "../utils/metrics";


const pickerSelectStyles = StyleSheet.create(pickerStyles);

const TipoComunidadeSelect = (props) => {

    const [selectedItems, setSelectedItems] = useState(props.selectedItems);

    const onValueChange = (values) => {
        setSelectedItems(values);
        props.onValueChange(values);
    };

    const getTiposComunidade = () => {
        return tipos.data.tiposComunidade.map(tipoComunidade => ({
            ...tipoComunidade,
            name: tipoComunidade.nome,
            id: tipoComunidade.id
        }));
    };

    const NoResultsComponent = () => {
        return <View style={{textAlign: 'center', flex: 1, margin: 10}}><Text>Não há tipo de comunidade com esta busca</Text></View>
    };

    return <SectionedMultiSelect
        items={getTiposComunidade()}
        IconRenderer={MaterialIcons}
        uniqueKey="id"
        selectText="Tipo de comunidade"
        confirmText="Concluído"
        selectedText="tipos"
        searchPlaceholderText="Buscar"
        noResultsComponent={NoResultsComponent}
        styles={{selectToggle: {marginBottom: 8}, selectToggleText: {fontSize: metrics.tenWidth*1.8}}}
        single={false}
        alwaysShowSelectText={true}
        readOnlyHeadings={false}
        onSelectedItemsChange={onValueChange}
        selectedItems={selectedItems}
    />
};

export default TipoComunidadeSelect;
