import React from 'react'
import tipos from "../bases/tipos";
import {TextInput} from "react-native-paper";
import {StyleSheet} from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import {pickerStyles} from "../styles/pickers";
import {Inputs} from "../styles";


const pickerSelectStyles = StyleSheet.create(pickerStyles);

const TipoAreaDeUsoSelect = (props) => {
    const onValueChange = (value) => {
        props.onValueChange(value);
    };

    const getTiposAreaDeUso = () => {
        return tipos.data.tiposAreaDeUso.map(tipoAreaDeUso => ({
            ...tipoAreaDeUso,
            label: tipoAreaDeUso.nome,
            value: tipoAreaDeUso.id
        }));
    };

    return <TextInput
        label="Tipo de local de uso"
        value={props.selectedValue}
        style={Inputs.textInput}
        labelStyle={Inputs.textInputLabel}
        render={(props2) => (
            <RNPickerSelect
                onValueChange={onValueChange}
                placeholder={{label: '', value: null}}
                useNativeAndroidPickerStyle={false}
                style={pickerSelectStyles}
                items={getTiposAreaDeUso()}
                value={props.selectedValue}
            />
        )}
    />
};

export default TipoAreaDeUsoSelect;
