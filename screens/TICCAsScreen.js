import React, {useState} from 'react';
import {View, Text} from "react-native";
import {WebView} from "react-native-webview";
import {Button, ActivityIndicator} from "react-native-paper";
import {SafeAreaView} from "react-native-safe-area-context";
import Constants from 'expo-constants';
import {Layout} from "../styles";
import AppbarTonomapa from "../partials/AppbarTonomapa";
import colors from '../assets/colors';
import TELAS from '../bases/telas';

export default function TICCAsScreen({navigation}) {
    const [carregando, setCarregando] = useState(true);

    const ActivityIndicatorElement = () => {
        return (
            <View style={Layout.appOverlay}>
                <View style={{backgroundColor: colors.primary, padding: 40, alignItems: 'center', borderRadius: 8}}>
                    <ActivityIndicator size="large" animating={true} color={colors.white} />
                    <Text style={{color: colors.white, marginTop: 30}}>
                        Baixando o conteúdo...
                    </Text>
                </View>
            </View>
        );
    };

    return (
        <View style={Layout.containerStretched}>
            <AppbarTonomapa
                navigation={navigation}
                title="O que são TICCAs?"
                goBack={true}
            />
                <View style={{...Layout.containerStretched, paddingBottom: 20}}>
                    <WebView
                        source={{uri: Constants.manifest.extra.TICCAsUri}}
                        startInLoadingState={true}
                        renderLoading={ActivityIndicatorElement}
                    />
                    <Button
                    mode="contained"
                    dark={true}
                    style={Layout.buttonCenter}
                    onPress={() => {
                        navigation.navigate('Home');
                    }}
                    >
                    Voltar
                    </Button>
                </View>
        </View>
    );
}
