import React, {useState, useReducer, useRef} from 'react';
import {
    AppState,
    BackHandler,
    StyleSheet,
    View,
    Text,
    Dimensions,
    Alert,
    Image,
    PixelRatio
} from 'react-native';
import {MaterialCommunityIcons} from '@expo/vector-icons';
import MapView, {
    PROVIDER_GOOGLE,
    MAP_TYPES,
    Polygon,
    ProviderPropType,
    Marker,
    Callout} from 'react-native-maps';
import { captureRef } from "react-native-view-shot";
import {
    Avatar,
    Appbar,
    Button,
    IconButton,
    Snackbar,
    Portal,
    Dialog,
    ActivityIndicator,
    Banner
} from "react-native-paper";
import AppbarTonomapa from "../partials/AppbarTonomapa";
import {SafeAreaView} from "react-native-safe-area-context";
import colors from "../assets/colors";
import {Layout, LayoutMapa} from "../styles";
import * as Location from 'expo-location';
import {
    AuthContext,
    apiErrors,
    persisteCurrentUser,
    persisteCurrentTerritorioStatusId,
    persisteSettings,
    carregaTudoLocal,
    enviaParaDashboard,
    validaDados,
    cloneCurrentUser,
    updateTerritorioStatusId
} from "../db/api";
import {
    pluralize,
    deepEqual,
    geraTmpId,
    resgataEstadoMunicipioDoId,
    openUrlInBrowser
} from "../utils/utils";
import {exportarTerritorio} from '../utils/exportarTerritorio';
import AppIntroSlider from 'react-native-app-intro-slider';
import TELAS from "../bases/telas";
import STATUS, {
    buildStatusMessage,
    alertStatusId,
    isEnviarEnabled,
    isEnviarPTTEnabled,
    isCurrentTerritorioEditable,
    isPTTMode
} from "../bases/status";
import {geraIconeTipoUsoOuConflito} from "../bases/icones";
import {useFocusEffect} from '@react-navigation/native';
import tipos from "../bases/tipos";
import {slideStyles, SLIDES, renderSlide, SlideWidget} from "../bases/intro_slides";
import {
    calculaAreaPoligono,
    formataArea,
    UNIDADES_AREA,
    isTelaMapeamento
} from '../maps/mapsUtils';
import metrics from '../utils/metrics';
import Constants from 'expo-constants';
import AsyncStorage from '@react-native-async-storage/async-storage';

/**
 * Tela principal do aplicativo, onde está o mapa
 * @module screens/MapaScreen
 */

 /**
  * @type {String}
  */
 let snackBarMessage = "";

/**
 * @method      MapaScreen
 * @param       {object}   provider
 * @param       {object}   navigation        Objeto de navegação
 * @param       {object}   route             Os parâmetros passados de outra screen para esta
 */
export default function MapaScreen({provider = null, navigation = null, route}) {
    /*DEBUG*/ //console.log('Home', route.params);
    const {width, height} = Dimensions.get('window');

    /**
     * @type {Number}
     */
    const ASPECT_RATIO = width / height;

    /**
     * @type {Number}
     */
    const LATITUDE_DELTA = 0.0922;

    /**
     * @type {Number}
     */
    const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

    /**
     * @type {Object}
     */
    const GEOLOCATION_OPTIONS = { accuracy: Location.Accuracy.Highest, distanceInterval: 5, timeInterval: 1000 };

    /**
     * @type {Object}
     */
    const ICONE_TIPO = {
        locationMark: require('../assets/images/gps/escuro/noun_Gps_Add.png'),
        locationMarkClaro: require('../assets/images/gps//claro/noun_Gps_Add.png'),
        verticeMovel: require('../assets/images/vertices/escuro/30/vertice.png'),
        verticeMovelClaro: require('../assets/images/vertices/claro/30/vertice.png'),
        vertice: require('../assets/images/vertices/escuro/30/vertice.png'),
        verticeClaro: require('../assets/images/vertices/claro/30/vertice.png')
    };

    /**
     * @type {Object}
     */
    const LOGO_IMAGE = {
        colorida: require('../assets/images/logo/colorida/80/logo.png'),
        branca: require('../assets/images/logo/branca/80/logo.png')
    };

    /**
     * @type {Object}
     */
    const initialState = {
        region: null,
        polygons: [],
        markers: [],
        markersTipoUsoConflito: [],
        editingMarker: null,
        creatingHole: false,
        modo: null,
        tela: TELAS.MAPA,
        locationMarkerCoordinate: null,
        currentUser: null,
        enviarDadosDialogo: {
            botaoEnviar: true,
            botaoCancelar: true,
            botaoOk: null,
            etapa: 'situacao',
            situacao: [],
            erro: null,
            apiErrors: {
                graphQLErrors: [],
                networkError: ""
            }
        },
        carregando: null,
        preparandoShot: null,
        statusMessage: null,
        mapType: null,
        settings: {},
        introDialog: true
    };

    /**
     * @type {Object}
     */
    const enviarDadosDialogoErro = {
        ...initialState.enviarDadosDialogo,
        botaoEnviar: null,
        botaoCancelar: null,
        botaoOk: true,
        etapa: 'erro'
    };

    /**
     * @type {Object}
     */
    const enviarDadosDialogoCarregando = {
        ...enviarDadosDialogoErro,
        botaoOk: null,
        etapa: 'carregando',
        situacao: 'carregando',
    };

    /**
     * @method reducer
     * @param  {Object} state
     * @param  {Object} newState
     * @return {Object}
     */
    const reducer = (state, newState) => {
        return {...state, ...newState};
    };

    /**
     * @type {Array}
     */
    const [state, setState] = useReducer(reducer, initialState);

    /**
     * @type {Object}
     */
    const calloutVisible = useRef(false);

    /**
     * @type {Object}
     */
    const locationMarker = useRef();

    /**
     * @type {Object}
     */
    const isLocationMonitoramentoUsuariaAtivo = useRef(false);
    const locationMonitoramentoUsuaria = useRef(null);

    /**
     * @type {Object}
     */
    const shouldPingStatusId = useRef(true);

    /**
     * @type {Object}
     */
    const shouldCheckStatusChange = useRef(true);

    const updateMonitoramentoLocation = (tela = null) => {
        /*DEBUG*/ //console.log('updateMonitoramentoLocation');
        if (!tela) {
            tela = (route.params.tela)
                ? route.params.tela
                : state.tela;
        }
        if (state.settings.useGPS && isTelaMapeamento(tela)) {
            locationAtivar();
        } else {
            locationDesativar();
        }
    }

    /**
     * @type {Object}
     */
    const map = useRef(null);

    /**
     * @type {Object}
     */
    const viewShotRef = useRef(null);

    /**
     * @type {Object}
     */
    const mapLoaded = useRef(false);

    /**
     * @type {Array}
     */
    const mapMarker = [];

    /**
     * @type {Array}
     */
    const mapPolygon = [];

    /**
     * @type {Object}
     */
    let mapEditingPolygon = useRef();

    /**
     * @type {Array}
     */
    const mapEditingPolygonEdge = [];

    /**
     * @type {Object}
     */
    const [snackBarVisible, setSnackBarVisible] = useState(false);

    /**
     * @type {Object}
     */
    const {signUp, signOut} = React.useContext(AuthContext);

    /**
     * @type {Object}
     */
    const edgePadding = {top: metrics.tenWidth*18, bottom: metrics.tenWidth*6, left: metrics.tenWidth*4, right: metrics.tenWidth*4};

    /**
     * @type {Object}
     */
    const edgePaddingAdjusted = Platform.OS === 'android'
        ? {top: PixelRatio.getPixelSizeForLayoutSize(edgePadding.top), bottom: PixelRatio.getPixelSizeForLayoutSize(edgePadding.bottom), left: PixelRatio.getPixelSizeForLayoutSize(edgePadding.left), right: PixelRatio.getPixelSizeForLayoutSize(edgePadding.right)}
        : edgePadding;

    /**
     * Recarrega do cache os dados de usuário (reload) e atualiza a situação do status
     * @inner recarregaCurrentUser
     */
    const recarregaCurrentUser = () => {
        setState({currentUser: null});
    }

    /**
     * Atualiza as configurações de usuária e salva
     * @inner updateSettings
     * @param  {Object}       params Novo objeto de configurações "settings"
     */
    const updateSettings = (params) => {
        /*DEBUG*/ //console.log('entered updateSettings');
        persisteSettings(params.settings).then((newSettings) => {
            setState({
                settings: newSettings,
            });
            if (!params.noSnackBarMessage) {
                snackBarMessage = "Configurações atualizadas com sucesso"
                setSnackBarVisible(true);
            }
        });
    };

    const showTiccasAlert = async (territorio) => {
        try {
            const message = territorio.statusId == STATUS.OK_TONOMAPA && !JSON.parse(await AsyncStorage.getItem('@ticcas_mensagem_' + territorio.id));
            message &&
                Alert.alert(
                    `Parabéns, a comunidade ${territorio.nome} foi aprovada!`,
                    "Deseja saber mais sobre TICCAs?",
                    [
                        {
                            text: "Não",
                            onPress: () => markShowedTiccas(territorio.id)
                        },
                        { text: "Sim, eu quero", onPress: () => { navigation.navigate('TICCAs'); markShowedTiccas(territorio.id) } }
                    ],
                    { cancelable: true }
                );
        } catch (error) {
            console.error(error)
        }
    }

    const markShowedTiccas = async (id) => {
        try {
            await AsyncStorage.setItem('@ticcas_mensagem_'+id, JSON.stringify(true));
        } catch (error) {
            console.error(error)
        }
    }

    const getAllMapElementsCoords = () => {
        let coords = [];
        for (let i = 0; i < state.polygons.length; i++) {
            coords = coords.concat(state.polygons[i].coordinates);
        }
        for (let i = 0; i < state.markers.length; i++) {
            coords.push(state.markers[i].coordinate);
        }
        if (state.settings.editing && state.settings.editing.coordinates.length >= 3) {
            coords = coords.concat(state.settings.editing.coordinates);
        }
        return coords;
    };

    const areaPoligonos = () => {
        if (state.polygons.length) {
            return state.polygons.reduce((area, current) => area + current.area, 0);
        }
        return null;
    };

    const mapFitToElements = () => {
        /*DEBUG*/ //console.log('mapFitToElements');
        if (
            map && (state.polygons.length ||
            state.markers.length ||
            (
                state.settings.editing &&
                state.settings.editing.coordinates &&
                state.settings.editing.coordinates.length >= 3)
            )
        ) {
            if (state.polygons.length==0 && state.markers.length == 1 && (!state.settings.editing || state.settings.editing.coordinates.length < 3)) {
                map.current.animateCamera({center: state.markers[0].coordinate});
            } else {
                map.current.fitToCoordinates(
                    getAllMapElementsCoords(),
                    {edgePadding: edgePaddingAdjusted}
                );
            }
        } else if (state.region) {
            map.current.animateToRegion(state.region, 2000);
        }
    };

    /**
     * Informa do erro de exportação do PDF
     * @inner alertaErroExportaPDF
     */
    const alertaErroExportaPDF = () => {
        Alert.alert(
            'Erro ao gerar o relatório!',
            'Este erro às vezes acontece por conta de memória do celular, e ao tentar de novo, pode dar certo. Deseja tentar mais uma vez?',
            [
                {
                    text: 'Cancelar', onPress: async () => {
                    }
                },
                {
                    text: 'Sim, tentar de novo', onPress: async () => {
                        ajustaMapaEExportaPDF();
                    }
                }
            ],
            {cancelable: true},
        );
    };

    const ajustaMapaEExportaPDF = () => {
        const finishLoadingState = {
            carregando: null,
            preparandoShot: false
        };
        setState({
            carregando: 'Gerando o relatório da comunidade...',
            preparandoShot: true
        });
        mapFitToElements();
        const params = {
            currentUser: state.currentUser,
            areaTxt: formataArea(areaPoligonos(), state.settings.unidadeArea),
            statusMessage: state.statusMessage,
            uri: null
        };
        setTimeout(() => {
            setState(finishLoadingState);
        }, 30000);
        setTimeout(() => {
            map.current.takeSnapshot({
                format: 'png',
                result: 'base64'
            }).then(uri => {
                params.uri = uri;
                /*DEBUG*/ //console.log('snapshot taken');
                exportarTerritorio(params).then(() => {
                    setState(finishLoadingState);
                }).catch(e => {
                    setState(finishLoadingState);
                    alertaErroExportaPDF();
                });
            }).catch(e => {
                exportarTerritorio(params).then(() => {
                    setState(finishLoadingState);
                }).catch(e => {
                   ('Falha na geração do PDF sem o mapa', e);
                    setState(finishLoadingState);
                    alertaErroExportaPDF();
                });
            });
        }, 3000);
    };

    const carregaDados = () => {
        /*DEBUG*/ //console.log("Entrou em carregaDados");
        carregaTudoLocal()
            .then((dadosLocais) => {
                /*DEBUG*/ //console.log('carregaDados Resultado', {settings: dadosLocais.settings, currentTerritorio: {...dadosLocais.currentUser.currentTerritorio, mensagens: [], poligono: []}});

                if (dadosLocais.currentUser.currentTerritorio.nome == null) {
                    navigation.navigate('DadosBasicos', {atualizarDados: true, cadastrarTerritorio: true});
                    return;
                }
                showTiccasAlert(dadosLocais.currentUser.currentTerritorio)

                // setState({settings: dadosLocais.settings});
                gqlTerritorioToMapa({
                    currentUser: dadosLocais.currentUser,
                    settings: dadosLocais.settings
                });
            })
            .catch(error => {
                console.log('Erro ao pegar os dados:', error.message);
                Alert.alert(
                    'Erro',
                    `Não foi possível carregar os seus dados. Favor tentar entrar novamente, e se não funcionar, entre em contato com a equipe do Tô no Mapa. Mensagem de erro: ${error.message}`,
                    [
                        {
                            text: 'OK', onPress: async () => {
                                signOut();
                            }
                        },
                    ],
                    {cancelable: false},
                );
            });
    };

    /**
     * Converte dados do mapa para graphQL e os persiste na base local
     * Parâmetros:
     *   - currentUser - currentUser changed
     *   - polygons - polygons changed
     *   - markers - markers changed
     *   - snackBarMessage
     */
    const persisteGraphqlData = (params) => {
        /*DEBUG*/ //console.log('Entrou persisteGraphqlData');
        /*DEBUG*/ //console.log(params);
        if (!("currentUser" in params) && !("polygons" in params) && !("markers" in params)) {
            return;
        }

        let newState = {};
        const currentUser = ("currentUser" in params)
            ? cloneCurrentUser(params.currentUser)
            : cloneCurrentUser(state.currentUser);

        if ("polygons" in params) {
            //Traduz state.polygons para o formato aceito pelo backend
            const polygons = params.polygons;
            const multiPol = {
                type: 'MultiPolygon',
                coordinates: []
            };

            for (let polygon of polygons) {
                let backPolygon = [[]];
                for (let i in polygon.coordinates) {
                    backPolygon[0].push([polygon.coordinates[i].longitude, polygon.coordinates[i].latitude]);
                }
                backPolygon[0].push([polygon.coordinates[0].longitude, polygon.coordinates[0].latitude]);
                multiPol.coordinates.push(backPolygon);
            }

            currentUser.currentTerritorio.poligono = multiPol;

            newState = {
                polygons: polygons,
            };
        }

        if ("markers" in params) {
            newState.markers = params.markers;
        }

        persisteCurrentUser(currentUser).then((res) => {
            if (res.dadosMudaram) {
                // Update settings if necessary:
                if ("settings" in params) {
                    updateSettings({settings: params.settings, noSnackBarMessage: true});
                }

                //newState.currentUser = res.newCurrentUser;
                newState.currentUser = null; // obriga o carregamento e ajustes no status
                if ("newState" in params) {
                    newState = {
                        ...newState,
                        ...params.newState
                    }
                }
                setState(newState);

                // Show snackbar, if necessary:
                if ("snackBarMessage" in params) {
                    snackBarMessage = params.snackBarMessage;
                    setSnackBarVisible(true);
                }
            }
        });
    };

    const persisteUsoOuConflito = () => {
        const usoOuConflitoEditado = route.params.usoOuConflitoEditado;
        delete route.params.usoOuConflitoEditado;

        /*DEBUG*/ //console.log('usoOuConflitoEditado');

        let newMarkersList = [];
        let newCurrentUser = cloneCurrentUser(state.currentUser);
        let usosEConflitosEditados = {
            areasDeUso: newCurrentUser.currentTerritorio.areasDeUso,
            conflitos: newCurrentUser.currentTerritorio.conflitos
        };
        state.markers.forEach(marker => newMarkersList.push({...marker, coordinate:{...marker.coordinate}}));
        let stopLoop = false;
        if (usoOuConflitoEditado.id) {
            for (let indiceTerritorio in usosEConflitosEditados) {
                for (let usoOuConflitoKey in usosEConflitosEditados[indiceTerritorio]) {
                    if (usosEConflitosEditados[indiceTerritorio][usoOuConflitoKey].id === usoOuConflitoEditado.id) {
                        if (route.params.apagarUsoOuConflito) {
                            usosEConflitosEditados[indiceTerritorio].splice(usoOuConflitoKey, 1);
                        } else if (indiceTerritorio == 'conflitos' && usoOuConflitoEditado.__typename == 'AreaDeUso') {
                            usosEConflitosEditados.conflitos.splice(usoOuConflitoKey, 1);
                            usosEConflitosEditados.areasDeUso.push(usoOuConflitoEditado);
                        } else if (indiceTerritorio == 'areasDeUso' && usoOuConflitoEditado.__typename == 'Conflito') {
                            usosEConflitosEditados.areasDeUso.splice(usoOuConflitoKey, 1);
                            usosEConflitosEditados.conflitos.push(usoOuConflitoEditado);
                        } else {
                            usosEConflitosEditados[indiceTerritorio][usoOuConflitoKey] = usoOuConflitoEditado;
                        }
                        stopLoop = true;
                        break;
                    }
                }
                if (stopLoop) {
                    break;
                }
            }

            //Atualiza ou apaga marker
            for (let i = 0; i < newMarkersList.length; i++){
                let marker = newMarkersList[i];
                if (marker.object.id == usoOuConflitoEditado.id) {
                    if (route.params.apagarUsoOuConflito) {
                        newMarkersList.splice(i, 1);
                    } else if (!route.params || !("usoOuConflitoMoveu" in route.params)) {
                        newMarkersList[i].title = usoOuConflitoEditado.nome;
                        newMarkersList[i].description = usoOuConflitoEditado.descricao;
                        newMarkersList[i].object = usoOuConflitoEditado;
                    } else {
                        newMarkersList[i].object = usoOuConflitoEditado;
                        newMarkersList[i].coordinate.latitude = usoOuConflitoEditado.posicao.coordinates[1];
                        newMarkersList[i].coordinate.longitude = usoOuConflitoEditado.posicao.coordinates[0];
                    }
                    break;
                }
            }
            delete route.params.apagarUsoOuConflito;
        } else {
            usoOuConflitoEditado.id = geraTmpId(newMarkersList.length + 1);
            const indiceTerritorio = (usoOuConflitoEditado.__typename === 'AreaDeUso')
                ? 'areasDeUso'
                : 'conflitos';

            usosEConflitosEditados[indiceTerritorio].push(usoOuConflitoEditado);

            newMarkersList.push({
                id: newMarkersList.length + 1,
                title: usoOuConflitoEditado.nome,
                description: usoOuConflitoEditado.descricao,
                coordinate: {
                    latitude: usoOuConflitoEditado.posicao.coordinates[1],
                    longitude: usoOuConflitoEditado.posicao.coordinates[0]
                },
                object: usoOuConflitoEditado
            });
        }

        let persisteGraphqlDataParams = {
            currentUser: newCurrentUser,
            markers: newMarkersList
        }

        if (route.params && "snackBarMessage" in route.params) {
            persisteGraphqlDataParams = {
                ...persisteGraphqlDataParams,
                snackBarMessage: route.params.snackBarMessage,
            }
            delete route.params.snackBarMessage;
        }

        persisteGraphqlData(persisteGraphqlDataParams);
    };

    const alertTerritorioStatusIdChanged = (newStatusId = null) => {
        const status = (newStatusId) ? newStatusId : state.currentUser.currentTerritorio.statusId;
        const statusIdChanged = (newStatusId != null);

        // These internal status changes don't have to generate an alert
        if (statusIdChanged && [STATUS.ENVIO_PTT_NAFILA, STATUS.ENVIO_ARQUIVOS_PTT_NAFILA].includes(newStatusId)) {
            return;
        }

        const {title, body, urlReferencia} = alertStatusId(status, statusIdChanged);

        const btnEntendi = {
            text: 'Entendi', onPress: async () => {
                recarregaCurrentUser();
                navigation.navigate('Home', {tela: TELAS.MAPA});
            }
        };
        const btnUrlReferencia = {
            text: 'Ir ao site', onPress: async () => {
                openUrlInBrowser(urlReferencia);
                recarregaCurrentUser();
                navigation.navigate('Home', {tela: TELAS.MAPA});
            }
        };

        const botoes = (urlReferencia)
            ? [btnUrlReferencia, btnEntendi]
            : [btnEntendi];

        Alert.alert(
            title,
            body,
            botoes,
            { cancelable: !statusIdChanged },
        );
    };

    ////////////////////////////
    //     gps
    ///////////////////////////

    const alertAppSemPermissoesGPS = (isFalha = false) => {
        setState({carregando: null});
        updateSettings({settings:{useGPS: false}});
        const title = (isFalha)
            ? "Não foi possível encontrar a sua posição"
            : "Aplicativo sem permissões";
        const body = (isFalha)
            ? `Pode ser por falta de cobertura GPS aqui, ou alguma configuração do seu aparelho.`
            : `O Tô no Mapa não pode acessar a localização do seu celular! Para poder usar o GPS, vá para as configurações do celular, e conceda permissão de localização para a equipe do Tô no Mapa.`;
        Alert.alert(
            title,
            body,
            [
                {
                    text: 'Ok', onPress: async () => {
                    }
                }
            ],
            {cancelable: true},
        );
    };

    const locationChanged = (location) => {
        /*DEBUG*/ //console.log('locationChanged');
        if (locationMarker.current && locationMarker.current.props && locationMarker.current.props.coordinate) {
            setState({
                locationMarkerCoordinate: location.coords,
            });
        }
    };

    const locationAtivarAsync = async () => {
        /*DEBUG*/ //console.log('locationAtivarAsync', isLocationMonitoramentoUsuariaAtivo.current);
        if (!isLocationMonitoramentoUsuariaAtivo.current) {
            isLocationMonitoramentoUsuariaAtivo.current = true;
            setState({carregando: 'Ativando o GPS...'});
            let ret = await Location.getForegroundPermissionsAsync();

            if (ret.status !== 'granted') {
                ret = await Location.requestForegroundPermissionsAsync();
            }

            if (ret.status === 'granted') {
                locationMonitoramentoUsuaria.current = await Location.watchPositionAsync(GEOLOCATION_OPTIONS, locationChanged);
                const location = await Location.getCurrentPositionAsync({accuracy: Location.Accuracy.Highest});
                let newState = {carregando: null};
                if (location) {
                    const newCoords = {
                        latitude: location.coords.latitude, longitude: location.coords.longitude
                    };
                    map.current.animateCamera({center: newCoords});
                    setState({
                        carregando: null,
                        locationMarkerCoordinate: newCoords
                    });
                    updateSettings({settings: {useGPS: true}});
                    return;
                }

                console.log('Não foi possível pegar a posição atual com a precisão desejada');
                alertAppSemPermissoesGPS(true);

            } else {
                console.log('Não foram dadas as permissões para GPS!');
                alertAppSemPermissoesGPS();
            }
        }
    };
    const locationAtivar = () => {
        /*DEBUG*/ //console.log('locationAtivar');
        locationAtivarAsync().catch(e => {
            console.log('Erro no retorno do locationAtivarAsync no locationAtivar!', e);
            alertAppSemPermissoesGPS();
        });
    };
    const locationDesativar = () => {
        /*DEBUG*/ //console.log('locationDesativar');
        if (isLocationMonitoramentoUsuariaAtivo.current) {
            if (locationMonitoramentoUsuaria.current) {
                locationMonitoramentoUsuaria.current.remove();
            }
            isLocationMonitoramentoUsuariaAtivo.current = false;
        }
        if (state.locationMarkerCoordinate) {
            setState({locationMarkerCoordinate: null});
        }
    }

    ///////////////////////////////
    //   PROCESSA ROUTE.PARAMS  //
    //////////////////////////////

    if (route.params.firstRun && state.currentUser) {
        delete route.params.firstRun;

        if (state.settings.editing != null) {
            route.params.tela = TELAS.MAPEAR_TERRITORIO;
        }

        updateTerritorioStatusId(state.currentUser.currentTerritorio.id).then((res) => {
            /*DEBUG*/ //console.log('updateTerritorioStatusId', res);
            if (res.dadosMudaram) {
                alertTerritorioStatusIdChanged(res.newStatusId);
            }
        });
    }
    if (route.params.appIntro) {
        delete route.params.appIntro;
        updateSettings({settings: {appIntro: true}, noSnackBarMessage: true});
    }
    if (route.params.tela && state.tela != route.params.tela) {
            updateMonitoramentoLocation(route.params.tela);
            setState({tela: route.params.tela});
        delete route.params.tela;
    }

    if (route.params.gotNotification) {
        const gotNotification = {...route.params.gotNotification};
        delete route.params.gotNotification;
        if (gotNotification.fetchPolicy == 'network-only') {
            const complemento = (state.settings.territorios.length > 1)
                ? '\n\nAtenção: Pode ser que estas mensagens sejam direcionadas para outra comunidade que você esteja mapeando, e não para a atual ' + state.currentUser.currentTerritorio.nome + '!\n\nNeste caso, vá em Configurações e mude a comunidade atual.'
                : '';
            Alert.alert(
                'Novas mensagens da equipe Tô no Mapa',
                'Você recebeu novas mensagens. Para vê-as, você deve enviar os dados atuais ao Tô no Mapa.' + complemento,
                [
                    {
                        text: 'Ir a Enviar Dados', onPress: async () => {
                            navigation.navigate('Home', {tela: TELAS.ENVIAR});
                        }
                    },
                    {
                        text: 'Ok', onPress: async () => {
                        }
                    },
                ],
                {cancelable: true},
            );
        } else if (gotNotification.territorioId == state.currentUser.currentTerritorio.id) {
            persisteCurrentTerritorioStatusId(gotNotification.statusId).then((res) => {
                if (res.dadosMudaram) {
                    alertTerritorioStatusIdChanged(res.newStatusId);
                    recarregaCurrentUser();
                    return;
                }

                Alert.alert(
                    'Nova mensagem da equipe Tô no Mapa',
                    'A nova mensagem é a seguinte:\n\n' + gotNotification.texto,
                    [
                        {
                            text: 'Ver todas as mensagens', onPress: async () => {
                                navigation.navigate('Mensagens', {atualizarDados: true});
                            }
                        },
                        {
                            text: 'Ok', onPress: async () => {
                            }
                        },
                    ],
                    {cancelable: false},
                );
            });
        } else {
            let territorioNome;
            for (let territorio of state.settings.territorios) {
                if (parseInt(territorio.id) == parseInt(gotNotification.territorioId)) {
                    territorioNome = territorio.nome;
                    break;
                }
            }
            Alert.alert(
                'Nova mensagem da equipe Tô no Mapa',
`
A equipe Tô no Mapa enviou uma mensagem para o território ${territorioNome}:

${gotNotification.texto}

Você pode mudar para ${territorioNome} em "Configurações".
`,
                [
                    {
                        text: 'Ir para as Configurações', onPress: async () => {
                            navigation.navigate('Configuracoes', {atualizarDados: true});
                        }
                    },
                    {
                        text: 'Ok', onPress: async () => {
                        }
                    },
                ],
                {cancelable: true},
            );
        }
    }

    if (route.params.ajustaMapaEExportaPDF) {
        delete route.params.ajustaMapaEExportaPDF;
        ajustaMapaEExportaPDF();
    }

    if (route.params.atualizarDados) {
        delete route.params.atualizarDados;
        recarregaCurrentUser();
    }

    if (route.params.usoOuConflitoEditado) {
        persisteUsoOuConflito();
    } else if (route.params.snackBarMessage) {
        snackBarMessage = route.params.snackBarMessage;
        delete route.params.snackBarMessage;
        setSnackBarVisible(true);
    }

    useFocusEffect(
        React.useCallback(() => {
            const onBackPress = () => {
                if (route.params.tela != TELAS.MAPA) {
                    navigation.navigate('Home', {tela: TELAS.MAPA});
                    return true;
                } else {
                    navigation.goBack();
                }
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);

            shouldPingStatusId.current = true;

            const onChangeAppState = (appState) => {
                if (appState.match(/inactive|background/)) {
                    shouldPingStatusId.current = false;
                    locationDesativar();
                    return;
                }

                /*DEBUG*/ //console.log('changed state!', appState);

                shouldPingStatusId.current = true;
                updateMonitoramentoLocation()
                recarregaCurrentUser();
            };
            AppState.addEventListener('change', onChangeAppState);

            return () => {
                shouldPingStatusId.current = false;
                AppState.removeEventListener('change', onChangeAppState);
                BackHandler.removeEventListener('hardwareBackPress', onBackPress);
            };
        }, [route, state, shouldPingStatusId])
    );

    // Load data from local storage
    if (state.currentUser == null) {
        carregaDados();
        shouldCheckStatusChange.current = true;
    } else if (shouldCheckStatusChange.current == true) {
        shouldCheckStatusChange.current = false;

        let mudouStatusId = false;
        validaDados({
            currentUser: state.currentUser,
            settings: state.settings,
            markers: state.markers,
            poligonos: state.polygons
        }).then(situacaoArray => {
            if (!state.enviarDadosDialogo.situacao || !deepEqual(state.enviarDadosDialogo.situacao, situacaoArray)) {
                state.enviarDadosDialogo.situacao = situacaoArray;
                mudouStatusId = true;
            }

            if (mudouStatusId) {
                state.statusMessage = buildStatusMessage(state.currentUser.currentTerritorio.statusId);

                /*DEBUG*/ //console.log('Mudou status para: ' + state.currentUser.currentTerritorio.statusId);
                setState({
                    enviarDadosDialogo: state.enviarDadosDialogo,
                    currentUser: state.currentUser,
                    statusMessage: state.statusMessage
                });
            }

            if (mapLoaded.current) {
                mapFitToElements();
            }
        });
    };

    if (shouldPingStatusId.current && state.currentUser) {
        /*DEBUG*/ //console.log('Entrou shouldPingStatusId');
        shouldPingStatusId.current = false;
        updateTerritorioStatusId(state.currentUser.currentTerritorio.id).then((res) => {
            if (res.dadosMudaram) {
                alertTerritorioStatusIdChanged(res.newStatusId);
            }
        });
    }

    const getNameTipoUsoOuConflito = (marker) => {
        let nameTipoUsoOuConflito;
        if (marker.object.__typename == 'Conflito') {
            for (let i = 0; i < tipos.data.tiposConflito.length; i++) {
                if (marker.object.tipoConflitoId == tipos.data.tiposConflito[i].id) {
                    nameTipoUsoOuConflito = tipos.data.tiposConflito[i].nome;
                    break;
                }
            }
        } else {
            for (let i = 0; i < tipos.data.tiposAreaDeUso.length; i++) {
                if (marker.object.tipoAreaDeUsoId == tipos.data.tiposAreaDeUso[i].id) {
                    nameTipoUsoOuConflito = tipos.data.tiposAreaDeUso[i].nome;
                    break;
                }
            }
        }
        return nameTipoUsoOuConflito;
    };

    // Translate polygons and markers to map
    const gqlTerritorioToMapa = (params) => {
        if (!("currentUser" in params)) {
            return;
        }
        const settings = params.settings;
        const currentUser = params.currentUser;
        let polygons = [];
        let markers = [];
        let todasCoords = {latitude: [], longitude: []};

        // Generate map polygons:
        if (currentUser.currentTerritorio.poligono) {
            for (let poly of currentUser.currentTerritorio.poligono.coordinates) {
                let newPolygon = {
                    id: polygons.length + 1,
                    coordinates: [],
                    holes: [],
                    area: 0
                };
                for (let i = 0; i < poly[0].length - 1; i++) {
                    newPolygon.coordinates.push({latitude: poly[0][i][1], longitude: poly[0][i][0]});
                    todasCoords.latitude.push(poly[0][i][1]);
                    todasCoords.longitude.push(poly[0][i][0]);
                }
                newPolygon.area = calculaAreaPoligono(newPolygon.coordinates, state.settings.unidadeArea);
                polygons.push(newPolygon);
            }
        }

        let markersTipoUsoConflito = [];
        // Generate map markers - conflitos
        for (let conflito of currentUser.currentTerritorio.conflitos) {
            if (conflito.posicao) {
                let icone = geraIconeTipoUsoOuConflito('conflito', conflito.tipoConflitoId);
                let newConflito = {
                    id: markers.length + 1,
                    title: conflito.nome,
                    description: conflito.descricao,
                    coordinate: {
                        latitude: conflito.posicao.coordinates[1],
                        longitude: conflito.posicao.coordinates[0]
                    },
                    icone: icone,
                    object: conflito
                };
                todasCoords.latitude.push(conflito.posicao.coordinates[1]);
                todasCoords.longitude.push(conflito.posicao.coordinates[0]);
                markers.push(newConflito);
                mapMarker.push(null);
                markersTipoUsoConflito.push(getNameTipoUsoOuConflito(newConflito));
            }
        }

        // Generate map markers - locais de uso
        for (let areaDeUso of currentUser.currentTerritorio.areasDeUso) {
            if (areaDeUso.posicao) {
                let icone = geraIconeTipoUsoOuConflito('areaDeUso', areaDeUso.tipoAreaDeUsoId);
                let newAreaDeUso = {
                    id: markers.length + 1,
                    title: areaDeUso.nome,
                    description: areaDeUso.descricao,
                    coordinate: {
                        latitude: areaDeUso.posicao.coordinates[1],
                        longitude: areaDeUso.posicao.coordinates[0]
                    },
                    icone: icone,
                    object: areaDeUso
                };
                todasCoords.latitude.push(areaDeUso.posicao.coordinates[1]);
                todasCoords.longitude.push(areaDeUso.posicao.coordinates[0]);
                markers.push(newAreaDeUso);
                mapMarker.push(null);
                markersTipoUsoConflito.push(getNameTipoUsoOuConflito(newAreaDeUso));
            }
        }

        const {municipio} = resgataEstadoMunicipioDoId(currentUser.currentTerritorio.municipioReferenciaId);
        if (polygons.length > 0 || markers.length > 0) {
            let diffCoords = {latitude: (Math.max(...todasCoords.latitude) - Math.min(...todasCoords.latitude)), longitude: (Math.max(...todasCoords.longitude) - Math.min(...todasCoords.longitude))};
            state.region = {
                latitude: Math.min(...todasCoords.latitude) + diffCoords.latitude/2,
                longitude: Math.min(...todasCoords.longitude) + diffCoords.longitude/2,
                latitudeDelta: diffCoords.latitude + LATITUDE_DELTA/4,
                longitudeDelta: diffCoords.longitude + LONGITUDE_DELTA/4
            }
        } else {
            state.region = {
                latitude: municipio.latitude,
                longitude: municipio.longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            }
        }

        state.statusMessage = buildStatusMessage(currentUser.currentTerritorio.statusId);
        /*DEBUG*/ //console.log("REGION", state.region);
        setState({
            region: state.region,
            statusMessage: state.statusMessage,
            currentUser: currentUser,
            settings: settings,
            polygons,
            markers,
            markersTipoUsoConflito
        });
    };

    ////////////////////////////////
    // TELA MAPEAR_TERRITORIO
    ///////////////////////////////
    const qtdePoligonos = () => {
        let qtde = 0;
        if (state.currentUser && state.currentUser.currentTerritorio.poligono && state.currentUser.currentTerritorio.poligono.coordinates) {
            qtde = state.currentUser.currentTerritorio.poligono.coordinates.length;
        }
        return qtde;
    };

    const apagarTodosPoligonosOuMarkers = () => {
        let title, message, qtde;
        if (state.tela == TELAS.MAPEAR_TERRITORIO) {
            qtde = qtdePoligonos();
            title = pluralize(qtde, 'Apagar a área', 'Apagar todas as áreas');
            message = pluralize(qtde, 'Deseja mesmo apagar a área que você mapeou para delimitar o território? Esta ação é definitiva.', 'Deseja mesmo apagar as ' + qtde + ' áreas que você mapeou para delimitar o território? Esta ação é definitiva.');
        } else {
            qtde = state.markers.length;
            title = pluralize(qtde, 'Apagar o local de uso ou conflito', 'Apagar todos os locais de uso e conflitos');
            message = pluralize(qtde, 'Deseja mesmo apagar o local de uso ou conflito que você registrou? Esta ação é definitiva.', 'Deseja mesmo apagar os ' + qtde + ' locais de uso e conflitos que você registrou? Esta ação é definitiva.');
        }
        Alert.alert(
            title,
            message,
            [
                {
                    text: 'Não, continuar mapeando', onPress: async () => {
                    }
                },
                {
                    text: 'Sim, apagar', onPress: async () => {
                        if (state.tela == TELAS.MAPEAR_TERRITORIO) {
                            persisteGraphqlData({
                                polygons: [],
                                snackBarMessage: pluralize(qtde, "Área apagada com sucesso!", qtde + " áreas apagadas com sucesso!")
                            });
                        } else {
                            let newCurrentUser = cloneCurrentUser(state.currentUser);
                            newCurrentUser.currentTerritorio.conflitos = [];
                            newCurrentUser.currentTerritorio.areasDeUso = [];
                            persisteGraphqlData({
                                currentUser: newCurrentUser,
                                markers: [],
                                snackBarMessage: pluralize(qtde, "Local de uso ou conflito apagado com sucesso!", qtde + " locais de uso ou conflitos apagados com sucesso!")
                            });
                        }
                    }
                },
            ],
            {cancelable: false},
        );
    };

    const apagarPoligonoEmEdicao = () => {
        if (state.settings.editing) {
            Alert.alert(
                "Cancelar a demarcação desta área",
                "Você deseja cancelar o cadastro de nova área que está em edição?",
                [
                    {
                        text: 'Não, continuar mapeando', onPress: async () => {
                        }
                    },
                    {
                        text: 'Sim', onPress: async () => {
                            if (state.settings.editing.originalPolygon) {
                                persisteGraphqlData({
                                    polygons: [...state.polygons, state.settings.editing.originalPolygon],
                                    settings: {editing: null},
                                    snackBarMessage: "A edição da área foi cancelada com sucesso!"
                                });
                            } else {
                                snackBarMessage = "A edição da área foi cancelada com sucesso!";
                                setSnackBarVisible(true);
                                updateSettings({settings: {editing: null}, noSnackBarMessage: true});
                            }
                        }
                    },
                ],
                {cancelable: true},
            );
        }
    };

    // Adiciona vértice ao polígono:
    const adicionaVerticePoligono = (e) => {
        let newEditing;
        if (!state.settings.editing) {
            newEditing = {
                editing: {
                    id: state.polygons.length + 1,
                    coordinates: [e.nativeEvent.coordinate],
                    holes: [],
                    area: 0,
                }
            };
        } else if (!state.creatingHole) {
            const newCoords = [...state.settings.editing.coordinates, e.nativeEvent.coordinate];
            const newArea = calculaAreaPoligono(newCoords, state.settings.unidadeArea);
            newEditing = {
                editing: {
                    ...state.settings.editing,
                    coordinates: newCoords,
                    area: newArea
                },
            };
        } else {
            const holes = [...state.settings.editing.holes];
            holes[holes.length - 1] = [
                ...holes[holes.length - 1],
                e.nativeEvent.coordinate,
            ];
            newEditing = {
                editing: {
                    ...state.settings.editing,
                    id: state.polygons.length + 1,
                    coordinates: [...state.settings.editing.coordinates],
                    holes,
                },
            };
        }
        updateSettings({settings: newEditing, noSnackBarMessage: true});
    };

    // Adicionar vértice GPS ao polígono
    const mapearTerritorioModoGPSAddPonto = (coordsGPS) => {
        if (!state.settings.editing) {
            const newEditing = {
                editing: {
                    id: state.polygons.length + 1,
                    coordinates: [coordsGPS],
                    holes: [],
                    area: 0
                }
            };
            updateSettings({settings: newEditing, noSnackBarMessage: true});
        } else {
            const lastCoord = state.settings.editing.coordinates[state.settings.editing.coordinates.length - 1];
            if (lastCoord.latitude == coordsGPS.latitude && lastCoord.longitude == coordsGPS.longitude) {
                Alert.alert(
                    'Ponto já adicionado',
                    'Você já adicionou este ponto.',
                    [
                        {
                            text: 'Ok'
                        }
                    ],
                    {cancelable: true},
                );
            } else {
                snackBarMessage = "Sua posição foi adicionada com sucesso";
                setSnackBarVisible(true);
                const newCoords = [...state.settings.editing.coordinates, coordsGPS];
                const newArea = calculaAreaPoligono(newCoords, state.settings.unidadeArea);
                const newEditing = {
                    editing: {
                        ...state.settings.editing,
                        coordinates: newCoords,
                        area: newArea
                    }
                };
                updateSettings({settings: newEditing, noSnackBarMessage: true});
            }
        }
    };

    const marcarPontoSalvar = (ponto) => {
        const {markers} = state;
        const marker = {
            id: markers.length + 1,
            ...ponto,
        };
        setState({
            markers: [...markers, ponto],
        });
    };

    /* Marca ponto de uso ou conflito */
    const marcarPonto = (e) => {
        navigation.navigate('EditarUsoConflito', {
            usoOuConflito: {
                posicao: {
                    type: 'Point',
                    coordinates: [e.nativeEvent.coordinate.longitude, e.nativeEvent.coordinate.latitude]
                }
            },
        });
    };

    const editarPonto = (usoOuConflito, index = null) => {
        if (index !== null) {
            mapMarker[index].hideCallout();
            calloutVisible.current = false;
        }
        navigation.navigate('EditarUsoConflito', {
            usoOuConflito
        })
    };

    // Marcar ponto ou conflito usando GPS
    const mapearUsoConflitoModoGPSAddPonto = (coordsGPS) => {
        const lastCoord = (state.markers[state.markers.length-1])
            ? state.markers[state.markers.length-1].coordinate
            : null;
        if (lastCoord && lastCoord.latitude == coordsGPS.latitude && lastCoord.longitude == coordsGPS.longitude) {
            Alert.alert(
                'Ponto já adicionado',
                'Você já adicionou este ponto.',
                [
                    {
                        text: 'Ok'
                    }
                ],
                {cancelable: true},
            );
        } else {
            navigation.navigate('EditarUsoConflito', {
                usoOuConflito: {
                    posicao: {
                        type: 'Point',
                        coordinates: [coordsGPS.longitude, coordsGPS.latitude]
                    }
                },
            });
        }
    };

    ///////////////////////////////
    // Generic utils:
    ///////////////////////////////

    const mapMovedOrTouched = (e) => {
        if (!calloutVisible.current) {
            if (state.tela === TELAS.MAPEAR_TERRITORIO ) {
                adicionaVerticePoligono(e);
            } else if (state.tela === TELAS.MAPEAR_USOSECONFLITOS) {
                marcarPonto(e);
            }
        } else if (calloutVisible.current) {
            calloutVisible.current = false;
        }
    };

    const mapOptions = {
        scrollEnabled: true,
    };

    let statusMapeamentoTxt = "";
    let appBarTitle = null;
    switch (state.tela) {
        case TELAS.MAPEAR_TERRITORIO:
            appBarTitle = "Mapear o território";
            if (
                (
                    !state.settings.editing ||
                    !state.settings.editing.coordinates.length ||
                    state.settings.editing.coordinates.length == 0
                ) && qtdePoligonos() == 0
            ) {
                statusMapeamentoTxt = (state.locationMarkerCoordinate)
                    ? "Toque no mapa ou adicione sua posição atual para delimitar a área"
                    : "Toque no mapa para delimitar a área (Deve ter mais que 3 pontos)";
            } else {
                if (qtdePoligonos() > 0) {
                    let areaTxt = ' (' + formataArea(areaPoligonos(), state.settings.unidadeArea) + '). ';
                    statusMapeamentoTxt = qtdePoligonos() + pluralize(qtdePoligonos(), ' área salva', ' áreas salvas') + areaTxt;
                }
                if (state.settings.editing && state.settings.editing.coordinates.length) {
                    //statusMapeamentoTxt += "Marcando nova área: " + state.settings.editing.coordinates.length + pluralize(state.settings.editing.coordinates.length, " vértice."," vértices.");
                    statusMapeamentoTxt += "Nova área em edição";
                    if (state.settings.editing.coordinates.length >= 3) {
                        let area = calculaAreaPoligono(state.settings.editing.coordinates, state.settings.unidadeArea, true);
                        statusMapeamentoTxt += ' (' + area + ')';
                    }
                }
            }
            break;
        case TELAS.MAPEAR_USOSECONFLITOS:
            appBarTitle = "Locais de Uso e Conflitos";
            if (!state.markers.length) {
                statusMapeamentoTxt = (state.locationMarkerCoordinate)
                    ? "Toque no mapa ou adicione sua posição atual para registrar um local de uso ou conflito"
                    : "Toque no mapa para registrar um local de uso ou conflito";
            } else {
                statusMapeamentoTxt = "Você já registrou " + state.markers.length + pluralize(state.markers.length, " local de uso ou conflito", " locais de uso e conflitos");
            }
            break;
        case TELAS.ENVIAR:
            appBarTitle = "Enviar dados ao Tô no mapa";
    }

    /**
     * Controle de mensagens de erro ou sucesso ao aplicar o envio de dados locais para o dashboard (nuvem)
     * @method aplicaEnviaParaDashboard
     */
    const aplicaEnviaParaDashboard = (enviarParaRevisao) => {
        setState({enviarDadosDialogo: enviarDadosDialogoCarregando});
        enviaParaDashboard({
            currentUser: state.currentUser,
            appFiles: state.settings.appFiles,
            enviarParaRevisao: enviarParaRevisao
        }).then((res) => {
            if (res) {
                // res devolve res.settings, res.token e res.currentUser
                /*DEBUG*/ //console.log("resposta enviaParaDashboard", res);

                // /**
                //  * @todo: ATENÇÃO: Em setembro de 2021 será necessário apagar este comando de signUp, pois a partir de agora, quem entra no app já tem usuária criada! Fica ainda aqui um tempo para resolver questão de compatibilidade com versões anteriores!
                //  */
                // if (res.token) {
                //     signUp(res.token);
                // }

                let stateChange = {
                    enviarDadosDialogo : {
                        ...enviarDadosDialogoErro,
                        situacao: 'erro'
                    }
                };
                if (res.currentUser) {
                    snackBarMessage = "Dados enviados com sucesso ao Tô no Mapa!";
                    stateChange = {
                        ...stateChange,
                        enviarDadosDialogo: {
                            ...stateChange.enviarDadosDialogo,
                            etapa: 'sucesso',
                            enviarParaRevisao: enviarParaRevisao
                        },
                        snackBarVisible: true,
                        currentUser: null // force data reload and status update
                    };
                } else {
                    stateChange.enviarDadosDialogo.apiErrors = {
                        ...stateChange.enviarDadosDialogo.apiErrors,
                        ...res.apiErrors
                    };
                }

                setState(stateChange);
            } else {
                // TODO: que casos dá aqui?
                console.log('Erro no enviaParaDashboard (res nulo)', res);
                setState({
                    enviarDadosDialogo: {
                        ...enviarDadosDialogoErro,
                        apiErrors: {
                            ...enviarDadosDialogoErro.apiErrors,
                            graphQLErrors: ['Falha desconhecida (retorno nulo de enviaParaDashboard)']
                        }
                    }
                });
            }
        },(e) => {
            // Uma exceção foi lançada no interior do enviaParaDashboard:
            console.log('Exceção lançada de dentro do enviaParaDashboard', e.message);
            setState({
                enviarDadosDialogo: {
                    ...enviarDadosDialogoErro,
                    apiErrors: {
                        ...enviarDadosDialogoErro.apiErrors,
                        graphQLErrors: [`Erro lançado de dentro de enviaParaDashboard: ${e.message}`]
                    }
                }
            });
        });
    };

    const validaDashBoardMode = (enviarParaRevisao) => {
        if (!enviarParaRevisao) {
            aplicaEnviaParaDashboard(enviarParaRevisao);
            return;
        }

        const titleAlert = (!isPTTModeVar)
            ? 'Tem certeza que quer enviar os dados ao Tô no mapa?'
            : 'Tem certeza que quer enviar os dados para a PTT?';

        const bodyAlert = (!isPTTModeVar)
            ? 'Após o envio, a equipe Tô no Mapa vai analisar os dados enviados.\n\nVocê não poderá alterar os dados enquanto não houver um retorno da equipe do Tô no Mapa.\n\nTem certeza?'
            : 'Você não poderá desfazer esta ação. Tem certeza?';

        Alert.alert(
            titleAlert,
            bodyAlert,
            [
                {
                    text: 'Não'
                },
                {
                    text: 'Sim, enviar',
                    onPress: async () => {
                        aplicaEnviaParaDashboard(true);
                    }
                },
            ],
            {cancelable: true},
        );
    };

    const isEnviarEnabledVar = isEnviarEnabled(state.currentUser);
    const isEnviarPTTEnabledVar = isEnviarPTTEnabled(state.currentUser);
    const isCurrentTerritorioEditableVar = isCurrentTerritorioEditable(state.currentUser);
    const isPTTModeVar = isPTTMode(state.currentUser);

    const iconeTipo = (marker) => {
        return marker.object.__typename === 'Conflito'
            ? ICONE_TIPO.conflito
            : ICONE_TIPO.areaDeUso;
    };


    let mapTypeStyles = {
        label: "Mapa",
        polygonFillColor: colors.polygonBackgroundHybrid,
        polygonEditingStrokeColor: colors.background,
        backgroundMarkerView: "white",
        logoImageToShow: LOGO_IMAGE.branca,
        markerIconToShow: ICONE_TIPO.locationMarkClaro,
        iconButtonStyle: LayoutMapa.mapearStatusButtonClaro
    };
    if (Constants.manifest.extra.xyzRTiles.using || state.settings.mapType != MAP_TYPES.HYBRID) {
        mapTypeStyles = {
            label:  "Satélite",
            polygonFillColor: colors.polygonBackgroundStandard,
            polygonEditingStrokeColor: colors.text,
            backgroundMarkerView: "black",
            logoImageToShow: LOGO_IMAGE.colorida,
            markerIconToShow: ICONE_TIPO.locationMark,
            iconButtonStyle: LayoutMapa.mapearStatusButton
        };
    }

    const renderSlideNext = () => {
        return (
            <SlideWidget type="Next"/>
        );
    }
    const renderSlidePrev = () => {
        return (
            <SlideWidget type="Prev"/>
        );
    }
    const renderSlideDone = () => {
        return (
            <SlideWidget type="Done"/>
        );
    }

    /*DEBUG*/ //console.log(state);

    //////////////////////
    // RENDER!
    /////////////////////
    return (
        <React.Fragment>
        {(state.settings.appIntro) &&
            <React.Fragment>
            <Portal>
                <Dialog
                    visible={state.introDialog}
                    onDismiss={() => {
                        setState({introDialog: false});
                    }}>
                    <Dialog.Title>Boas vindas ao Tô no Mapa!</Dialog.Title>
                    <Dialog.Content>
                        <View style={Layout.col}>
                            <Text style={Layout.textParagraph}>Você deseja conhecer como funciona o aplicativo?</Text>
                        </View>
                    </Dialog.Content>
                    <Dialog.Actions style={{justifyContent: 'space-between'}}>
                        <Button onPress={() => {
                            updateSettings({settings: {appIntro: false}, noSnackBarMessage: true});
                        }}>Não</Button>
                        <Button onPress={() => {
                            setState({introDialog: false});
                        }}>Sim!</Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>

            <AppIntroSlider
                renderItem={renderSlide}
                data={SLIDES}
                renderNextButton={renderSlideNext}
                renderPrevButton={renderSlidePrev}
                renderDoneButton={renderSlideDone}
                showSkipButton={true}
                skipLabel="PULAR"
                dotStyle={{backgroundColor:"#666"}}
                onDone={() => {
                    updateSettings({settings: {appIntro: false}, noSnackBarMessage: true});
                }}
            />
            </React.Fragment>
        }
        {!state.settings.appIntro &&
        <View style={(state.preparandoShot && 1 != 1) ? styles.containerShot : styles.container}>
            <MapView
                ref={map}
                provider={Constants.manifest.extra.xyzRTiles.using ? null : PROVIDER_GOOGLE}
                style={LayoutMapa.map}
                mapType={
                    Constants.manifest.extra.xyzRTiles.using
                    ? MAP_TYPES.NONE
                    : state.settings.mapType
                }
                initialRegion={state.region}
                onLayout={() => {
                    if (state.polygons.length || state.markers.length) {
                        mapFitToElements();
                    }
                    mapLoaded.current = true;
                }}
                zoomControlEnabled={false}
                onPress={e => mapMovedOrTouched(e)}
                {...mapOptions}
            >

                {
                    Constants.manifest.extra.xyzRTiles.using ? (
                        // renders only if the app is configured to use an XYZ raster tile
                        <MapView.UrlTile
                            urlTemplate={Constants.manifest.extra.xyzRTiles.urlTile}
                            shouldReplaceMapContent={true}
                        />
                    ) : null
                }

                {state.locationMarkerCoordinate && !state.preparandoShot &&
                <Marker
                    key='locationMarker'
                    ref={locationMarker}
                    coordinate={state.locationMarkerCoordinate}
                    onPress={() => {
                        calloutVisible.current = true;
                    }}
                >
                    <Image
                        source={mapTypeStyles.markerIconToShow}
                        style={Layout.icon}
                        resizeMode="contain"
                    />
                    <Callout
                        onPress = {(props) => {
                            let coordsGPS = state.locationMarkerCoordinate;
                            locationMarker.current.hideCallout();
                            if (state.tela == TELAS.MAPEAR_TERRITORIO) {
                                mapearTerritorioModoGPSAddPonto(coordsGPS)
                            } else {
                                mapearUsoConflitoModoGPSAddPonto(coordsGPS);
                            }
                        }}
                    >
                        <View style={[Layout.col, {width:metrics.tenWidth*16}]}>
                            <Text style={Layout.calloutActionCenter}>Clique para adicionar</Text>
                            <Text style={Layout.calloutActionCenter}>sua posição</Text>
                        </View>
                    </Callout>
                </Marker>
                }
                {state.polygons.map((polygon, index) => (
                    <Polygon
                        key={polygon.id}
                        ref={(ref) => mapPolygon[index] = ref}
                        coordinates={polygon.coordinates}
                        holes={polygon.holes}
                        strokeColor={colors.warning}
                        fillColor={mapTypeStyles.polygonFillColor}
                        strokeWidth={3}
                        tappable={state.tela != TELAS.MAPEAR_USOSECONFLITOS}
                        zIndex={2}
                        onPress={(e) => {
                            if (!isCurrentTerritorioEditableVar) {
                                return;
                            }

                            calloutVisible.current = true;
                            const alertApagarArea = () => {
                                Alert.alert(
                                    'Apagar área',
                                    'Tem certeza que quer apagar esta área de ' + formataArea(polygon.area, state.settings.unidadeArea) + '? Esta operação não tem volta',
                                    [
                                        {
                                            text: 'Não'
                                        },
                                        {
                                            text: 'Sim, quero apagar esta área', onPress: async () => {
                                                state.polygons.splice(index, 1);
                                                persisteGraphqlData({
                                                    polygons: state.polygons,
                                                    snackBarMessage: "Área da comunidade apagada com sucesso!"
                                                });
                                            }
                                        },
                                    ],
                                    {cancelable: true},
                                );
                            };
                            if (state.settings.editaPoligonos) {
                                let buttons = [
                                    {
                                        text: 'Cancelar'
                                    },
                                    {
                                        text: 'Apagar a área', onPress: async () => {
                                            alertApagarArea();
                                        }
                                    },
                                    {
                                        text: 'Editar a área', onPress: async () => {
                                            let polygon = state.polygons.splice(index, 1);
                                            persisteGraphqlData({
                                                polygons: state.polygons,
                                                settings: {
                                                    editing: {
                                                        id: polygon[0].id,
                                                        coordinates: polygon[0].coordinates,
                                                        holes: polygon[0].holes,
                                                        area: polygon[0].area,
                                                        originalPolygon: polygon[0]
                                                    }
                                                }
                                            });
                                            navigation.navigate('Home', {tela: TELAS.MAPEAR_TERRITORIO});
                                        }
                                    }
                                ];
                                if (state.tela == TELAS.MAPEAR_TERRITORIO) {
                                    buttons.push({
                                        text: 'Adicionar vértice de nova área', onPress: async () => {
                                            adicionaVerticePoligono(e);
                                        }
                                    });
                                }
                                Alert.alert(
                                    'Editar ou apagar área',
                                    'Você pode voltar a editar esta área (' + formataArea(polygon.area, state.settings.unidadeArea) + ') ou apagá-la. O que deseja fazer?',
                                    buttons,
                                    {cancelable: true},
                                );
                                return;
                            }

                            alertApagarArea();
                        }}
                    />
                ))}
                {!state.preparandoShot && state.markers.map((marker, index) => (
                    <Marker
                        key={marker.id}
                        ref={(ref) => mapMarker[index] = ref}
                        coordinate={marker.coordinate}
                        title={marker.title}
                        description={marker.description}
                        onPress={() => {
                            calloutVisible.current = true;
                        }}
                        draggable
                        onDragEnd={(e) => {
                            if (!isCurrentTerritorioEditableVar) {
                                return;
                            }

                            let usoOuConflitoEditado = {
                                ...state.markers[index].object,
                                posicao: {
                                    ...state.markers[index].object.posicao,
                                    coordinates: [
                                        e.nativeEvent.coordinate.longitude,
                                        e.nativeEvent.coordinate.latitude
                                    ]
                                }
                            };
                            navigation.navigate('Home', {
                                usoOuConflitoEditado: usoOuConflitoEditado,
                                tela: state.tela,
                                usoOuConflitoMoveu: true,
                                snackBarMessage: marker.object.__typename == 'Conflito' ? "Conflito movido com sucesso" : "Local de uso movido com sucesso"
                            });
                        }}
                    >
                        <Image
                            source={marker.icone}
                            style={Layout.icon}
                            resizeMode="contain"
                        />
                        <Callout
                            onPress={() => {
                                if (isCurrentTerritorioEditableVar) {
                                    editarPonto(marker.object, index);
                                }
                            }}
                        >
                            <View style={[Layout.col, {width:metrics.tenWidth*16}]}>
                                {(marker.title != null && marker.title.trim() != '') &&
                                    <Text style={Layout.calloutTitle}>
                                        {marker.title}
                                    </Text>
                                }
                                <Text style={Layout.calloutDescription}>
                                    <>
                                        {marker.object.__typename == 'Conflito'
                                            ? <Text>Conflito: {state.markersTipoUsoConflito[index]}</Text>
                                            : <Text>Uso: {state.markersTipoUsoConflito[index]}</Text>
                                        }
                                    </>
                                </Text>
                                {'area' in marker.object && parseFloat(marker.object.area) > 0 &&
                                    <Text style={Layout.calloutDescription}>
                                        Área: {formataArea(marker.object.area, UNIDADES_AREA.METRO_QUADRADO)}
                                    </Text>
                                }
                                {(marker.object.descricao != null && marker.object.descricao.trim() != '') &&
                                    <Text style={Layout.calloutDescriptionSub}>
                                        {marker.object.descricao}
                                    </Text>
                                }

                                {isCurrentTerritorioEditableVar &&
                                    <Text style={Layout.calloutAction}>Editar ou apagar</Text>
                                }
                            </View>
                        </Callout>
                    </Marker>
                ))}
                {state.preparandoShot && state.markers.map((marker, index) => (
                    <Marker
                        key={marker.id}
                        ref={(ref) => mapMarker[index] = ref}
                        coordinate={marker.coordinate}
                    >
                        <Image
                            source={marker.icone}
                            style={Layout.icon}
                            resizeMode="contain"
                        />
                    </Marker>
                ))}
                {state.settings.editing &&
                    <React.Fragment>
                        <Polygon
                            key="mapEditingPolygon"
                            ref={mapEditingPolygon}
                            coordinates={state.settings.editing.coordinates}
                            holes={state.settings.editing.holes}
                            strokeColor={mapTypeStyles.polygonEditingStrokeColor}
                            fillColor="rgba(255,0,0,0.5)"
                            strokeWidth={3}
                            tappable={state.tela == TELAS.MAPA}
                            onPress={() => {
                                navigation.navigate('Home', {tela: TELAS.MAPEAR_TERRITORIO})
                            }}
                        />
                        {!state.settings.editaPoligonos && state.tela == TELAS.MAPEAR_TERRITORIO && state.settings.editing.coordinates.map((coord, index) => (
                            <Marker
                                key={index}
                                coordinate={coord}
                                centerOffset={{x:0.5,y:0.5}}
                                anchor={{x:0.5,y:0.5}}
                            >
                                {(index == 0 || index == state.settings.editing.coordinates.length - 1) &&
                                   <MaterialCommunityIcons name="circle" size={metrics.tenWidth*2} style={{paddingTop: 0, paddingLeft: 0, color:(index == 0) ? colors.primary : colors.danger}}/>
                                }
                            </Marker>
                        ))}
                        {state.settings.editaPoligonos && state.tela == TELAS.MAPEAR_TERRITORIO && state.settings.editing.coordinates.map((coord, index) => (
                            <Marker
                                key={index}
                                ref={(ref) => mapEditingPolygonEdge[index] = ref}
                                coordinate={coord}
                                onPress={() => {
                                    calloutVisible.current = true;
                                }}
                                centerOffset={{x:0.5,y:0.5}}
                                anchor={{x:0.5,y:0.5}}
                                draggable
                                onDragEnd={(e) => {
                                    let coords = [...state.settings.editing.coordinates];
                                    coords[index] = e.nativeEvent.coordinate;
                                    const newEditing = {
                                        editing: {
                                            ...state.settings.editing,
                                            coordinates: coords
                                        }
                                    };
                                    updateSettings({settings: newEditing, noSnackBarMessage: true});
                                }}
                            >
                                <View style={[Layout.vertice, {backgroundColor: mapTypeStyles.backgroundMarkerView}]}>
                                    {(index == 0 || index == state.settings.editing.coordinates.length - 1) &&
                                        <MaterialCommunityIcons name="circle" size={metrics.tenWidth * 2} style={{ paddingTop: 0, paddingLeft: 0, color: (index == 0) ? colors.primary : colors.danger }} />
                                    }
                                </View>
                                <Callout
                                    onPress = {() => {
                                        Alert.alert(
                                            'Apagar vértice',
                                            'Tem certeza que quer apagar este ponto da área da comunidade?',
                                            [
                                                {
                                                    text: 'Não'
                                                },
                                                {
                                                    text: 'Sim, apagar', onPress: async () => {
                                                        mapEditingPolygonEdge[index].hideCallout();
                                                        calloutVisible.current = false;
                                                        let coords = [...state.settings.editing.coordinates];
                                                        coords.splice(index, 1);
                                                        const newEditing = {
                                                            editing: {
                                                                ...state.settings.editing,
                                                                coordinates: coords
                                                            }
                                                        };
                                                        updateSettings({settings: newEditing, noSnackBarMessage: true});
                                                    }
                                                },
                                            ],
                                            {cancelable: true},
                                        );
                                    }}
                                >
                                    <View style={{...Layout.col, minWidth:metrics.tenWidth*10}}>
                                        <Text style={{...Layout.calloutTitle, flex:1}}>
                                            {'Vértice nº ' + (index+1)}
                                        </Text>
                                        {index == (state.settings.editing.coordinates.length - 1) &&
                                            <Text style={Layout.calloutDescription}>
                                                Último vértice
                                            </Text>
                                        }
                                        <Text style={Layout.calloutAction}>
                                            Apagar
                                        </Text>
                                    </View>
                                </Callout>
                            </Marker>
                        ))}
                    </React.Fragment>
                }
                {state.editingMarker && (
                    <Marker
                        key={state.editingMarker.id}
                        coordinate={state.editingMarker.coordinate}
                        title={state.editingMarker.title}
                        description={state.editingMarker.description}
                        pinColor={'tan'}
                    />
                )}
            </MapView>
            {isTelaMapeamento(state.tela) &&
            <React.Fragment>
                <View style={{position: 'absolute', top: 0, width: "100%"}}>
                    <AppbarTonomapa
                        navigation={navigation}
                        title={appBarTitle}
                        rightAction={{
                            icon:'crosshairs-gps',
                            color:(state.locationMarkerCoordinate) ? colors.primary : colors.divider,
                            fn: () => {
                                if (state.locationMarkerCoordinate) {
                                    Alert.alert(
                                        'Posição por GPS ativada',
                                        'Você pode adicionar pontos no mapa a partir da sua posição atual, usando a funcionalidade de localização do seu celular. Deseja desativar esta opção?',
                                        [
                                            {
                                                text: 'Não quero'
                                            },
                                            {
                                                text: 'Sim, quero desativar o GPS', onPress: async () => {
                                                    updateSettings({settings: {useGPS: false}});
                                                    locationDesativar();
                                                }
                                            },
                                        ],
                                        {cancelable: true},
                                    );
                                } else {
                                    Alert.alert(
                                        'Posição por GPS desativada',
                                        'Você pode adicionar pontos no mapa a partir da sua posição atual, usando a funcionalidade de localização do seu celular. Esta função está desativada. Quer ativá-la?',
                                        [
                                            {
                                                text: 'Não quero'
                                            },
                                            {
                                                text: 'Sim, quero ativar GPS', onPress: async () => {
                                                    locationAtivar();
                                                }
                                            },
                                        ],
                                        {cancelable: true},
                                    );
                                }
                            }
                        }}
                    />
                    {statusMapeamentoTxt != '' &&
                        <View style = {Layout.statusBar}>
                            <Text style={Layout.statusBarText}>{statusMapeamentoTxt}</Text>
                        </View>
                    }
                </View>
            </React.Fragment>
            }
            {isTelaMapeamento(state.tela) &&
            <View style={Layout.col}>
                {state.locationMarkerCoordinate &&
                <View style={{...LayoutMapa.mapearButtonsContainer, display: 'none'}}>
                    <Button
                        mode="contained"
                        onPress={(state.tela == TELAS.MAPEAR_TERRITORIO) ? mapearTerritorioModoGPSAddPonto : mapearUsoConflitoModoGPSAddPonto}
                        color={colors.white}
                        style={LayoutMapa.mapearButtons}
                        contentStyle={{height: metrics.tenWidth*4.5}}
                        labelStyle={{...LayoutMapa.mapearButtonsLabel, color:colors.text}}
                        icon={() => (
                            <Avatar.Icon size={metrics.tenWidth*3.6} icon="compass" style={LayoutMapa.avatarButtonModoInativo}/>
                        )}
                    >
                        Adicionar posição atual
                    </Button>
                </View>
                }

                <View style={{flexDirection: 'row'}}>
                    <View style={LayoutMapa.mapearButtonsContainerRow}>
                        <Button
                            mode="contained"
                            onPress={
                                state.settings.editing ? apagarPoligonoEmEdicao : apagarTodosPoligonosOuMarkers
                            }
                            disabled={(state.tela == TELAS.MAPEAR_TERRITORIO && !state.settings.editing && !qtdePoligonos()) || (state.tela == TELAS.MAPEAR_USOSECONFLITOS && !state.markers.length)}
                            color={colors.danger}
                            style={LayoutMapa.mapearButtonsLeft}
                            contentStyle={{height: metrics.tenWidth*4.5}}
                            labelStyle={{...LayoutMapa.mapearButtonsLabel}}
                            icon={({ size, color, direction }) => (
                                <Avatar.Icon size={metrics.tenWidth*3.6} icon={state.settings.editing ? "cancel" : "broom"} style={LayoutMapa.avatarButtonExit}/>
                            )}>
                                {state.settings.editing ? <Text>Cancelar</Text> : <Text>Apagar tudo</Text>}
                        </Button>

                        <Button
                            mode="contained"
                            disabled={(state.tela == TELAS.MAPEAR_TERRITORIO && (state.settings.editing && state.settings.editing.coordinates.length < 3))}
                            onPress={() => {
                                switch (state.tela) {
                                    case TELAS.MAPEAR_TERRITORIO:
                                        if (state.settings.editing) {
                                            persisteGraphqlData({
                                                polygons: [...state.polygons, state.settings.editing],
                                                settings: {editing: null},
                                                snackBarMessage: "Área da comunidade salva com sucesso!"
                                            });
                                        } else {
                                            navigation.navigate('Home', {tela: TELAS.MAPA});
                                        }
                                        break;
                                    case TELAS.MAPEAR_USOSECONFLITOS:
                                        navigation.navigate('Home', {tela: TELAS.MAPA});
                                        break;
                                }
                            }}
                            color={colors.primary}
                            style={LayoutMapa.mapearButtonsRight}
                            contentStyle={{height: metrics.tenWidth*4.5}}
                            labelStyle={LayoutMapa.mapearButtonsLabel}
                            icon={({ size, color, direction }) => (
                                <Avatar.Icon size={metrics.tenWidth*3.6} icon="check" style={LayoutMapa.avatarButtonDone}/>
                            )}>
                            {state.settings.editing ? "Salvar" : "Feito!"}
                        </Button>
                    </View>
                </View>
            </View>
            }
            {state.currentUser && state.tela === TELAS.ENVIAR &&
            <Portal>
                <Dialog
                    visible={true}
                    onDismiss={() => {
                        shouldCheckStatusChange.current = true;
                        setState({enviarDadosDialogo: initialState.enviarDadosDialogo});
                        navigation.navigate('Home', {tela: TELAS.MAPA});
                    }}>
                    <Dialog.Title>{!isPTTModeVar ? 'Enviar ao Tô no Mapa' : 'Enviar para a PTT'}</Dialog.Title>
                    <Dialog.Content>
                        <Text style={Layout.enviarTitle}>{state.currentUser.currentTerritorio.nome}</Text>
                        {state.enviarDadosDialogo.etapa == 'situacao' && !state.settings.editing && isEnviarEnabledVar &&
                        state.enviarDadosDialogo.situacao.map((resultado, index) => {
                            return <View style={Layout.row} key={index}>
                                <Avatar.Icon size={metrics.tenWidth*3.6} icon={resultado.icon}
                                    color={resultado.iconStyle.color}
                                    style={resultado.iconStyle}
                                /><Text>{resultado.text}</Text>
                                </View>
                        })
                        }
                        {state.enviarDadosDialogo.etapa == 'situacao' && !state.settings.editing && isEnviarEnabledVar && isPTTModeVar && state.enviarDadosDialogo.situacao.some(item => item.pttError) &&
                            <Text style={Layout.row}>
                                Não é possível enviar para a PTT. Por favor, completemente as informações que faltam (círculos vermelhos acima).
                            </Text>
                        }
                        {state.enviarDadosDialogo.etapa == 'situacao' && state.settings.editing &&
                        <View style={Layout.colLeft}>
                            <Text style={Layout.textParagraph}>Ops: você tem uma área sendo editada.</Text>
                            <Text style={Layout.textParagraph}>Por favor, vá em "mapear território" para salvar esta área ou então cancelar a edição.</Text>
                        </View>
                        }
                        {state.enviarDadosDialogo.etapa == 'situacao' && !isEnviarEnabledVar &&
                        <View style={Layout.colLeft}>
                            <Text style={Layout.textParagraph}>Dados sincronizados com o servidor:</Text>
                            <Text style={Layout.textParagraph}>Você não alterou nenhuma informação desde a última vez que enviou os dados ao Tô no Mapa.</Text>
                        </View>
                        }
                        {state.enviarDadosDialogo.etapa == 'carregando' &&
                        <View style={Layout.rowCenter}>
                            <ActivityIndicator animating={true} color={colors.primary} />
                        </View>
                        }
                        {state.enviarDadosDialogo.etapa == 'sucesso' && state.enviarDadosDialogo.enviarParaRevisao &&
                        <View style={Layout.col}>
                            <Text style={Layout.textParagraph}>As informações foram enviadas com sucesso para revisão pela equipe Tô no Mapa.</Text>
                            <Text style={Layout.textParagraph}>Não desinstale o app, pois é nele que vamos responder se as informações foram aceitas ou se são necessárias alterações.</Text>
                            <Text style={Layout.textParagraph}>Em breve daremos a resposta.</Text>
                        </View>
                        }
                        {state.enviarDadosDialogo.etapa == 'sucesso' && !state.enviarDadosDialogo.enviarParaRevisao &&
                        <View style={Layout.col}>
                            <Text style={Layout.textParagraph}>Os dados e anexos da comunidade foram salvos na nuvem do Tô no Mapa.</Text>
                            <Text style={Layout.textParagraph}>Você pode seguir editando normalmente as informações.</Text>
                            <Text style={Layout.textParagraph}>Quando finalizar o mapeamento, envie novamente ao Tô no Mapa e escolha a opção de enviar para análise.</Text>
                        </View>
                        }
                        {state.enviarDadosDialogo.etapa == 'erro' &&
                        <View style={Layout.col}>
                            <Text style={{...Layout.textParagraph,...Layout.bold}}>Dados não enviados.</Text>
                            {(state.enviarDadosDialogo.apiErrors.networkError != "" && state.enviarDadosDialogo.apiErrors.graphQLErrors.length == 0) &&
                                <React.Fragment>
                                    <Text style={Layout.textParagraph}>Falha na conexão com a internet durante o envio.</Text>
                                    <Text style={{...Layout.textParagraph, ...Layout.italic}}>
                                        {state.enviarDadosDialogo.apiErrors.networkError}
                                    </Text>
                                    <Text style={Layout.textParagraph}>Por favor, se a internet está boa, entre em contato com nossa equipe de apoio e compartilhe o texto do erro acima.</Text>
                                </React.Fragment>
                            }
                            {state.enviarDadosDialogo.apiErrors.graphQLErrors.length > 0 &&
                                <React.Fragment>
                                    <Text style={Layout.textParagraph}>Infelizmente encontramos um erro ao tentar enviar os dados. Se possível, tente mais uma vez, e de não der certo novamente, pedimos que envie um print desta mensagem para a equipe Tô no Mapa:</Text>
                                    <Text style={{...Layout.textParagraph, ...Layout.italic}}>
                                        {state.enviarDadosDialogo.apiErrors.graphQLErrors.join(', ')}
                                    </Text>
                                </React.Fragment>
                            }
                        </View>
                        }
                    </Dialog.Content>
                    <Dialog.Actions
                        style={(state.enviarDadosDialogo.etapa == 'situacao' && !isPTTModeVar)
                            ? Layout.dialogButtonsMultiLine
                            : Layout.dialogButtons
                        }
                    >
                        {!state.settings.editing && isEnviarEnabledVar && state.enviarDadosDialogo.botaoCancelar &&
                        <Button onPress={() => {
                            shouldCheckStatusChange.current = true;
                            setState({enviarDadosDialogo: initialState.enviarDadosDialogo});
                            navigation.navigate('Home', {tela: TELAS.MAPA});
                        }}>Cancelar</Button>
                        }
                        {!state.settings.editing && isEnviarEnabledVar && state.enviarDadosDialogo.botaoEnviar && !state.enviarDadosDialogo.situacao.some(item => item.pttError) &&
                            <>
                                {!isPTTModeVar &&
                                    <>
                                        <Button onPress={() => {
                                            validaDashBoardMode(false);
                                        }}>Enviar e seguir editando</Button>
                                        <Button onPress={() => {
                                            validaDashBoardMode(true);
                                        }}>Enviar para análise!</Button>
                                    </>
                                }
                                {isPTTModeVar &&
                                    <Button onPress={() => {
                                        validaDashBoardMode(true);
                                    }}>Enviar</Button>
                                }
                            </>
                        }
                        {state.enviarDadosDialogo.etapa == 'sucesso' && !isPTTModeVar &&
                            <Button onPress={() => {
                                shouldCheckStatusChange.current = true;
                                setState({enviarDadosDialogo: initialState.enviarDadosDialogo});
                                navigation.navigate('Home', {tela: TELAS.MAPA, ajustaMapaEExportaPDF: true});
                            }}>Exportar relatório</Button>
                        }
                        {(state.settings.editing || !isEnviarEnabledVar || state.enviarDadosDialogo.botaoOk) &&
                        <Button onPress={() => {
                            shouldCheckStatusChange.current = true;
                            setState({enviarDadosDialogo: initialState.enviarDadosDialogo});
                            navigation.navigate('Home', {tela: state.settings.editing ? TELAS.MAPEAR_TERRITORIO : TELAS.MAPA});
                        }}>Ok</Button>
                        }
                    </Dialog.Actions>
                </Dialog>
            </Portal>
            }
            {state.tela !== TELAS.ENVIAR && <View style={{ paddingHorizontal: metrics.tenWidth*0.5, position: 'absolute', top: state.tela == TELAS.MAPA ? metrics.tenWidth*2 : metrics.tenWidth*11, left: 0, width: '100%' }}>
                <SafeAreaView style={{ flexDirection: 'row', justifyContent: state.tela == TELAS.MAPA ? 'space-between' : 'flex-end', alignItems: 'center' }}>
                    {[TELAS.MAPA, TELAS.ENVIAR].includes(state.tela) && state.statusMessage &&
                        <IconButton
                            size={metrics.tenWidth*3.6}
                            color={state.statusMessage.color}
                            icon={state.statusMessage.icon}
                            style={mapTypeStyles.iconButtonStyle}
                            onPress={() => {
                                alertTerritorioStatusIdChanged();
                            }}
                        />
                    }
                    {
                        !Constants.manifest.extra.xyzRTiles.using && (
                            <Button
                                mode="contained"
                                contentStyle={{
                                    height: metrics.tenWidth * 3.6,
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                                color={colors.background}
                                labelStyle={{
                                    textAlign: "center",
                                    textAlignVertical: "center",
                                    fontSize: metrics.tenWidth * 1.6,
                                }}
                                onPress={() => {
                                    switch (state.settings.mapType) {
                                        case MAP_TYPES.STANDARD:
                                            state.settings.mapType = MAP_TYPES.HYBRID;
                                            break;
                                        case MAP_TYPES.HYBRID:
                                            state.settings.mapType = MAP_TYPES.STANDARD;
                                            break;
                                    }
                                    updateSettings({
                                        settings: { mapType: state.settings.mapType },
                                        noSnackBarMessage: true,
                                    });
                                }}
                            >
                                {mapTypeStyles.label}
                            </Button>
                        )
                    }
                    {[TELAS.MAPA, TELAS.ENVIAR].includes(state.tela) &&
                        <Image source={mapTypeStyles.logoImageToShow} style={LayoutMapa.mapearLogoImage}
                            resizeMode="contain" />
                    }
                </SafeAreaView>
            </View>
            }
            {state.mapSnapshotUri &&
                <View style={Layout.appOverlay}>
                    <Image source={{ uri: state.mapSnapshotUri }} />
                </View>
            }
            {[TELAS.MAPA, TELAS.ENVIAR].includes(state.tela) && !isPTTModeVar &&
            <Appbar style={LayoutMapa.appbarBottom}>
                <Appbar.Action
                    disabled={!isCurrentTerritorioEditableVar}
                    icon="tooltip-account"
                    size={metrics.tenWidth * 3}
                    onPress={() => {
                        if (isCurrentTerritorioEditableVar) {
                            navigation.navigate((!isPTTModeVar) ? 'DadosBasicos' : 'DadosPTT', {atualizarDados: true});
                            return;
                        }

                        alertTerritorioStatusIdChanged();
                    }}
                />
                <Appbar.Action
                    disabled={!isCurrentTerritorioEditableVar || isPTTModeVar}
                    icon="map" size={metrics.tenWidth * 3}
                    onPress={() => {
                        if (isCurrentTerritorioEditableVar) {
                            navigation.navigate('Home', {tela: TELAS.MAPEAR_TERRITORIO});
                            return;
                        }

                        alertTerritorioStatusIdChanged();
                    }}
                />
                <Appbar.Action
                    disabled={!isCurrentTerritorioEditableVar || isPTTModeVar}
                    icon="map-marker"
                    size={metrics.tenWidth * 3}
                    onPress={() => {
                        if (isCurrentTerritorioEditableVar) {
                            navigation.navigate('Home', {tela: TELAS.MAPEAR_USOSECONFLITOS});
                            return;
                        }

                        alertTerritorioStatusIdChanged();
                    }}
                />
                <Appbar.Action
                    icon="file"
                    size={metrics.tenWidth * 3}
                    onPress={() => {
                        navigation.navigate('AnexoList', {atualizarDados: true});
                    }}
                />
                <Appbar.Action
                    icon="menu"
                    size={metrics.tenWidth * 3}
                    onPress={() => navigation.openDrawer()}
                />
                {(isEnviarEnabledVar || isEnviarPTTEnabledVar) &&
                    <View style={{position:'absolute',bottom:metrics.tenWidth * 1.7,right:metrics.tenWidth * 1.3}}>
                        <IconButton icon='circle' size={metrics.tenWidth * 1.5} color={colors.warning} onPress={() => navigation.openDrawer()}/>
                    </View>
                }
            </Appbar>
            }
            {[TELAS.MAPA, TELAS.ENVIAR].includes(state.tela) && isPTTModeVar &&
            <Appbar style={LayoutMapa.appbarBottomPTT}>
                <Appbar.Content
                    title="Modo PTT"
                    titleStyle={Layout.topBarTitle}
                />
                <Appbar.Action
                    disabled={!isCurrentTerritorioEditableVar}
                    icon="tooltip-account"
                    color={colors.actionPTT}
                    size={metrics.tenWidth * 3}
                    onPress={() => {
                        if (isCurrentTerritorioEditableVar) {
                            navigation.navigate('DadosPTT', {atualizarDados: true});
                            return;
                        }

                        alertTerritorioStatusIdChanged();
                    }}
                />
                <Appbar.Action
                    icon="file"
                    color={colors.actionPTT}
                    size={metrics.tenWidth * 3}
                    onPress={() => {
                        navigation.navigate('AnexoList', {atualizarDados: true});
                    }}
                />
                <Appbar.Action
                    icon="menu"
                    color={colors.actionPTT}
                    size={metrics.tenWidth * 3}
                    onPress={() => navigation.openDrawer()}
                />
                {(isEnviarEnabledVar || isEnviarPTTEnabledVar) &&
                    <View style={isPTTModeVar ? LayoutMapa.appbarBottomCircleEnviarPTT : LayoutMapa.appbarBottomCircleEnviar}>
                        <IconButton icon='circle' size={metrics.tenWidth * 1.5} color={colors.warning} onPress={() => navigation.openDrawer()}/>
                    </View>
                }
            </Appbar>
            }
            <Snackbar visible={snackBarVisible == true} style={{position: 'absolute', bottom: metrics.tenWidth*6}} duration={2000} onDismiss={() => {setSnackBarVisible(false)}}>
                {snackBarMessage}
            </Snackbar>
        </View>
        }
        {(!state.settings || !state.region || state.carregando) &&
            <View style={Layout.appOverlay}>
                <View style={{backgroundColor: colors.primary, padding: metrics.tenWidth * 4, alignItems: 'center', borderRadius: metrics.tenWidth * 0.8}}>
                    <ActivityIndicator size="large" animating={true} color={colors.white} />
                    <Text style={{color: colors.white, marginTop: metrics.tenWidth * 3}}>
                        {state.carregando ? state.carregando : 'Carregando...'}
                    </Text>
                </View>
            </View>
        }
        </React.Fragment>
    );
}

MapaScreen.propTypes = {
    provider: ProviderPropType,
};

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    containerShot: {
        width: 794,
        height: 1123,
        justifyContent: 'flex-end',
        alignItems: 'center',
    }
});
