import React, {useReducer, useRef} from 'react';
import {Image, StyleSheet, View, Alert, ScrollView} from "react-native";
import {ActivityIndicator, IconButton, Button, Caption, Dialog, TextInput, Title, Text} from "react-native-paper";
import {Inputs, Layout} from "../styles";
import * as DocumentPicker from 'expo-document-picker';
import AppbarTonomapa from "../partials/AppbarTonomapa";
import {SafeAreaView} from "react-native-safe-area-context";
import MunicipioSelectH from "../components/MunicipioSelectH";
import {TextInputMask} from 'react-native-masked-text';
import {cpf} from 'cpf-cnpj-validator';
import {pickerStyles} from "../styles/pickers";
import colors from "../assets/colors";
import {AuthContext, enviaParaDashboard, carregaCurrentUser, carregaSettings, persisteCurrentUser, persisteSettings} from "../db/api";
import {geraTmpId} from '../utils/utils';
import STATUS from "../bases/status";
import CURRENT_USER from "../bases/current_user";
import SETTINGS from "../bases/settings";

/**
 * Tela de cadastro de usuária ou de nova comunidade
 * @module screens/CadastroScreen
 */

const pickerSelectStyles = StyleSheet.create(pickerStyles);

export default function CadastroScreen({route, navigation}) {
    const initialState = {
        currentUser: null,
        settings: null,
        municipioReferenciaId: null,
        estadoId: null,
        telefone: null,
        titulo: '',
        labelComunidade: '',
        loading: true
    };

    const reducer = (state, newState) => {
        return {...state, ...newState};
    };
    const [state, setState] = useReducer(reducer, initialState);
    const loading = useRef(true);
    const { signUp } = React.useContext(AuthContext);

    // Load data from local storage
    if (loading.current && !state.currentUser) {
        loading.current = false;
        const titulosCadastro = {
            titulo: 'Cadastro',
            labelComunidade: 'Comunidade que será mapeada',
            labelSalvar: 'Enviar'
        };
        if (route.name == 'Cadastro') {
            let newCurrentUser = CURRENT_USER;
            let newState = {...titulosCadastro};
            if (route.params && "username" in route.params) {
                newCurrentUser.username = route.params.username;
                newState.telefone = route.params.username;
                delete route.params.username;
            }
            newState.currentUser = newCurrentUser;
            newState.loading = false;
            setState(newState);
        } else {
            /*DEBUG*/ //console.log('CadastroScreen calls carregaCurrentUser');
            carregaCurrentUser().then((currentUser) => {
                if (currentUser) {
                    let newState = {};
                    if (route.name == 'Cadastro') {
                        let estadoId = null;
                        let municipioReferenciaId = null;
                        if (!!currentUser.currentTerritorio.municipioReferenciaId) {
                            estadoId = parseInt(currentUser.currentTerritorio.municipioReferenciaId.toString().substring(0, 2));
                            municipioReferenciaId = parseInt(currentUser.currentTerritorio.municipioReferenciaId);
                        }
                        setState({
                            ...titulosCadastro,
                            currentUser: currentUser,
                            municipioReferenciaId: municipioReferenciaId,
                            estadoId: estadoId,
                            loading: false
                        });
                    } else {
                        carregaSettings().then(settings => {
                            currentUser.currentTerritorio = CURRENT_USER.currentTerritorio;
                            setState({
                                currentUser: currentUser,
                                settings: settings,
                                titulo: 'Nova comunidade',
                                labelComunidade: 'Nome da comunidade',
                                labelSalvar: 'Criar',
                                loading: false
                            });
                        });
                    }
                }
            }).catch((error) => {
                setState({
                    ...titulosCadastro,
                    currentUser: CURRENT_USER,
                    loading: false
                });
                console.log("Não foi criado este usuário localmente ainda.", error);
            });
        }
    }

    const onChangeEstado = (estadoId) => {
        setState({
            municipioReferenciaId: null,
            estadoId: estadoId
        });
    };

    function validarCadastroBasico() {
        const errors = [];

        if (route.name == 'Cadastro') {
            if (!state.currentUser.username || state.currentUser.username.length < 10) {
                errors.push(['telefone', '· Seu telefone']);
            }

            if (!state.currentUser.fullName || 0 === state.currentUser.fullName.trim().length) {
                errors.push(['nome', '· Seu nome']);
            }

            if (!state.currentUser.cpf || 0 === state.currentUser.cpf.trim().length) {
                errors.push(['cpf', '· CPF obrigatório']);
            }

            if (!cpf.isValid(state.currentUser.cpf)) {
                errors.push(['cpf', '· CPF inválido']);
            }
        }


        if (!state.municipioReferenciaId) {
            errors.push(['municipioReferenciaId', '· Município']);
        }

        if (!state.currentUser.currentTerritorio.nome || 0 === state.currentUser.currentTerritorio.nome.trim().length) {
            errors.push(['nomeComunidade', '· Nome da comunidade que vai ser mapeada']);
        } else if (route.name == 'CadastroTerritorio') {
            let comunidadeJaExiste = false;
            for (let territorio of state.settings.territorios) {
                if (territorio.id != null && territorio.nome === state.currentUser.currentTerritorio.nome) {
                    comunidadeJaExiste = true;
                    break;
                }
            }
            if (comunidadeJaExiste) {
                errors.push(['nomeComunidadeJaExiste', '· Você já está mapeando uma comunidade com este nome. Por favor, escolha outro nome']);
            }
        }

        if (errors.length > 0) {
            let message = "Os seguintes dados precisam ser fornecidos ou alterados:\n\n";
            for (let erro of errors) {
                message += erro[1] + "\n"
            }

            Alert.alert(
                'Dados incompletos ou repetidos',
                message,
                [
                    {
                        text: 'OK', onPress: async () => {
                        }
                    },
                ],
                {cancelable: true},
            );
            return false;
        }

        return true;
    }

    const alertaErro = (titulo, erro) => {
        Alert.alert(
            titulo,
            erro,
            [
                {
                    text: 'OK', onPress: async () => {
                    }
                },
            ],
            {cancelable: true},
        );
    };

    const salvarFn = async () => {
        if (validarCadastroBasico()) {
            setState({loading: true});
            const currentUser = {
                ...state.currentUser,
                cpf: state.currentUser.cpf.replace(/[\W_]/g, ''),
                currentTerritorio: {
                    ...state.currentUser.currentTerritorio,
                    municipioReferenciaId: state.municipioReferenciaId.toString(),
                    statusId: STATUS.EM_PREENCHIMENTO
                }
            };

            if (route.name == 'Cadastro') {
                const res = await enviaParaDashboard({
                    currentUser: currentUser,
                    appFiles: SETTINGS.appFiles,
                    isCadastro: true
                });
                if (res) {
                    // res devolve res.settings e res.currentUser
                    /*DEBUG*/ //console.log("resposta enviaParaDashboard", res);

                    if (res.token) {
                        signUp(res.token);
                        return;
                    }

                    setState({loading: false});
                    alertaErro('Este telefone já está cadastrado no Tô no Mapa', `Por favor, entre em contato com a equipe Tô no Mapa e peça o seu Código de Segurança para entrar no aplicativo. Mensagem: ${res.erros}`);
                } else {
                    setState({loading: false});
                    alertaErro('Erro desconhecido', 'Nenhuma mensagem voltou do servidor.');
                }
            } else {
                await persisteCurrentUser({currentUser: currentUser});
                // setState({loading: false});
                state.settings.territorios.push(currentUser.currentTerritorio);
                const res = await persisteSettings({territorios: state.settings.territorios});
                navigation.navigate('Home', {
                    atualizarDados: true,
                    snackBarMessage: 'Você agora está mapeando a comunidade ' + currentUser.currentTerritorio.nome
                });
            }
        }
    };

    return (
        <React.Fragment>
        {state.loading &&
            <View style={Layout.appOverlay}>
                <View style={{backgroundColor: colors.primary, padding: 40, alignItems: 'center', borderRadius: 8}}>
                    <ActivityIndicator size="large" animating={true} color={colors.white} />
                </View>
            </View>
        }
        {!state.loading &&
        <View style={Layout.containerStretched}>
            <AppbarTonomapa navigation={navigation} title={state.titulo} goBack={true} />
            <View style={Layout.body}>
                <ScrollView keyboardShouldPersistTaps="handled">
                    <SafeAreaView style={Layout.innerBody}>
                        {route.name == 'Cadastro' &&
                            <TextInput
                                label="Seu telefone"
                                value={state.telefone}
                                labelStyle={Inputs.textInputLabel}
                                style={Inputs.textInput}
                                onChangeText={text => {
                                    const newCurrentUser = {
                                        ...state.currentUser,
                                        username: text.replace(/[^0-9.]/g, '')
                                    };
                                    setState({
                                        telefone: text,
                                        currentUser: newCurrentUser
                                    });
                                }}
                                render={props => (
                                    <TextInputMask
                                        {...props}
                                        type={'cel-phone'}
                                        options={{
                                            maskType: 'BRL',
                                            withDDD: true,
                                            dddMask: '(99) '
                                        }}
                                    />
                                )}
                            />
                        }
                        {route.name == 'Cadastro' &&
                            <TextInput
                                label="Seu nome"
                                style={Inputs.textInput}
                                labelStyle={Inputs.textInputLabel}
                                value={state.currentUser.fullName}
                                onChangeText={(value) => {
                                    const newCurrentUser = {
                                        ...state.currentUser,
                                        fullName: value
                                    };
                                    setState({currentUser: newCurrentUser});
                                }}
                            />
                        }
                        {route.name == 'Cadastro' &&
                            <TextInput
                                label="Seu CPF"
                                style={Inputs.textInput}
                                labelStyle={Inputs.textInputLabel}
                                value={state.currentUser.cpf}
                                keyboardType = 'numeric'
                                onChangeText={(value) => {
                                    const newCurrentUser = {
                                        ...state.currentUser,
                                        cpf: cpf.format(value)
                                    };
                                    setState({currentUser: newCurrentUser});
                                }}
                            />
                        }
                        <TextInput label={state.labelComunidade} style={Inputs.textInput}
                                   value={state.currentUser.currentTerritorio.nome} onChangeText={(value) => {
                            const newCurrentUser = {
                                ...state.currentUser,
                                currentTerritorio: {
                                    ...state.currentUser.currentTerritorio,
                                    nome: value
                                }
                            };
                            setState({currentUser: newCurrentUser});
                        }}/>

                        <Caption style={Inputs.caption}>Município de Referência</Caption>
                        <MunicipioSelectH
                            estadoId={state.estadoId}
                            municipioId={state.municipioReferenciaId}
                            onValueChange={(value) => {
                                const newState = {
                                    municipioReferenciaId: value.municipioId
                                }
                                if (value.hasOwnProperty('estadoId')) {
                                    newState.estadoId = value.estadoId;
                                }
                                setState(newState);
                            }}
                        />

                        <View style={Layout.buttonsBottomWrapper}>
                            <View style={(route.name == 'Cadastro') ? Layout.buttonRight : {...Layout.row}}>
                                {(route.name == 'CadastroTerritorio') &&
                                    <Button mode="contained" style={Layout.buttonLeft} color={colors.danger} onPress={() => navigation.goBack()}>Cancelar</Button>
                                }
                                <Button mode="contained" dark={true} style={Layout.buttonRight} color={colors.primary} onPress={salvarFn}>{state.labelSalvar}</Button>
                            </View>
                        </View>
                    </SafeAreaView>
                </ScrollView>
            </View>
        </View>
        }
        </React.Fragment>
    );
}
