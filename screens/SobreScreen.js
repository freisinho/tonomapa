import React from 'react';
import {Image, StyleSheet, View, ScrollView} from "react-native";
import {Button, Paragraph, Surface, Text, Title} from "react-native-paper";
import {Alignments, Layout, Messages} from "../styles";
import {SafeAreaView} from "react-native-safe-area-context";
import AppbarTonomapa from "../partials/AppbarTonomapa";
import TextoSobre from '../partials/TextoSobre';
import colors from '../assets/colors';
import TELAS from '../bases/telas';

export default function SobreScreen({navigation}) {
    return (
        <View style={Layout.containerCentered}>
            <AppbarTonomapa navigation={navigation} title="Sobre o Tô no Mapa" goBack={true} />
            <View style={Layout.body}>
                <ScrollView keyboardShouldPersistTaps="handled">
                    <SafeAreaView style={Layout.innerBody}>
                        <Image source={require('../assets/images/logo/main/tonomapa_logo.png')} style={Layout.logoImageCadastro}
                                   resizeMode="contain"/>

                        <TextoSobre />

                        <View style={Layout.buttonsBottomWrapper}>
                            <View style={{...Layout.row}}>
                                <Button
                                    mode="outlined"
                                    dark={false}
                                    color={colors.warning}
                                    style={Layout.buttonCenter}
                                    onPress={() => {
                                        navigation.navigate('TermosDeUso');
                                    }}
                                >
                                    Termos de Uso
                                </Button>
                                <Button
                                    mode="contained"
                                    dark={true}
                                    style={Layout.buttonCenter}
                                    onPress={() => {
                                        navigation.navigate('Home', {tela: TELAS.MAPA})
                                    }}
                                >
                                    Voltar
                                </Button>
                            </View>
                        </View>
                    </SafeAreaView>
                </ScrollView>
            </View>
        </View>
    );
}
