import React, {useReducer, useState, useRef} from 'react';
import {TouchableHighlight, BackHandler, View, Alert} from "react-native";
import {Clipboard} from "expo-clipboard"
import {RadioButton, IconButton, Button, Switch, Text, ActivityIndicator, Snackbar, Portal, Dialog} from "react-native-paper";
import {Layout} from "../styles";
import {useFocusEffect} from '@react-navigation/native';
import colors from "../assets/colors";
import AppbarTonomapa from "../partials/AppbarTonomapa";
import {carregaCurrentUser, carregaTudoLocal, persisteSettings} from "../db/api";
import {deepEqual} from "../utils/utils";
import {UNIDADES_AREA} from "../maps/mapsUtils";
import TELAS from "../bases/telas";
import { STATUS, isEnviarEnabled } from "../bases/status";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import metrics from "../utils/metrics";

let snackBarMessage = "";

export default function ConfiguracoesScreen({navigation, route}) {
    const SNACKBAR_MESSAGES = {
        CONFIGURACOES: 'Configurações atualizadas com sucesso',
        COMUNIDADE_ALTERADA: 'Comunidade alterada com sucesso!',
        CODIGO_SEGURANCA_COPIADO: 'Código de segurança copiado!'
    };
    const initialState = {
        settings: null,
        currentUser: null,
        indiceTerritorio: null,
        loading: false,
    };

    const reducer = (state, newState) => {
        return {...state, ...newState};
    };
    const [state, setState] = useReducer(reducer, initialState);
    const [snackBarVisible, setSnackBarVisible] = useState(false);
    const [dialogListaTerritoriosVisible, setDialogListaTerritoriosVisible] = useState(false);
    const [dialogListaUnidadesVisible, setDialogListaUnidadesVisible] = useState(false);
    const [novoIndiceTerritorio, setNovoIndiceTerritorio] = useState(null);
    const dadosMudaram = useRef(false);

    if (!state.settings || route.params.atualizarDados) {
        if (route.params && "atualizarDados" in route.params) {
            delete route.params.atualizarDados;
        }
        /*DEBUG*/ //console.log('ConfiguracoesScreen calls carregaTudoLocal');
        carregaTudoLocal().then((data) => {
            let newState = {};
            let mudouState = false;
            if (!deepEqual(state.settings, data.settings)) {
                newState.settings = data.settings;
                mudouState = true;
            };
            if (!deepEqual(state.currentUser, data.currentUser)) {
                let indiceTerritorio;
                for (let i in data.settings.territorios) {
                    if (data.settings.territorios[i].nome === data.currentUser.currentTerritorio.nome) {
                        indiceTerritorio = i;
                        break;
                    }
                }
                newState.currentUser = data.currentUser;
                newState.indiceTerritorio = indiceTerritorio;
                mudouState = true;
            };
            if (route.params && "dadosMudaram" in route.params) {
                dadosMudaram.current = route.params.dadosMudaram;
                snackBarMessage = (route.params.snackBarMessage)
                    ? route.params.snackBarMessage
                    : SNACKBAR_MESSAGES.CONFIGURACOES;
                delete route.params.dadosMudaram;
                setSnackBarVisible(true);
            } else {
                dadosMudaram.current = false;
            }
            if (mudouState) {
                setState(newState);
            }
        });
    }

    if (route.params.editaComunidades) {
        delete route.params.editaComunidades;
        setDialogListaTerritoriosVisible(true);
    }

    const updateSettings = (params) => {
        persisteSettings(params.settings).then((newSettings) => {
            /*DEBUG*/ //console.log(params.settings, newSettings);
            setState({settings: newSettings});
            if (!params.noSnackBarMessage) {
                snackBarMessage = SNACKBAR_MESSAGES.CONFIGURACOES;
                setSnackBarVisible(true);
            }
        });
        dadosMudaram.current = true;
    }

    const exitScreen = () => {
        let navigateParams = {tela: TELAS.MAPA}
        if (dadosMudaram.current) {
            navigateParams = {
                ...navigateParams,
                atualizarDados: true
            };
        }
        navigation.navigate('Home', navigateParams);
        return true;
    }

    const TerritorioRadioList = () => {
        // Fix used exclusively for the case the user acts too quickly to exit configuracoesScreen:
        let territorios = [];
        for (let i in state.settings.territorios) {
            if (state.settings.territorios[i].id != null || parseInt(state.indiceTerritorio) == parseInt(i)) {
                territorios.push(state.settings.territorios[i]);
            }
        }
        // End fix.
        return (
            <React.Fragment>
            {state.indiceTerritorio != null
                ? <RadioButton.Group
                            value={'item' + state.indiceTerritorio}
                            onValueChange={value => {
                                if (value != state.indiceTerritorio) {
                                    setNovoIndiceTerritorio(value.replace('item',''));
                                }
                                setDialogListaTerritoriosVisible(false);
                            }}>
                    {territorios.map((territorio, indiceTerritorio) => (
                        <View style={Layout.row} key={'item' + indiceTerritorio}>
                            <Text style={Layout.text}>
                                {territorio.nome}
                            </Text>
                            <RadioButton value={'item' + indiceTerritorio} />
                        </View>
                    ))}
                </RadioButton.Group>
                : <View>
                    <Text style={Layout.textParagraph}>
                        Você ainda não enviou dados para o Tô no Mapa. Quando tiver enviado, poderá adicionar novos territórios a serem mapeados aqui.
                    </Text>
                </View>
            }
            </React.Fragment>
        );
    };

    //Android Back button
    useFocusEffect(
        React.useCallback(() => {
            const onBackPress = () => {
                return exitScreen();
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);
            return () =>
                BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        }, [route])
    );

    const isEnviarEnabledVar = isEnviarEnabled(state.currentUser);

    /*DEBUG*/ //console.log('tonomapa: ConfiguracoesScreen', state);

    return (
        <React.Fragment>
        {(state.loading || !state.settings) &&
            <View style={Layout.appOverlay}>
                <View style={{backgroundColor: colors.primary, padding: 40, alignItems: 'center', borderRadius: 8}}>
                    <ActivityIndicator size="large" animating={true} color={colors.white} />
                    <Text style={{color: colors.white, marginTop: 30}}>Carregando...</Text>
                </View>
            </View>
        }
        {(!state.loading && state.settings) &&
            <View style={Layout.containerStretched}>
                <AppbarTonomapa navigation={navigation} title="Configurações" customGoBack={exitScreen}/>
                <View style={Layout.bodyConfiguracoes}>
                    <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={true} extraScrollHeight={200}>
                        <View style={Layout.innerBody}>
                            {state.currentUser.codigoUsoCelular &&
                                <View style={Layout.rowConfiguracoesComDesc}>
                                    <TouchableHighlight underlayColor={colors.primary} onPress={() => {
                                        Clipboard.setString(state.currentUser.codigoUsoCelular);
                                        snackBarMessage = SNACKBAR_MESSAGES.CODIGO_SEGURANCA_COPIADO;
                                        setSnackBarVisible(true);
                                    }}>
                                        <View style={Layout.row}>
                                            <Text style={Layout.text}>
                                                Código de Segurança
                                            </Text>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <Text style={Layout.text}>
                                                    {state.currentUser.codigoUsoCelular}
                                                </Text>
                                                <IconButton icon='content-copy' color={colors.primary} size={16}/>
                                            </View>
                                        </View>
                                    </TouchableHighlight>
                                    <Text style={Layout.descConfiguracoes}>Não perca o seu código de segurança! Você precisará dele e do seu número de telefone se for reinstalar o aplicativo ou trocar de aparelho.</Text>
                                </View>
                            }
                            <TouchableHighlight underlayColor={colors.primary} onPress={() => {
                                setDialogListaTerritoriosVisible(true);
                            }}>
                                <View style={Layout.rowConfiguracoes}>
                                    <View style={Layout.rowConfiguracoesComDesc}>
                                        <Text style={Layout.text}>Comunidade sendo mapeada</Text>
                                        <Text style={Layout.textPeq}>{state.currentUser.currentTerritorio.nome}</Text>
                                    </View>
                                    <IconButton icon="swap-horizontal-bold" size={32} color={colors.primary} />
                                </View>
                            </TouchableHighlight>

                            <View style={Layout.secaoConfiguracoes}>
                                <Text style={{fontSize: metrics.tenWidth*2.0, color: colors.primary}}>
                                Comportamento do App
                                </Text>
                            </View>

                            <TouchableHighlight underlayColor={colors.primary} onPress={() => {
                                setDialogListaUnidadesVisible(true);
                            }}>
                                <View style={Layout.rowConfiguracoesComDesc}>
                                    <Text style={Layout.text}>Unidade de área</Text>
                                    <Text style={Layout.textPeq}>{state.settings.unidadeArea.nome} ({state.settings.unidadeArea.sigla})</Text>
                                </View>
                            </TouchableHighlight>

                            <View style={Layout.rowConfiguracoes}>
                                <Text style={Layout.textLeftFromSwitch}>Uso do GPS</Text>
                                <Switch style={Layout.switchRight} color={colors.primary} value={state.settings.useGPS} onValueChange={() => {
                                    state.settings.useGPS = !state.settings.useGPS;
                                    updateSettings({settings: {useGPS: state.settings.useGPS}});
                                }}/>
                            </View>

                            <View style={Layout.rowConfiguracoes}>
                                <Text style={Layout.textLeftFromSwitch}>Edição das áreas (polígonos)</Text>
                                <Switch style={Layout.switchRight} color={colors.primary} value={state.settings.editaPoligonos} onValueChange={() => {
                                    state.settings.editaPoligonos = !state.settings.editaPoligonos;
                                    updateSettings({settings: {editaPoligonos: state.settings.editaPoligonos}});
                                }}/>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </View>
                <Snackbar visible={snackBarMessage && snackBarVisible == true} style={{position: 'absolute', bottom: 0}} duration={2000} onDismiss={() => {setSnackBarVisible(false)}}>
                    {snackBarMessage}
                </Snackbar>
                <Portal>
                    <Dialog visible={dialogListaTerritoriosVisible} onDismiss={() => {setDialogListaTerritoriosVisible(false)}}>
                        <Dialog.Title>Comunidades</Dialog.Title>
                        <Dialog.Content>
                            <TerritorioRadioList/>
                        </Dialog.Content>
                        <Dialog.Actions style={{flexDirection: 'column', alignItems: 'flex-end'}}>
                            <Button onPress={() => {setDialogListaTerritoriosVisible(false)}}>Cancelar</Button>
                            <Button onPress={() => {
                                setDialogListaTerritoriosVisible(false);
                                if (isEnviarEnabledVar) {
                                    setNovoIndiceTerritorio('criarTerritorio');
                                } else {
                                    navigation.navigate('CadastroTerritorio');
                                }
                            }}>Adicionar comunidade</Button>
                        </Dialog.Actions>
                    </Dialog>

                    <Dialog visible={dialogListaUnidadesVisible} onDismiss={() => {setDialogListaUnidadesVisible(false)}}>
                        <Dialog.Title>Unidade de área</Dialog.Title>
                        <Dialog.Content>
                        <RadioButton.Group
                            value={state.settings.unidadeArea.sigla}
                            onValueChange={value => {
                                for (let unidadeArea of Object.values(UNIDADES_AREA)) {
                                    if (unidadeArea.sigla == value) {
                                        state.settings.unidadeArea = unidadeArea;
                                        updateSettings({settings: {unidadeArea: unidadeArea}});
                                        break;
                                    }
                                }
                                setDialogListaUnidadesVisible(false);
                            }}>
                            {Object.values(UNIDADES_AREA).map(unidadeArea => (
                                <View style={Layout.row} key={unidadeArea.sigla}>
                                    <Text style={Layout.text}>
                                        {unidadeArea.nome} ({unidadeArea.sigla})
                                    </Text>
                                    <RadioButton value={unidadeArea.sigla} />
                                </View>
                            ))}
                        </RadioButton.Group>
                        </Dialog.Content>
                        <Dialog.Actions style={{flexDirection: 'column', alignItems: 'flex-end'}}>
                            <Button onPress={() => {setDialogListaUnidadesVisible(false)}}>Cancelar</Button>
                        </Dialog.Actions>
                    </Dialog>

                    {novoIndiceTerritorio != null &&
                        <Dialog visible={novoIndiceTerritorio != null} onDismiss={() => {setNovoIndiceTerritorio(null)}}>
                            <Dialog.Title>Confirmar mudança de comunidade</Dialog.Title>
                            <Dialog.Content>
                                {novoIndiceTerritorio === 'criarTerritorio' &&
                                    <View>
                                        <Text style={Layout.textParagraph}>
                                            <Text style={Layout.bold}>Atenção!</Text>
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            Você tem mudanças da comunidade
                                            <Text style={Layout.bold}> {state.currentUser.currentTerritorio.nome} </Text>
                                            não enviadas ao Tô no Mapa!
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            <Text style={Layout.bold}>
                                                Se você criar uma nova comunidade agora, perderá todas as alterações ainda não enviadas!
                                            </Text>
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            Tem certeza que quer criar uma nova comunidade a ser mapeada? Se sim, é preciso conexão à internet.
                                        </Text>
                                    </View>
                                }
                                {novoIndiceTerritorio != 'criarTerritorio' && !isEnviarEnabledVar &&
                                    <View>
                                        <Text style={Layout.textParagraph}>
                                            Tem certeza que quer mudar a comunidade para
                                            <Text style={Layout.bold}> {state.settings.territorios[novoIndiceTerritorio].nome}</Text>
                                            ?
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            Esta mudança só é possível com acesso a internet!
                                        </Text>
                                    </View>
                                }
                                {novoIndiceTerritorio != 'criarTerritorio' && isEnviarEnabledVar &&
                                    <View>
                                        <Text style={Layout.textParagraph}>
                                            <Text style={Layout.bold}>Atenção!</Text>
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            Você tem mudanças da comunidade
                                            <Text style={Layout.bold}> {state.currentUser.currentTerritorio.nome} </Text>
                                            não enviadas ao Tô no Mapa!
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            <Text style={Layout.bold}>
                                                Se você mudar de comunidade agora, perderá todas as alterações ainda não enviadas!
                                            </Text>
                                        </Text>
                                        <Text style={Layout.textParagraph}>
                                            Tem certeza que quer mudar para a Comunidade
                                            <Text style={Layout.bold}> {state.settings.territorios[novoIndiceTerritorio].nome}</Text>
                                            ? Se sim, é preciso conexão à internet!
                                        </Text>
                                    </View>
                                }
                            </Dialog.Content>
                            <Dialog.Actions style={{flexDirection: 'column', alignItems: 'flex-end'}}>
                                <Button onPress={() => {setNovoIndiceTerritorio(null)}}>Cancelar</Button>
                                <Button onPress={() => {
                                    if (novoIndiceTerritorio === 'criarTerritorio') {
                                        setNovoIndiceTerritorio(null);
                                        navigation.navigate('CadastroTerritorio');
                                    } else {
                                        const novoIdTerritorio = state.settings.territorios[novoIndiceTerritorio].id;
                                        setState({
                                            loading: true
                                        });
                                        /*DEBUG*/ //console.log('Mudar de comunidade calls carregaCurrentUser');
                                        carregaCurrentUser({
                                            fetchPolicy: 'network-only',
                                            idTerritorio: novoIdTerritorio
                                        }).then(({newCurrentUser, newSettings}) => {
                                            dadosMudaram.current = true;
                                            snackBarMessage = SNACKBAR_MESSAGES.COMUNIDADE_ALTERADA;
                                            // setSnackBarVisible(true);
                                            setState({
                                                loading: false,
                                                // currentUser: newCurrentUser,
                                                // settings: newSettings,
                                                // indiceTerritorio: novoIndiceTerritorio
                                            });
                                            setNovoIndiceTerritorio(null);
                                            navigation.navigate('Home', {tela: TELAS.MAPA, atualizarDados: true, snackBarMessage: 'Você está mapeando ' + newCurrentUser.currentTerritorio.nome});
                                        }).catch(e => {
                                            console.log('erro pegando território. Internet?', e);
                                            // TODO: tratar este erro!
                                            setNovoIndiceTerritorio(null);
                                        });
                                    }
                                }}>Sim, mudar comunidade</Button>
                            </Dialog.Actions>
                        </Dialog>
                    }

                </Portal>
            </View>
        }
        </React.Fragment>
    );
}
