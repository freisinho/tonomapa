import React, {useReducer, useRef} from 'react';
import {View, Alert} from "react-native";
import {Button, Text, TextInput, IconButton, Switch} from "react-native-paper";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import * as DocumentPicker from 'expo-document-picker';
import * as FileSystem from 'expo-file-system';
import * as mime from "react-native-mime-types";
import XMLParser from 'react-xml-parser';
import AppbarTonomapa from "../partials/AppbarTonomapa";
import MunicipioSelectH from "../components/MunicipioSelectH";
import TipoComunidadeSelect from "../components/TipoComunidadeSelect";
import {kml} from "../maps/togeojson";
import {getCurrentMensagemIndex, cloneCurrentUser, carregaTudoLocal, persisteCurrentUser, persisteSettings, cloneAppFiles} from "../db/api";
import {iconeDoMimeType, formataTamanhoArquivo, apagaArquivos} from "../db/arquivos";
import {pluralize, formataNumeroBR} from "../utils/utils";
import {Inputs, Layout} from "../styles";
import colors from "../assets/colors";
import TELAS from "../bases/telas";
import {adjustTerritorioFieldTypes} from '../bases/dadosbasicos';
import MENSAGEM from '../bases/mensagem';
import STATUS from '../bases/status';

export default function DadosBasicosScreen({navigation, route}) {
    const initialState = {
        id: null,
        nome: null,
        anoFundacao: null,
        qtdeFamilias: null,
        estadoId: null,
        municipioReferenciaId: null,
        tiposComunidade: null,
        publico: false,
        poligono: null,
        poligonoArq: null,
        mensagem: null,
        currentUser: null,
        settings: null,
    };

    const reducer = (state, newState) => {
        return {...state, ...newState};
    };
    const [state, setState] = useReducer(reducer, initialState);
    const populandoEstado = useRef(true);

    // Load data from local storage
    if (!state.currentUser || route.params.atualizarDados) {
        delete route.params.atualizarDados;
        populandoEstado.current = true;
        carregaTudoLocal().then(({currentUser, settings}) => {
            const newCurrentUser = cloneCurrentUser(currentUser);
            const newAppFiles = cloneAppFiles(settings.appFiles);
            /*DEBUG*/ //console.log('dadosBasicos', newCurrentUser);
            let estadoId = null;
            let municipioReferenciaId = null;
            if (!!newCurrentUser.currentTerritorio.municipioReferenciaId) {
                estadoId = parseInt(newCurrentUser.currentTerritorio.municipioReferenciaId.toString().substring(0,2));
                municipioReferenciaId = parseInt(newCurrentUser.currentTerritorio.municipioReferenciaId);
            }
            const i = getCurrentMensagemIndex(newCurrentUser);
            const currentMensagemTexto = (i >= 0)
                ? newCurrentUser.currentTerritorio.mensagens[i].texto
                : null;
            let tiposComunidade = [];
            for (let tipoComunidade of newCurrentUser.currentTerritorio.tiposComunidade) {
                tiposComunidade.push(tipoComunidade.id);
            }
            setState({
                ...newCurrentUser.currentTerritorio,
                nome: newCurrentUser.currentTerritorio.nome,
                estadoId: estadoId,
                municipioReferenciaId: municipioReferenciaId,
                anoFundacao: (newCurrentUser.currentTerritorio.anoFundacao)
                    ? newCurrentUser.currentTerritorio.anoFundacao.toString()
                    : null,
                qtdeFamilias: (newCurrentUser.currentTerritorio.qtdeFamilias)
                    ? newCurrentUser.currentTerritorio.qtdeFamilias.toString()
                    : null,
                tiposComunidade: tiposComunidade,
                publico: newCurrentUser.currentTerritorio.publico,
                mensagem: currentMensagemTexto,
                currentUser: newCurrentUser,
                settings: settings
            });
        }).catch(e => {
            console.log("Usuário não existe na base local!", e);
            // TODO: O que fazer num caso como estes? Tratar. O ideal é efetuar logout...
        });
    }

    const validarTerritorio = () => {
        const errors = [];
        if (!state.nome || 0 === state.nome.trim().length) {
            errors.push(['nome', '· Nome da comunidade']);
        }

        if (!state.municipioReferenciaId) {
            errors.push(['municipioReferenciaId', '· Município']);
        }
        if (errors.length > 0) {

            let message = "Os seguintes dados precisam ser fornecidos:\n\n";
            for (let erro of errors) {
                message += erro[1] + "\n"
            }

            Alert.alert(
                'Dados incompletos',
                message,
                [
                    {
                        text: 'OK', onPress: async () => {
                        }
                    },
                ],
                {cancelable: false},
            );
            return false;
        }
        return true;
    };

    const salvarFn = async () => {
        if (validarTerritorio()) {
            state.currentUser = {
                ...state.currentUser,
                currentTerritorio: {
                    ...state.currentUser.currentTerritorio,
                    nome: state.nome,
                    estadoId: state.estadoId,
                    municipioReferenciaId: state.municipioReferenciaId,
                    anoFundacao: state.anoFundacao,
                    qtdeFamilias: state.qtdeFamilias,
                    tiposComunidade: state.tiposComunidade,
                    publico: state.publico
                }
            };
            if (state.poligono) {
                state.currentUser.currentTerritorio.poligono.coordinates = [
                    ...state.currentUser.currentTerritorio.poligono.coordinates,
                    ...state.poligono.coordinates
                ];
            }
            const newCurrentUser = {
                ...state.currentUser,
                currentTerritorio: adjustTerritorioFieldTypes(state.currentUser.currentTerritorio)
            };

            const i = getCurrentMensagemIndex(newCurrentUser);

            if (state.mensagem) {
                const newMensagem = {
                    ...MENSAGEM,
                    texto: state.mensagem
                };
                if (i >= 0) {
                    // Edit existing unsent message:
                    newCurrentUser.currentTerritorio.mensagens[i] = newMensagem;
                } else {
                    // Add new unsent message:
                    newCurrentUser.currentTerritorio.mensagens.push(newMensagem);
                }
            } else if (i >= 0) {
                // Remove unsent message:
                newCurrentUser.currentTerritorio.mensagens.slice(i, 1);
            }

            setState({poligonoArq: null});

            if ('cadastrarTerritorio' in route.params) {
                newCurrentUser.currentTerritorio.statusId = STATUS.EM_PREENCHIMENTO;
            }

            const res = await persisteCurrentUser(newCurrentUser);

            let params = {tela: TELAS.MAPA};
            if (res.dadosMudaram) {
                params.snackBarMessage = "As informações da comunidade foram atualizadas!";
                params.atualizarDados = true;
            }

            navigation.navigate('Home', params);
        }
    };

    const onChangeEstado = (estadoId) => {
        if (populandoEstado.current) {
            populandoEstado.current = false;
        } else {
            setState({
                municipioReferenciaId: null,
                estadoId: estadoId
            });
        }
    };

    const openKmlImport = () => {
        Alert.alert(
            'Importar arquivo KML',
            "Escolha o arquivo KML com os polígonos que delimitam o território. Esta função é avançada e não é necessária: você pode registrar as áreas da sua comunidade diretamente no app! Se você não sabe o que é KML, ignore este campo.",
            [
                {
                    text: 'Cancelar', onPress: () => {}
                },
                {
                    text: 'Selecionar arquivo KML',
                    onPress: async () => {
                        const result = await DocumentPicker.getDocumentAsync({type: 'application/vnd.google-earth.kml+xml'});
                        if (result.type === 'success') {
                            const kmlContent = await FileSystem.readAsStringAsync(result.uri, {});
                            const xmlDoc = new XMLParser().parseFromString(kmlContent);
                            const json = kml(xmlDoc);

                            let polygons = [];
                            for (let element of json.features) {
                                if (element.geometry.type === 'Polygon') {
                                    let coordinates = [];
                                    for (let polygonPart of element.geometry.coordinates) {
                                        for (let i = 0; i < polygonPart.length - 1; i++) {
                                            let coordinate = polygonPart[i];
                                            coordinates.push({longitude: coordinate[0], latitude: coordinate[1]});
                                        }
                                    }
                                    polygons.push(coordinates);
                                }
                            }

                            //Agora ao formato graphQl
                            const multiPol = {
                                type: 'MultiPolygon',
                                coordinates: []
                            };

                            for (let polygon of polygons) {
                                let backPolygon = [[]];
                                for (let i in polygon) {
                                    backPolygon[0].push([polygon[i].longitude, polygon[i].latitude]);
                                }
                                backPolygon[0].push([polygon[0].longitude, polygon[0].latitude]);
                                multiPol.coordinates.push(backPolygon);
                            }

                            const tamanho = formataNumeroBR(result.size / 1024);

                            const poligonoArq = {
                                nome: result.name,
                                tamanho: tamanho
                            }

                            setState({
                                poligono: multiPol,
                                poligonoArq: poligonoArq
                            });
                        }
                    }
                },
            ],
            {cancelable: true},
        );
    };

    return (
        <View style={Layout.containerStretched}>
            <AppbarTonomapa navigation={navigation} title="Dados da comunidade" />
            <View style={Layout.body}>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={true} extraScrollHeight={200}>
                    {state.currentUser &&
                    <View style={Layout.innerBody}>

                        <TextInput
                            label="Nome da comunidade"
                            style={Inputs.textInput}
                            labelStyle={Inputs.textInputLabel}
                            value={state.nome}
                            onChangeText={(value) => setState({nome: value})}
                        />

                        <TextInput
                            label="Ano de criação"
                            style={Inputs.textInput}
                            labelStyle={Inputs.textInputLabel}
                            keyboardType={'number-pad'}
                            maxLength={4}
                            value={state.anoFundacao}
                            onChangeText={(value) => setState({anoFundacao: value})}
                        />

                        <MunicipioSelectH
                            estadoId={state.estadoId}
                            municipioId={state.municipioReferenciaId}
                            onValueChange={(value) => {
                                const newState = {
                                    municipioReferenciaId: value.municipioId
                                };
                                if (value.hasOwnProperty('estadoId')) {
                                    newState.estadoId = value.estadoId;
                                }
                                setState(newState);
                            }}
                        />

                        <TextInput
                            label="Quantidade de famílias"
                            style={Inputs.textInput}
                            keyboardType="phone-pad"
                            value={state.qtdeFamilias}
                            onChangeText={(value) => setState({qtdeFamilias: value})}
                        />

                        <View style={{...Inputs.caption, alignItems: 'stretch'}}>
                            <TipoComunidadeSelect
                                selectedItems={state.tiposComunidade}
                                onValueChange={(values) => {
                                    setState({tiposComunidade: values})
                                }}
                            />
                        </View>

                        <View style={{...Layout.col, alignItems: 'flex-start'}}>
                            <Text style={Inputs.caption}>Opcional: Importar polígonos KML</Text>
                            {state.poligonoArq != null
                                ? <View style={{alignItems: 'flex-start'}}>
                                    <Button
                                        mode="contained"
                                        style={Layout.buttonLeft}
                                        dark={true}
                                        color={colors.text}
                                        onPress={openKmlImport}
                                    >
                                        Trocar arquivo...
                                    </Button>

                                    <Text>{state.poligonoArq.nome} ({state.poligonoArq.tamanho}Kb) - {pluralize(state.poligono.coordinates.length, "1 polígono importado", state.poligono.coordinates.length + " polígonos importados")}</Text>
                                </View>

                                : <Button
                                    mode="contained"
                                    style={Layout.buttonLeft}
                                    dark={true}
                                    color={colors.text}
                                    onPress={openKmlImport}
                                >
                                    Arquivo KML...
                                </Button>
                            }
                        </View>

                        {state.currentUser.id &&
                            <TextInput
                                label="Opcional: Mensagem à equipe Tô no Mapa"
                                style={Inputs.textInput}
                                labelStyle={Inputs.textInputLabel}
                                value={state.mensagem}
                                onChangeText={(value) => setState({mensagem: value})}
                            />
                        }

                        <View style={Layout.rowAnexoForm}>
                            <Text style={Layout.textLeftFromSwitch}>
                                Dados públicos: Os dados desta comunidade podem ser disponibilizados publicamente?
                            </Text>
                            <Switch
                                style={Layout.switchRight}
                                color={colors.primary}
                                value={state.publico}
                                onValueChange={() => {
                                    setState({publico: !state.publico});
                                }}
                            />
                        </View>

                        <View style={{...Layout.buttonRight, marginTop: 20}}>
                            <Button mode="contained" dark={true} onPress={salvarFn}>
                                Salvar
                            </Button>
                        </View>
                    </View>
                    }
                </KeyboardAwareScrollView>
            </View>
        </View>
    );
}
