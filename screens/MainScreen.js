import React, {useReducer, useState, useRef} from 'react';
import {MaterialCommunityIcons} from '@expo/vector-icons';
import {createDrawerNavigator, DrawerContentScrollView, DrawerItem} from "@react-navigation/drawer";
import {Avatar, Button, Paragraph, Text, IconButton} from "react-native-paper";
import SobreScreen from "./SobreScreen";
import TICCAsScreen from "./TICCAsScreen";
import TermosDeUsoScreen from "./TermosDeUsoScreen";
import DadosBasicosScreen from "./DadosBasicosScreen";
import DadosPTTScreen from "./DadosPTTScreen";
import MensagensScreen from "./MensagensScreen";
import MapaScreen from "./MapaScreen";
import ConfiguracoesScreen from "./ConfiguracoesScreen";
import {Image, Alert, StyleSheet, View} from "react-native";
import TELAS from "../bases/telas";
import {openUrlInBrowser} from "../utils/utils";
import STATUS, {
    isEnviarEnabled,
    isCurrentTerritorioEditable,
    isPTTMode,
    isEnviarPTTEnabled,
    alertStatusId
} from "../bases/status";
import colors from "../assets/colors";
import {Layout} from "../styles";
import {
    AuthContext,
    carregaSettings,
    carregaCurrentUser,
    persisteCurrentTerritorioStatusId
} from "../db/api";
import AnexoListScreen from "./AnexoListScreen";
import AnexoFormScreen from "./AnexoFormScreen";
import Constants from 'expo-constants';
import metrics from "../utils/metrics";

const RightDrawer = createDrawerNavigator();

/**
 * Tela do aplicativo que monta o menu lateral e direciona para as telas
 * @method      MainScreen
 * @param       {object}   route Parâmetros passados de outras screens pelo navigation
 * @constructor
 */
export default function MainScreen({route}) {
    /*DEBUG*/ //console.log('MainScreen', route.params);

    const [telaAtual, setTelaAtual] = useState(TELAS.MAPA);
    const [currentTerritorio, setCurrentTerritorio] = useState(null);

    const isOKTonomapa = () => {
        return (currentTerritorio && currentTerritorio.statusId == STATUS.OK_TONOMAPA);
    };

    const isEnviarEnabledVar = () => {
        return isEnviarEnabled({currentTerritorio: currentTerritorio});
    };

    const isCurrentTerritorioEditableVar = () => {
        return isCurrentTerritorioEditable({currentTerritorio: currentTerritorio});
    };

    const isPTTModeVar = () => {
        return isPTTMode({currentTerritorio: currentTerritorio});
    };

    const isEnviarPTTEnabledVar = () => {
        return isEnviarPTTEnabled({currentTerritorio: currentTerritorio});
    };

    if (route.params && "telaAtual" in route.params && telaAtual != route.params.telaAtual) {
        route.params.telaAtual = null;
        setTelaAtual(route.params.telaAtual);
    }

    carregaCurrentUser().then((newCurrentUser) => {
        setCurrentTerritorio(newCurrentUser.currentTerritorio);
    }).catch((error) => {
        console.log('erro ao carregar currentUser', error);
    });

    const alertTerritorioStatusIdChanged = () => {
        const status = currentTerritorio.statusId;

        const {title, body, urlReferencia} = alertStatusId(status);

        const btnEntendi = {
            text: 'Entendi'
        };

        const btnUrlReferencia = {
            text: 'Ir ao site', onPress: async () => {
                openUrlInBrowser(urlReferencia);
            }
        };

        const botoes = (urlReferencia)
            ? [btnUrlReferencia, btnEntendi]
            : [btnEntendi];

        Alert.alert(
            title,
            body,
            botoes,
            { cancelable: true }
        );
    };

    /**
     * @method      CustomDrawerContent
     * @param       {Object}            props
     * @constructor
     */
    function CustomDrawerContent(props) {

        const {signOut} = React.useContext(AuthContext);

        /*DEBUG*/ //console.log('tonomapa: MainScreen');

        return (
            <DrawerContentScrollView {...props}>
                <View style={Layout.drawerHeader}>
                    <Image
                        source={require('../assets/images/logo/main/tonomapa_logo.png')}
                        style={Layout.drawerLogoImage}
                        resizeMode="contain"
                    />
                    {currentTerritorio &&
                        <Text style={Layout.drawerHeaderLabel}>{currentTerritorio.nome}</Text>
                    }
                    <IconButton
                        icon="swap-horizontal-bold"
                        color={colors.primary}
                        onPress={() => {
                            props.navigation.navigate('Configuracoes', {atualizarDados: true, editaComunidades: true})}
                        }
                    />
                </View>

                <DrawerItem
                    label="Configurações"
                    labelStyle={Layout.drawerLabel}
                    icon={() => (
                        <MaterialCommunityIcons
                            name="cog"
                            style={Layout.drawerIcon}
                        />
                    )}
                    focused={telaAtual == TELAS.CONFIGURACOES}
                    onPress={() => { props.navigation.navigate('Configuracoes', {atualizarDados: true}); }}
                />

                <DrawerItem
                    label="Sobre o Tô no mapa"
                    labelStyle={Layout.drawerItemSobre}
                    icon={() => (
                        <MaterialCommunityIcons
                            name="information"
                            style={Layout.drawerIcon}
                        />
                    )}
                    focused={telaAtual == TELAS.SOBRE}
                    onPress={() => { props.navigation.jumpTo('Sobre'); }}
                />

                <DrawerItem
                    label="TICCAs: Gestão"
                    labelStyle={Layout.drawerLabel}
                    icon={() => (
                        <MaterialCommunityIcons
                            name="home-group"
                            style={Layout.drawerIcon}
                        />
                    )}
                    focused={telaAtual == TELAS.SOBRE}
                    onPress={() => {
                        props.navigation.jumpTo('TICCAs');
                    }}
                />

                <DrawerItem
                    label={(!isPTTModeVar()) ? "Dados da comunidade" : "Dados PTT"}
                    labelStyle={(isCurrentTerritorioEditableVar()) ? Layout.drawerLabel : Layout.drawerLabelDisabled}
                    icon={() => (
                        <MaterialCommunityIcons
                            name="tooltip-account"
                            style={isCurrentTerritorioEditableVar() ? Layout.drawerIcon : Layout.drawerIconDisabled}
                        />
                    )}
                    focused={[TELAS.DADOS_BASICOS, TELAS.Dados_PTT].includes(telaAtual)}
                    onPress={() => {
                        if (isCurrentTerritorioEditableVar()) {
                            props.navigation.navigate((!isPTTModeVar()) ? 'DadosBasicos' : 'DadosPTT', { atualizarDados: true });
                            return;
                        }

                        if (isPTTModeVar()) {
                            Alert.alert(
                                'Dados já encaminhados para a PTT',
                                'Você já fez o pré-cadastro da comunidade na Plataforma de Territórios Tradicionais do Conselho Nacional de Povos e Comunidades Tradicionais do Ministério Público Federal. Para seguir o cadastramento, acesse https://territoriostradicionais.mpf.mp.br',
                                [
                                    {
                                        text: 'ok', onPress: async () => {
                                        }
                                    },
                                ],
                                {cancelable: true},
                            );
                            return;
                        }

                        alertTerritorioStatusIdChanged();
                    }}
                />

                {!isPTTModeVar() &&
                    <>
                        <DrawerItem
                        label="Mapear o território"
                        labelStyle={(!isPTTModeVar() && isCurrentTerritorioEditableVar()) ? Layout.drawerLabel : Layout.drawerLabelDisabled}
                        icon={() => (
                            <MaterialCommunityIcons
                            name="map"
                            style={(!isPTTModeVar() && isCurrentTerritorioEditableVar()) ? Layout.drawerIcon : Layout.drawerIconDisabled}
                            />
                        )}
                        focused={telaAtual == TELAS.MAPEAR_TERRITORIO}
                        onPress={() => {
                            if (!isPTTModeVar() && isCurrentTerritorioEditableVar()) {
                                props.navigation.navigate('Home', {tela: TELAS.MAPEAR_TERRITORIO});
                                return;
                            }

                            alertTerritorioStatusIdChanged();
                        }}
                        />

                        <DrawerItem
                        label="Usos e conflitos"
                        labelStyle={(!isPTTModeVar() && isCurrentTerritorioEditableVar()) ? Layout.drawerLabel : Layout.drawerLabelDisabled}
                        icon={() => (
                            <MaterialCommunityIcons
                            name="map-marker"
                            style={(!isPTTModeVar() && isCurrentTerritorioEditableVar()) ? Layout.drawerIcon : Layout.drawerIconDisabled}
                            />
                        )}
                        focused={telaAtual == TELAS.MAPEAR_USOSECONFLITOS}
                        onPress={() => {
                            if (!isPTTModeVar() && isCurrentTerritorioEditableVar()) {
                                props.navigation.navigate('Home', {tela: TELAS.MAPEAR_USOSECONFLITOS});
                                return;
                            }

                            alertTerritorioStatusIdChanged();
                        }}/>
                    </>
                }

                <DrawerItem
                    label={!isPTTModeVar() ? "Arquivos e imagens" : "Anexos PTT"}
                    labelStyle={Layout.drawerLabel}
                    icon={() => (
                        <MaterialCommunityIcons
                            name="file"
                            style={Layout.drawerIcon}
                        />
                    )}
                    focused={telaAtual == TELAS.ARQUIVOS}
                    onPress={() => { props.navigation.navigate('AnexoList', {atualizarDados: true}); }}
                />

                { (isEnviarPTTEnabledVar()) ? <DrawerItem
                    label="Enviar para a PTT!"
                    style={(isEnviarEnabledVar() || isOKTonomapa()) ? Layout.drawerEnviar : Layout.drawerItem}
                    labelStyle={(isEnviarEnabledVar() || isOKTonomapa()) ? Layout.drawerEnviarLabel : Layout.drawerLabelDisabled}
                    icon={() => (
                        <MaterialCommunityIcons
                            name="upload"
                            style={(isEnviarEnabledVar() || isOKTonomapa()) ? Layout.drawerEnviarIcon : Layout.drawerIconDisabled}
                        />
                    )}
                    focused={telaAtual == TELAS.ENVIAR}
                    onPress={() => {
                        if (isEnviarEnabledVar()) {
                            props.navigation.navigate('Home', { tela: TELAS.ENVIAR });
                            return;
                        }

                        Alert.alert(
                            'Cadastrar comunidade na PTT',
                            'Graças a uma parceria do Tô no Mapa com o Conselho Nacional de Povos e Comunidades Tradicionais do MPF (CNCTC/MPF) e da Rede PCT, você pode cadastrar a comunidade na Plataforma de Territórios Tradicionais (PTT).\n\nE isso por aqui no aplicativo mesmo! Basta aceitar e fornecer dados adicionais e arquivos específicos!\n\nQuer fazer este cadastro na Plataforma de Territórios Tradicionais (PTT)?',
                            [
                                {
                                    text: 'Não', onPress: async () => {
                                    }
                                },
                                {
                                    text: 'Sim, iniciar cadastro', onPress: async () => {
                                        persisteCurrentTerritorioStatusId(STATUS.EM_PREENCHIMENTO_PTT).then((res) => {
                                            props.navigation.navigate('Home', { tela: TELAS.MAPA, atualizarDados: true, snackBarMessage: 'Modo cadastro PTT' });
                                        });
                                    }
                                },
                            ],
                            {cancelable: true},
                        );
                    }}
                />
                : <DrawerItem label="Enviar ao Tô no mapa!"
                    style={(isEnviarEnabledVar()) ? Layout.drawerEnviar : Layout.drawerItem}
                    labelStyle={(isEnviarEnabledVar()) ? Layout.drawerEnviarLabel : Layout.drawerLabelDisabled}
                    icon={() => (
                        <MaterialCommunityIcons
                            name="upload"
                            style={(isEnviarEnabledVar()) ? Layout.drawerEnviarIcon : Layout.drawerIconDisabled} /
                        >
                    )}
                    focused={telaAtual == TELAS.ENVIAR}
                    onPress={() => {
                        props.navigation.navigate('Home', { tela: TELAS.ENVIAR });
                    }}
                />
                }

                <DrawerItem
                    label="Compartilhar relatório"
                    style={Layout.drawerItem}
                    labelStyle={Layout.drawerLabel}
                    icon={() => (
                        <MaterialCommunityIcons
                            name="share"
                            style={Layout.drawerIcon}
                        />
                    )}
                    onPress={() => {
                        props.navigation.navigate('Home', {tela: TELAS.MAPA, ajustaMapaEExportaPDF: true});
                    }}
                />

                {!isPTTModeVar() &&
                    <DrawerItem
                    label="Mensagens"
                    labelStyle={Layout.drawerLabel}
                    icon={() => (
                        <MaterialCommunityIcons
                        name="message"
                        style={Layout.drawerIcon}
                        />
                    )}
                    focused={telaAtual == TELAS.MENSAGENS}
                    onPress={() => {
                        if (currentTerritorio) {
                            if (currentTerritorio.id) {
                                props.navigation.navigate('Mensagens', {atualizarDados: true});
                                return;
                            }

                            Alert.alert(
                                'Território ainda não cadastrado no Tô no Mapa',
                                'Você só poderá receber e enviar mensagens à equipe Tô no Mapa após ter enviado os dados pela primeira vez para o Tô no Mapa.',
                                [
                                    {
                                        text: 'Enviar Dados', onPress: async () => {
                                            props.navigation.navigate('Home', {tela: TELAS.ENVIAR});
                                        }
                                    },
                                    {
                                        text: 'Ok', onPress: async () => {
                                        }
                                    },
                                ],
                                {cancelable: true},
                            );
                        }
                    }}
                    />
                }

                <DrawerItem
                    label="Início"
                    activeTintColor="#FFF"
                    labelStyle={Layout.drawerLabel}
                    icon={() => (
                        <MaterialCommunityIcons
                            name="home"
                            style={Layout.drawerIcon}
                        />
                    )}
                    focused={telaAtual == TELAS.MAPA}
                    onPress={() => {
                        props.navigation.navigate('Home', {tela: TELAS.MAPA});
                    }}
                />

                {Constants.manifest.extra.testing &&
                    <>
                        <DrawerItem
                            label="Testes: ver tour inicial"
                            activeTintColor="#FFF"
                            labelStyle={Layout.drawerLabelDisabled}
                            icon={() => (
                                <MaterialCommunityIcons
                                    name="ship-wheel"
                                    style={Layout.drawerIconDisabled}
                                />
                            )}
                            focused={telaAtual == TELAS.MAPA}
                            onPress={() => {
                                props.navigation.navigate('Home', {tela: TELAS.MAPA, appIntro: true});
                            }}
                        />

                        <DrawerItem
                            label="Testes: sair"
                            activeTintColor="#FFF"
                            labelStyle={{...Layout.drawerLabel, color:"#999"}}
                            icon={() => (<MaterialCommunityIcons name="logout"
                            style={{...Layout.drawerIcon, color:"#999"}}/>)}
                            onPress={() => {signOut()}}
                        />
                    </>
                }
            </DrawerContentScrollView>
        );
    }

    return (
        <RightDrawer.Navigator
            drawerStyle={Layout.drawer}
            initialRouteName="Home"
            drawerPosition="right"
            edgeWidth={0}
            drawerContent={(props) => <CustomDrawerContent {...props} />}
        >
            <RightDrawer.Screen name="Configuracoes" component={ConfiguracoesScreen}/>
            <RightDrawer.Screen name="Sobre" component={SobreScreen}/>
            <RightDrawer.Screen name="TICCAs" component={TICCAsScreen}/>
            <RightDrawer.Screen name="TermosDeUso" component={TermosDeUsoScreen}/>
            <RightDrawer.Screen name="DadosBasicos" component={DadosBasicosScreen}/>
            <RightDrawer.Screen name="DadosPTT" component={DadosPTTScreen}/>
            <RightDrawer.Screen name="Mapa" component={MapaScreen}/>
            <RightDrawer.Screen name="UsosEConflitos" component={MapaScreen}/>
            <RightDrawer.Screen name="Mensagens" component={MensagensScreen}/>
            <RightDrawer.Screen name="AnexoList" component={AnexoListScreen}/>
            <RightDrawer.Screen name="AnexoForm" component={AnexoFormScreen}/>
            <RightDrawer.Screen name="Home" component={MapaScreen} initialParams={{tela: TELAS.MAPA, firstRun: true}}/>
        </RightDrawer.Navigator>
    );
}
