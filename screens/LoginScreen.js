import React, {useReducer} from 'react';
import {Platform, Image, StyleSheet, View} from "react-native";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Button, Text, TextInput} from "react-native-paper";
import {Buttons, Inputs, Layout} from "../styles";
import colors from "../assets/colors";
import {AuthContext} from "../db/api";
import {TextInputMask} from 'react-native-masked-text';
import {pickerStyles} from "../styles/pickers";


const childrenCommon = {
    flex: 1,
    alignSelf: "stretch"
};
const pickerSelectStyles = StyleSheet.create(pickerStyles);

const styles = StyleSheet.create({
    bottomButton: {
        marginBottom: 16
    },
    logoImage: {
        marginTop: 70,
        marginBottom: 20,
        height: 198,
        width: 237,
        alignSelf: 'center'
    },
    topView: {
        flexGrow: 1
    },
    bottomView: {}
});

export default function LoginScreen({navigation, route}) {
    const reducer = (state, newState) => {
        return {...state, ...newState};
    };
    const [state, setState] = useReducer(reducer, {
        username: '',
        password: '',
        telefone: ''
    });

    if (route.params && "username" in route.params) {
        const newState = {
            username: route.params.username,
            telefone: route.params.username,
            password: route.params.password
        };
        delete route.params.username;
        delete route.params.password;
        setState(newState);
    }


    const {signIn} = React.useContext(AuthContext);

    const onSubmitForm = () => {
        signIn({username: state.username, password: state.password, navigation});
    };

    return (
        <View style={{...Layout.containerStretched, backgroundColor: colors.primary}}>
            <View style={Layout.body}>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={true} extraScrollHeight={120}>
                    <View style={Layout.bodyFlexEnd}>
                        <Image source={require('../assets/icons/login/icon-login.png')} style={styles.logoImage} resizeMode="contain"/>
                        <TextInput
                            label="Seu telefone"
                            value={state.telefone}
                            underlineColor={colors.text}
                            labelStyle={Inputs.textInputLabel}
                            style={Inputs.textInputLogin}
                            theme={{colors: {primary:colors.text}}}
                            onChangeText={usuaria => {
                                setState({
                                    telefone: usuaria,
                                    username: usuaria.replace(/[^0-9.]/g, '')
                                });
                            }}
                            render={props => (
                                <TextInputMask
                                    {...props}
                                    type={'cel-phone'}
                                    keyboardType="phone-pad"
                                    options={{
                                        maskType: 'BRL',
                                        withDDD: true,
                                        dddMask: '(99) '
                                    }}
                                />
                            )}
                        />
                        <TextInput
                            label="Código (se já tem cadastro)"
                            value={state.password}
                            keyboardType="phone-pad"
                            onChangeText={password => {
                                setState({
                                    password: password
                                });
                            }}
                            underlineColor={colors.text}
                            labelStyle={Inputs.textInputLabel}
                            style={Inputs.textInputLogin}
                            theme={{colors: {primary:colors.text}}}
                            keyboardType={'number-pad'}
                        />
                        <View style={Layout.buttonsBottomWrapper}>
                        <Button
                            mode="contained"
                            style={Layout.buttonCenter}
                            color={colors.background}
                            onPress={onSubmitForm}
                        >
                            Entrar
                        </Button>
                        <Button
                            mode="contained"
                            style={{...Layout.buttonCenter, marginTop: 30}}
                            color={colors.secondary}
                            onPress={() => {
                                navigation.navigate('PreRequisitos')
                            }}
                        >
                            Quero me cadastrar!
                        </Button>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        </View>
    );
}
