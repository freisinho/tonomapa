import React, {useReducer, useState, useRef} from 'react';
import {ActivityIndicator, Image, View, Alert, ScrollView, BackHandler} from "react-native";
import {Button, TextInput, Title, Text, Switch} from "react-native-paper";
import {useFocusEffect} from '@react-navigation/native';
import {Anexos, Inputs, Layout} from "../styles";
import * as DocumentPicker from 'expo-document-picker';
import AppbarTonomapa from "../partials/AppbarTonomapa";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {FontAwesome} from '@expo/vector-icons';
import Constants from 'expo-constants';
import colors from "../assets/colors";
import {pluralize, deepEqual, geraTmpId} from '../utils/utils';
import * as mime from "react-native-mime-types";
import {basename, isImage, isVideo, geraThumbDeVideo, iconeDoMimeType, formataTamanhoArquivo, apagaArquivos, fazAnexoLocalUri, fazNomeAnexo, persisteAnexo} from "../db/arquivos";
import ANEXO, {isAnexoPTT, TIPOS_ANEXO} from "../bases/anexo";
import GenericSelect from "../components/GenericSelect";
import { ANEXO_VALIDADOR } from '../utils/schemas';

export default function AnexoFormScreen({route, navigation}) {
    const maxFileSize = Constants.manifest.extra.maxFileSize;

    const anexosPTTOptions = () => {
        let options = [];
        for (let value in TIPOS_ANEXO) {
            options.push({
                label: TIPOS_ANEXO[value],
                value: value
            });
        }
        return options;
    }
    const initialLocalState = {
        currenUser: null,
        newAnexo: {...ANEXO},
        editing: false,
        file: null,
        fileRemoteUri: null,
        thumb: null,
        currentAnexo: null,
        icone: null,
    };

    const reducer = (state, newState) => {
        return {...state, ...newState};
    };
    const [state, setState] = useReducer(reducer, initialLocalState);

    const [carregando, setCarregando] = useState(false);

    const exitScreen = () => {
        navigation.goBack();
        return true;
    };

    if (route.params.currentAnexo) {
        const newCurrentAnexo = {...route.params.currentAnexo};
        delete route.params.currentAnexo;
        const newState = {
            editing: true,
            currentAnexo: newCurrentAnexo,
            newAnexo: newCurrentAnexo,
            currentUser: route.params.currentUser,
            file: route.params.file,
            fileRemoteUri: route.params.fileRemoteUri,
            thumb: route.params.thumb,
            icone: iconeDoMimeType(newCurrentAnexo.mimetype, 'awesome'),
        };
        setState(newState);
    } else if (route.params.newAnexo) {
        delete route.params.newAnexo;
        const newId = geraTmpId();
        const newState = {
            editing: false,
            newAnexo: {
                ...ANEXO,
                id: newId,
                tipoAnexo: (route.params.isPTTMode) ? TIPOS_ANEXO.FONTE_INFORMACAO : null
            },
            currentUser: route.params.currentUser,
            file: null,
            fileRemoteUri: null,
            thumb: null,
        };
        setState(newState);
    }

    useFocusEffect(
        React.useCallback(() => {
            //Handle Android Back button:
            const onBackPress = () => {
                return exitScreen();
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);

            return () => {
                BackHandler.removeEventListener('hardwareBackPress', onBackPress);
            };
        }, [route])
    );

    const validaDados = async () => {
        let errors = [];
        await ANEXO_VALIDADOR.validate(state.newAnexo, { abortEarly: false }).catch((err) => {
            errors = err.errors
        });

        if (!state.file && !state.thumb) {
            errors.push(['· Nenhum arquivo anexo']);
        }

        if (state.newAnexo.fileSize > maxFileSize) {
            errors.push(['· O arquivo enviado é maior que ' + formataTamanhoArquivo(maxFileSize) + '. Por favor, envie um arquivo menor.']);
        }

        if (errors.length > 0) {
            let message = "Os seguintes dados precisam ser fornecidos:\n\n";
            for (let erro of errors) {
                message += erro + "\n"
            }

            Alert.alert(
                'Dados incompletos',
                message,
                [
                    {
                        text: 'OK', onPress: async () => {
                        }
                    },
                ],
                {cancelable: true},
            );
            return false;
        }
        return true;
    };

    const getAnexo = async () => {
        const result = await DocumentPicker.getDocumentAsync({
            multiple: true,
            copyToCacheDirectory: false
        });

        if (result.type === 'success') {
            setCarregando(true);
            await apagaArquivos([state.file, state.thumb]);
            const res = await persisteAnexo({
                from: result.uri,
                currentUser: state.currentUser,
                filename: result.name
            });

            if (!res || !('destinationUri' in res)) {
                /**
                 * @todo Notificar à usuária sobre a falha! Pode ser celular cheio?
                 */
                console.log('Falha ao salvar o arquivo localmente');
                setCarregando(false);

                Alert.alert(
                    'Operação falhou!',
                    "Não foi possível concluir a operação. Por favor, tente novamente, com este ou outro arquivo. Se o erro persistir, ente por favor em contato com a equipe do Tô no Mapa",
                    [
                        {
                            text: 'Ok', onPress: () => {
                            }
                        }
                    ],
                    {cancelable: true},
                );
                return;
            }

            const mimetype = mime.lookup(result.name);
            let newState = {
                newAnexo: {
                    ...state.newAnexo,
                    mimetype: mimetype,
                    fileSize: result.size,
                    arquivo: null,
                    thumb: null
                },
                file: res.destinationUri,
                thumb: null,
                icone: iconeDoMimeType(mimetype, 'awesome')
            };

            if (isVideo(mimetype)) {
                const newThumbName = fazNomeAnexo(state.currentUser, basename({path: result.name, ext: 'jpg'}));
                const newThumbUri = fazAnexoLocalUri(newThumbName, 'thumb');
                const resThumbDeVideo = await geraThumbDeVideo(res.destinationUri, newThumbUri);
                if (resThumbDeVideo.ok) {
                    newState.thumb = newThumbUri;
                }
            }

            /*DEBUG*/ //console.log({newState});
            setState(newState);

            setCarregando(false);
        } else {
            Alert.alert(
                'Operação falhou!',
                "Não foi possível concluir a operação. Por favor, tente novamente, com este ou outro arquivo. Se o erro persistir, ente por favor em contato com a equipe do Tô no Mapa",
                [
                    {
                        text: 'Ok', onPress: () => {
                        }
                    }
                ],
                {cancelable: true},
            );
        }
    };

    const salvarFn = async () => {
        if (!(await validaDados())) {
            console.log('Erro ao validar os dados no salvar anexo', state)
            return;
        }

        if (
            !deepEqual(state.newAnexo, state.currentAnexo) ||
            !('file' in route.params) ||
            state.file != route.params.file
        ) {
            /*DEBUG*/ //console.log(state.newAnexo, state.currentAnexo, state.file);
            // Houve alterações no arquivo:
            navigation.navigate('AnexoList', {
                newAnexo: state.newAnexo,
                file: state.file,
                thumb: state.thumb,
            });
            return;
        }

        // navigation.goBack();
    };

    const apagarFn = () => {
        Alert.alert(
            'Apagar arquivo',
            "Não é possível desfazer esta ação. Deseja mesmo apagar este arquivo ou imagem?",
            [
                {
                    text: 'Não, manter', onPress: () => {
                    }
                },
                {
                    text: 'Sim, apagar', onPress: () => {
                        navigation.navigate('AnexoList', {apagar: state.newAnexo.id});
                    }
                },
            ],
            {cancelable: true},
        );
    };

    let thumbUri = null;
    const ehVideo = isVideo(state.newAnexo.mimetype);
    const ehImagem = isImage(state.newAnexo.mimetype);
    if (ehImagem || ehVideo) {
        thumbUri = (state.thumb) ? state.thumb : state.file;
    }
    const titleObject = (!route.params.isPTTMode)
        ? 'arquivo ou imagem'
        : 'anexo PTT';

    return (
        <React.Fragment>
        {state.newAnexo &&
        <View style={Layout.containerStretched}>
            <AppbarTonomapa navigation={navigation} title={state.editing ? "Editar " + titleObject : "Novo " + titleObject} customGoBack={exitScreen}/>
            <ScrollView>
            <View style={Layout.bodyFlexEnd}>
            <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={false}>
                <TextInput
                    label={!route.params.isPTTMode ? "Nome do arquivo/imagem" : "Nome do anexo PTT"}
                    style={Inputs.textInput}
                    labelStyle={Inputs.textInputLabel}
                    value={state.newAnexo.nome}
                    onChangeText={(value) => {
                        setState({
                            newAnexo: {
                                ...state.newAnexo,
                                nome: value
                            }
                        });
                    }}
                />
                <TextInput
                    label={ isAnexoPTT(state.newAnexo.tipoAnexo) ? "Descrição" : "Descrição (recomendado, mas opcional)"}
                    style={Inputs.textInput}
                    labelStyle={Inputs.textInputLabel}
                    value={state.newAnexo.descricao}
                    onChangeText={(value) => {
                        setState({
                            newAnexo: {
                                ...state.newAnexo,
                                descricao: value
                            }
                        });
                    }}
                    multiline={true}
                />

                <View style={Layout.colLeft}>
                    {(state.file || state.fileRemoteUri) &&
                        <View style={{alignItems: 'flex-start'}}>
                            {thumbUri
                                ? <View style={Anexos.itemImageWrapper}>
                                    <Image source={{uri: thumbUri}} style={Anexos.itemImage}/>
                                    {ehVideo &&
                                        <FontAwesome name="play-circle-o" color={colors.white} size={40} style={Anexos.playIcon}/>
                                    }
                                </View>
                                : <View style={Anexos.formIconWrapper}>
                                        <FontAwesome name={state.icone} size={48} color={colors.text}/>
                                </View>
                            }
                            {state.newAnexo.fileSize &&
                                <Text>Tamanho: {formataTamanhoArquivo(state.newAnexo.fileSize)}</Text>
                            }
                            {!state.file &&
                                <Text>{basename({path: state.fileRemoteUri})}</Text>
                            }
                            <Button mode="contained" style={Layout.buttonLeft} dark={true} color={colors.text} onPress={getAnexo}>
                                Trocar arquivo...
                            </Button>
                        </View>
                    }
                    {(!state.file && !state.fileRemoteUri) &&
                        <View>
                            <Button mode="contained" style={Layout.buttonLeft} dark={true} color={colors.text} onPress={getAnexo}>
                                Arquivo...
                            </Button>
                        </View>
                    }
                </View>

                {
                    isAnexoPTT(state.newAnexo.tipoAnexo) &&
                    <View>
                        <GenericSelect
                            label='Tipo de anexo'
                            items={anexosPTTOptions()}
                            selectedValue={state.newAnexo.tipoAnexo}
                            onValueChange={
                                (value) => setState({
                                    newAnexo: {
                                        ...state.newAnexo,
                                        tipoAnexo: value
                                    }
                                })}
                        />
                    </View>
                }
                {
                    !isAnexoPTT(state.newAnexo.tipoAnexo) &&
                    <View style={Layout.rowAnexoForm}>
                        <Text style={Layout.textLeftFromSwitch}>
                            Este documento ou imagem é ata de reunião da comunidade?
                        </Text>
                        <Switch
                            style={Layout.switchRight}
                            color={colors.primary}
                            value={state.newAnexo.tipoAnexo == 'ata'}
                            onValueChange={() => {
                                setState({
                                    newAnexo: {
                                        ...state.newAnexo,
                                        tipoAnexo: state.newAnexo.tipoAnexo == 'ata' ? null : 'ata'
                                    }
                                });
                            }}
                        />
                    </View>
                }
                <View style={Layout.rowAnexoForm}>
                    <Text style={Layout.textLeftFromSwitch}>
                        Este {!route.params.isPTTMode ? "documento ou imagem" : "anexo"} pode ser disponibilizado publicamente?
                    </Text>
                    <Switch
                        style={Layout.switchRight}
                        color={colors.primary}
                        value={state.newAnexo.publico}
                        onValueChange={() => {
                            setState({
                                newAnexo: {
                                    ...state.newAnexo,
                                    publico: !state.newAnexo.publico
                                }
                            });
                        }}
                    />
                </View>
            </KeyboardAwareScrollView>
            <View style={Layout.buttonsBottomWrapper}>
                <View style={(state.editing) ? {...Layout.row} : Layout.buttonRight}>
                    {(state.editing) &&
                        <Button mode="contained" style={Layout.buttonLeft} color={colors.danger} onPress={apagarFn}>Remover</Button>
                    }
                    <Button mode="contained" dark={true} style={Layout.buttonRight} color={colors.primary} onPress={salvarFn}>Salvar</Button>
                    </View>
                </View>
            </View>
         </ScrollView>
        </View>
        }
        {(!state.newAnexo || carregando) &&
            <View style={Layout.appOverlay}>
                <View style={{backgroundColor: colors.primary, padding: 40, alignItems: 'center', borderRadius: 8}}>
                    <ActivityIndicator size="large" animating={true} color={colors.white} />
                    <Text style={{color: colors.white, marginTop: 30}}>Carregando...</Text>
                </View>
            </View>
        }
        </React.Fragment>
    );
}
