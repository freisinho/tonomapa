import React, { useEffect, useState, useReducer } from 'react';
import { View, Alert, StatusBar } from "react-native";
import { Button, Text, TextInput, Switch } from "react-native-paper";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as DocumentPicker from 'expo-document-picker';
import * as FileSystem from 'expo-file-system';
import XMLParser from 'react-xml-parser';
import AppbarTonomapa from "../partials/AppbarTonomapa";
import MunicipioSelectH from "../components/MunicipioSelectH";
import GenericSelect from "../components/GenericSelect";
import { TextInputMask } from 'react-native-masked-text';
import { kml } from "../maps/togeojson";
import { carregaTudoLocal, persisteCurrentUser } from "../db/api";
import { pluralize, formataNumeroBR } from "../utils/utils";
import { Inputs, Layout } from "../styles";
import colors from "../assets/colors";
import * as yup from 'yup';
import { PTT_TERRITORIO_VALIDADOR, DADOSPTT_SCREEN_VALIDADOR } from '../utils/schemas';
import CURRENT_USER from '../bases/current_user';
import TELAS from "../bases/telas";
import { adjustTerritorioFieldTypes } from "../bases/dadosbasicos";
import tipos from "../bases/tipos";

const zonasLocalizacao = [
    { label: 'RURAL', value: 'RURAL' },
    { label: 'URBANA', value: 'URBANA' },
    { label: 'MISTA', value: 'MISTA' },
    { label: 'OUTRO', value: 'OUTRO' }
];

const getSegmentos = () => {
    return tipos.data.segmentos.map(segmento => ({
        label: segmento.nome,
        value: segmento.id
    }));
};

export default function DadosPTTScreen({ navigation, route }) {
    const reducer = (state, newState) => {
        return {...state, ...newState};
    };
    const initialState = {
        ecEstadoId: null,
        currentUser: null
    };
    const [state, setState] = useReducer(reducer, initialState);

    if (route.params.atualizarDados) {
        delete route.params.atualizarDados;
        carregaTudoLocal().then(({ currentUser }) => {
            setState({
                currentUser: currentUser,
                ecEstadoId: (currentUser.currentTerritorio.ecMunicipioId)
                    ? parseInt(currentUser.currentTerritorio.ecMunicipioId.toString().substring(0,2))
                    : null
            });
        });
    }


    const updateTerritorio = (data) => {
        setState({
            currentUser: {
                ...state.currentUser,
                currentTerritorio: {
                    ...state.currentUser.currentTerritorio,
                    ...data
                }
            }
        });
    };
    const updateUser = (data) => {
        setState({
            ...state.currentUser,
            ...data
        });
    };

    const validarTerritorio = async (schema, mode) => {
        let errors = [];
        await schema.validate(state.currentUser, { abortEarly: false }).catch((err) => {
            errors = err.errors
        });

        if (errors.length > 0) {
            let message = "Os seguintes dados estão incorretos ou não foram fornecidos:\n\n";
            for (let erro of errors) {
                message += erro + "\n"
            }
            mode == 'save' ?
                Alert.alert(
                    'Dados incompletos',
                    message,
                    [
                        {
                            text: 'OK', onPress: async () => {
                            }
                        }
                    ],
                    { cancelable: false },
                )
                :
                Alert.alert(
                    'Dados incompletos',
                    message,
                    [
                        {
                            text: 'FECHAR', onPress: async () => {
                            }
                        },
                        {
                            text: 'SALVAR MESMO ASSIM', onPress: async () => {
                                salvarFn();
                            }
                        }
                    ],
                    { cancelable: false },
                );
            return false;
        }
        return true;
    };


    const salvarFn = async () => {
        if (await validarTerritorio(DADOSPTT_SCREEN_VALIDADOR, 'save')) {
            const newCurrentUser = {
                ...state.currentUser,
                currentTerritorio: adjustTerritorioFieldTypes(state.currentUser.currentTerritorio)
            };
            const res = await persisteCurrentUser(newCurrentUser);
            let params = { tela: TELAS.MAPA };
            if (res.dadosMudaram) {
                params.snackBarMessage = "As informações PTT foram atualizadas!";
                params.atualizarDados = true;
            }

            navigation.navigate('Home', params);
        }
    };

    const validate = async () => {
        if (await validarTerritorio(PTT_TERRITORIO_VALIDADOR)) {
            Alert.alert(
                'Dados corretos',
                'Todos os campos estão corretos!',
                [
                    {
                        text: 'FECHAR', onPress: async () => {
                        }
                    },
                    {
                        text: 'SALVAR', onPress: async () => {
                            salvarFn();
                        }
                    }
                ],
                { cancelable: false },
            );
        }
    };

    return (
        <View style={Layout.containerStretched}>
            <AppbarTonomapa navigation={navigation} title="Dados do PTT" />
            {state.currentUser &&
                <View style={Layout.body}>
                    <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={true} extraScrollHeight={200}>
                        {state.currentUser &&
                            <View style={Layout.innerBody}>
                                <TextInput
                                    editable={false}
                                    label="Nome da comunidade"
                                    style={Inputs.textInput}
                                    labelStyle={Inputs.textInputLabel}
                                    value={state.currentUser.currentTerritorio.nome}
                                />

                                <MunicipioSelectH
                                    disabled={true}
                                    estadoId={(state.currentUser.currentTerritorio.municipioReferenciaId)
                                        ? parseInt(
                                            state.currentUser.currentTerritorio.municipioReferenciaId
                                                .toString()
                                                .substring(0, 2)
                                            )
                                        : null
                                    }
                                    municipioId={(state.currentUser.currentTerritorio.municipioReferenciaId)
                                        ? parseInt(state.currentUser.currentTerritorio.municipioReferenciaId)
                                        : null
                                    }
                                    onValueChange={(value) => {}}
                                />

                                <TextInput
                                    label="Cep da comunidade"
                                    value={state.currentUser.currentTerritorio.cep}
                                    keyboardType='number-pad'
                                    labelStyle={Inputs.textInputLabel}
                                    style={Inputs.textInput}
                                    onChangeText={text => {
                                        updateTerritorio({
                                            cep: text
                                        });
                                    }}
                                    render={props => (
                                        <TextInputMask
                                            {...props}
                                            type={'custom'}
                                            options={{
                                                mask: '99999-999'
                                            }}
                                        />
                                    )}
                                />

                                <TextInput
                                    label="Seu email (para entrar na PTT!)"
                                    keyboardType='email-address'
                                    style={Inputs.textInput}
                                    labelStyle={Inputs.textInputLabel}
                                    value={state.currentUser.email}
                                    onChangeText={(value) => updateUser({ email: value })}
                                />

                                <TextInput
                                    multiline numberOfLines={3}
                                    label="Descrição de acesso"
                                    style={Inputs.textInput}
                                    labelStyle={Inputs.textAreaLabel}
                                    value={state.currentUser.currentTerritorio.descricaoAcesso}
                                    onChangeText={(value) => updateTerritorio({ descricaoAcesso: value })}
                                />

                                <View style={[Layout.rowAnexoForm,{marginHorizontal:10}]}>
                                    <Text style={Layout.textLeftFromSwitch}>
                                        A descrição de acesso pode ser disponibilizada publicamente?
                                    </Text>
                                    <Switch
                                        style={Layout.switchRight}
                                        color={colors.primary}
                                        value={state.currentUser.currentTerritorio.descricaoAcessoPrivacidade}
                                        onValueChange={() => {
                                            updateTerritorio({ descricaoAcessoPrivacidade: !state.currentUser.currentTerritorio.descricaoAcessoPrivacidade });
                                        }}
                                    />
                                </View>

                                <GenericSelect
                                    label='Localização'
                                    items={zonasLocalizacao}
                                    selectedValue={state.currentUser.currentTerritorio.zonaLocalizacao}
                                    onValueChange={(value) => updateTerritorio({ zonaLocalizacao: value })}
                                />

                                {state.currentUser.currentTerritorio.zonaLocalizacao == 'OUTRO' &&
                                    <TextInput
                                        label="Outra localização"
                                        style={Inputs.textInput}
                                        labelStyle={Inputs.textInputLabel}
                                        value={state.currentUser.currentTerritorio.outraZonaLocalizacao}
                                        onChangeText={(value) => updateTerritorio({ outraZonaLocalizacao: value })}
                                    />
                                }

                                <TextInput
                                    label="Auto identificação"
                                    style={Inputs.textInput}
                                    labelStyle={Inputs.textInputLabel}
                                    value={state.currentUser.currentTerritorio.autoIdentificacao}
                                    onChangeText={(value) => updateTerritorio({ autoIdentificacao: value })}
                                />

                                <GenericSelect
                                    label='Tipo de comunidade (segmento)'
                                    items={getSegmentos()}
                                    selectedValue={state.currentUser.currentTerritorio.segmentoId}
                                    onValueChange={(value) => updateTerritorio({ segmentoId: value })}
                                />

                                <TextInput
                                    multiline
                                    numberOfLines={4}
                                    label="Descrição da história"
                                    style={Inputs.textInput}
                                    value={state.currentUser.currentTerritorio.historiaDescricao}
                                    onChangeText={(value) => updateTerritorio({ historiaDescricao: value })}
                                />
                                <TextInput
                                    label="Nome do contato"
                                    style={Inputs.textInput}
                                    labelStyle={Inputs.textInputLabel}
                                    value={state.currentUser.currentTerritorio.ecNomeContato}
                                    onChangeText={(value) => updateTerritorio({ ecNomeContato: value })}
                                />

                                <TextInput
                                    label="Logradouro"
                                    style={Inputs.textInput}
                                    labelStyle={Inputs.textInputLabel}
                                    value={state.currentUser.currentTerritorio.ecLogradouro}
                                    onChangeText={(value) => updateTerritorio({ ecLogradouro: value })}
                                />

                                <TextInput
                                    label="Número"
                                    keyboardType='numeric'
                                    style={Inputs.textInput}
                                    labelStyle={Inputs.textInputLabel}
                                    value={state.currentUser.currentTerritorio.ecNumero}
                                    onChangeText={(value) => updateTerritorio({ ecNumero: value })}
                                />

                                <TextInput
                                    label="Complemento"
                                    style={Inputs.textInput}
                                    labelStyle={Inputs.textInputLabel}
                                    value={state.currentUser.currentTerritorio.ecComplemento}
                                    onChangeText={(value) => updateTerritorio({ ecComplemento: value })}
                                />

                                <TextInput
                                    label="Bairro"
                                    style={Inputs.textInput}
                                    labelStyle={Inputs.textInputLabel}
                                    value={state.currentUser.currentTerritorio.ecBairro}
                                    onChangeText={(value) => updateTerritorio({ ecBairro: value })}
                                />

                                <TextInput
                                    label="Cep do contato"
                                    value={state.currentUser.currentTerritorio.ecCep}
                                    keyboardType='number-pad'
                                    labelStyle={Inputs.textInputLabel}
                                    style={Inputs.textInput}
                                    onChangeText={text => {
                                        updateTerritorio({
                                            ecCep: text
                                        });
                                    }}
                                    render={props => (
                                        <TextInputMask
                                            {...props}
                                            type={'custom'}
                                            options={{
                                                mask: '99999-999'
                                            }}
                                        />
                                    )}
                                />

                                <TextInput
                                    label="Caixa postal"
                                    keyboardType='number-pad'
                                    style={Inputs.textInput}
                                    labelStyle={Inputs.textInputLabel}
                                    value={state.currentUser.currentTerritorio.ecCaixaPostal}
                                    onChangeText={(value) => updateTerritorio({ ecCaixaPostal: value })}
                                />

                                <MunicipioSelectH
                                    labelMunicipio='Município do contato'
                                    labelEstado='Estado do contato'
                                    estadoId={state.ecEstadoId}
                                    municipioId={(state.currentUser.currentTerritorio.ecMunicipioId)
                                        ? parseInt(state.currentUser.currentTerritorio.ecMunicipioId)
                                        : null
                                    }
                                    onValueChange={(value) => {
                                        if (value.hasOwnProperty('estadoId')) {
                                            setState({
                                                ecEstadoId: value.estadoId,
                                                currentUser: {
                                                    ...state.currentUser,
                                                    currentTerritorio: {
                                                        ...state.currentUser.currentTerritorio,
                                                        ecMunicipioId: value.municipioId
                                                    }
                                                }
                                            });
                                            return;
                                        }

                                        updateTerritorio({
                                            ecMunicipioId: value.municipioId
                                        });
                                    }}
                                />

                                <TextInput
                                    label="E-mail do contato"
                                    keyboardType='email-address'
                                    style={Inputs.textInput}
                                    labelStyle={Inputs.textInputLabel}
                                    value={state.currentUser.currentTerritorio.ecEmail}
                                    onChangeText={(value) => updateTerritorio({ ecEmail: value })}
                                />

                                <TextInput
                                    label="Telefone do contato"
                                    value={state.currentUser.currentTerritorio.ecTelefone}
                                    keyboardType='number-pad'
                                    labelStyle={Inputs.textInputLabel}
                                    style={Inputs.textInput}
                                    onChangeText={text => {
                                        updateTerritorio({
                                            ecTelefone: text
                                        });
                                    }}
                                    render={props => (
                                        <TextInputMask
                                            {...props}
                                            type={'custom'}
                                            keyboardType="phone-pad"
                                            options={{
                                                mask: '(99) 99999-9999'
                                            }}
                                        />
                                    )}
                                />

                                <View style={{ ...Layout.rowWrapper, marginTop: 20 }}>
                                    <Button mode="contained" dark={true} onPress={validate}>Validar formulário</Button>

                                    <Button mode="contained" dark={true} onPress={salvarFn}>Salvar</Button>
                                </View>
                            </View>
                        }
                    </KeyboardAwareScrollView>
                </View>
            }
        </View>
    );
}
