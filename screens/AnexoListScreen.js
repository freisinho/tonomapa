import React, {useReducer, useState, useRef} from 'react';
import {TouchableHighlight, Image, Alert, BackHandler, FlatList, View} from "react-native";
import {Snackbar, FAB, ActivityIndicator, Button, Text} from "react-native-paper";
import {FontAwesome} from '@expo/vector-icons';
import AppbarTonomapa from "../partials/AppbarTonomapa";
import {Layout, Anexos} from "../styles";
import colors from "../assets/colors";
import {cloneAppFiles, persisteCurrentTerritorio, persisteSettings, carregaTudoLocal} from "../db/api";
import {pluralize} from "../utils/utils";
import * as FileSystem from 'expo-file-system';
import * as IntentLauncher from 'expo-intent-launcher';
import * as Linking from 'expo-linking';
import {useFocusEffect} from "@react-navigation/native";
import TELAS from "../bases/telas";
import ANEXO, {isAnexoPTT} from "../bases/anexo";
import { isCurrentTerritorioEditable, isPTTMode } from "../bases/status";
import {
    iconeDoMimeType,
    formataTamanhoArquivo,
    fazAnexoLocalUri,
    fazAnexoRemoteUri,
    apagaArquivos,
    isImage,
    isVideo
} from "../db/arquivos";
import {ErroGeral} from "../db/errors";

let snackBarMessage = "";

export default function AnexoListScreen({navigation, route}) {
    const initialState = {
        settings: null,
        currentUser: null,
        carregando: true,
    };
    const reducer = (state, newState) => {
        return {...state, ...newState};
    };
    const [state, setState] = useReducer(reducer, initialState);
    const [selected, setSelected] = React.useState(new Map());
    const [snackBarVisible, setSnackBarVisible] = useState(false);
    const dadosMudaram = useRef(false);
    const isCurrentTerritorioEditableVar = () => {
        return isCurrentTerritorioEditable(state.currentUser);
    };
    const isPTTModeVar = () => {
        return isPTTMode(state.currentUser);
    }

    const exitScreen = () => {
        navigation.navigate('Home', {atualizarDados: dadosMudaram.current});
        return true;
    };

    const apagarFn = (id) => {
        apagaArquivos([state.settings.appFiles.anexos[id].file, state.settings.appFiles.anexos[id].thumb]);

        const gqlAnexos = [...state.currentUser.currentTerritorio.anexos];
        for (let i in gqlAnexos) {
            if (gqlAnexos[i].id == id) {
                gqlAnexos.splice(i, 1);
                break;
            }
        }
        persisteCurrentTerritorio({anexos: gqlAnexos}).then((res) => {
            let newAppFiles = cloneAppFiles(state.settings.appFiles);
            delete newAppFiles.anexos[id];
            persisteSettings({appFiles: newAppFiles}).then((newSettings) => {
                /*DEBUG*/ //console.log(newSettings);
                dadosMudaram.current = true;
                snackBarMessage = "Arquivo apagado com sucesso";
                setSnackBarVisible(true);
                setState({
                    settings: newSettings,
                    currentUser: res.newCurrentUser,
                });
            });
        });
    };

    useFocusEffect(
        React.useCallback(() => {
            const onBackPress = () => {
                return exitScreen();
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);
            return () =>
                BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        }, [route])
    );

    if (route.params.atualizarDados || (state.carregando && !state.currentUser)) {
        if (route.params.atualizarDados) {
            delete route.params.atualizarDados;
            dadosMudaram.current = false;
        }
        carregaTudoLocal().then((data) => {
            setState({
                ...data,
                carregando: false
            });
        });
    }

    //Receiving file from AnexoForm and persisting it
    if (state.currentUser) {
        if (route.params && "newAnexo" in route.params) {
            const newId = route.params.newAnexo.id;
            const newAnexo = {...route.params.newAnexo};
            delete route.params.newAnexo;

            const gqlAnexos = [...state.currentUser.currentTerritorio.anexos] || [];
            let anexoIndex = -1;
            for (let i = 0; i < gqlAnexos.length; i++) {
                if (gqlAnexos[i].id === newId) {
                    anexoIndex = i;
                    break;
                }
            }
            if (anexoIndex > -1) {
                gqlAnexos.splice(anexoIndex, 1, newAnexo);
                snackBarMessage = "Arquivo atualizado com sucesso";
            } else {
                gqlAnexos.push(newAnexo);
                snackBarMessage = "Arquivo adicionado com sucesso";
            }

            /*DEBUG*/ //console.log('Vai persistir tudo');
            persisteCurrentTerritorio({anexos: gqlAnexos}).then((res) => {
                /*DEBUG*/ //console.log(res);
                let newAppFiles = (res.newSettings)
                    ? res.newSettings.appFiles
                    : {...state.settings.appfiles, anexos: {...state.settings.appFiles.anexos}};

                // update appfiles settings to currentterritorio
                //debugger;
                for (let anexo of res.newCurrentUser.currentTerritorio.anexos) {
                    if (anexo.id == newId) {
                        newAppFiles.anexos[anexo.id] = {
                            file: route.params.file,
                            thumb: route.params.thumb,
                        }
                        break;
                    }
                }

                persisteSettings({appFiles: newAppFiles}).then((newSettings) => {
                    /*DEBUG*/ //console.log(newSettings);
                    setSnackBarVisible(true);
                    dadosMudaram.current = true;
                    setState({
                        settings: newSettings,
                        currentUser: res.newCurrentUser,
                    });
                });
            });
        } else if (route.params && "apagar" in route.params) {
            //console.log('will remove');
            const targetId = route.params.apagar;
            apagarFn(targetId);
            delete route.params.apagar;
        }
    }

    const mostraStats = () => {
        let images = 0;
        let docs = 0;
        for (let anexo of state.currentUser.currentTerritorio.anexos) {
            if (
                (isPTTModeVar() && isAnexoPTT(anexo.tipoAnexo)) ||
                (!isPTTModeVar() && !isAnexoPTT(anexo.tipoAnexo))
            ) {
                if (isImage(anexo.mimetype)) {
                    images ++;
                } else {
                    docs ++;
                }
            }
        }
        let stats = '';
        if (docs) {
            stats += docs.toString() + pluralize(docs, ' arquivo', ' arquivos');
            if (images) {
                stats += ' e ';
            }
        }
        if (images) {
            stats += images.toString() + pluralize(images, ' imagem', ' imagens');
        }
        return (
            <View style={{...Layout.statusBar, marginBottom: 8}}>
                <Text style={Layout.statusBarText}>
                    {(stats != '') ? stats : "Não há nenhum arquivo"}
                </Text>
            </View>
        );
    };

    const Item = ({anexo, appFiles, isVisible, isPTTMode, onVisualizar, onEditar, onApagar}) => {
        let thumbUri = null;
        let icone = null;
        const ehVideo = (isVideo(anexo.mimetype));
        if (isImage(anexo.mimetype) && "anexos" in appFiles && anexo.id in appFiles.anexos) {
            thumbUri = (appFiles.anexos[anexo.id].thumb)
                ? appFiles.anexos[anexo.id].thumb
                : appFiles.anexos[anexo.id].file;
        } else if (ehVideo) {
            thumbUri = appFiles.anexos[anexo.id].thumb;
        } else {
            icone = iconeDoMimeType((anexo.mimetype) ? anexo.mimetype : appFiles.anexos[anexo.id].file, 'awesome');
        }
        return (
            <>
            {isVisible &&
                <View style={Anexos.item}>
                    {thumbUri &&
                        <TouchableHighlight onPress={() => {
                            onVisualizar(anexo);
                        }}>
                            <View>
                                <View style={Anexos.itemImageWrapper}>
                                    <Image style={Anexos.itemImage} source={{uri: thumbUri}}/>
                                    {ehVideo &&
                                        <FontAwesome name="play-circle-o" color={colors.white} size={40} style={Anexos.playIcon}/>
                                    }
                                </View>
                                <View>
                                    {isPTTMode
                                        ? <Text style={Anexos.itemTitle}>{anexo.tipoAnexo}: {anexo.nome} - {anexo.publico ? "PÚBLICO" : "NÃO PÚBLICO"}</Text>
                                        : <Text style={Anexos.itemTitle}>{(anexo.tipoAnexo == 'ata') ? "Ata (" + anexo.nome + ")" : anexo.nome} - {anexo.publico ? "PÚBLICO" : "NÃO PÚBLICO"}</Text>
                                    }
                                    {anexo.descricao != null && anexo.descricao != "" &&
                                        <Text style={Anexos.itemDescricao}>{anexo.descricao}</Text>
                                    }
                                </View>
                            </View>
                        </TouchableHighlight>
                    }
                    {!thumbUri &&
                        <TouchableHighlight onPress={() => {
                            onVisualizar(anexo);
                        }}>
                            <View style={Anexos.itemWithoutImageWrapper}>
                                <View style={Anexos.itemIconWrapper}>
                                    <FontAwesome name={icone} color={colors.secondary} size={72}/>
                                </View>
                                <View style={Anexos.itemWithoutImageDetails}>
                                    {isPTTMode
                                        ? <Text style={Anexos.itemTitle}>{anexo.tipoAnexo}: {anexo.nome} - {anexo.publico ? "PÚBLICO" : "NÃO PÚBLICO"}</Text>
                                        : <Text style={Anexos.itemTitle}>{(anexo.tipoAnexo == 'ata') ? "Ata (" + anexo.nome + ")" : anexo.nome} - {anexo.publico ? "PÚBLICO" : "NÃO PÚBLICO"}</Text>
                                    }
                                    {anexo.descricao != null && anexo.descricao != "" &&
                                        <Text style={Anexos.itemDescricao}>{anexo.descricao}</Text>
                                    }
                                    {anexo.fileSize &&
                                        <Text style={Anexos.itemTamanho}>Tamanho: {formataTamanhoArquivo(anexo.fileSize)}</Text>
                                    }
                                </View>
                            </View>
                        </TouchableHighlight>
                    }
                    <View style={Anexos.itemActions}>
                        {onApagar !== null &&
                            <Button mode="contained" dark={true} color={colors.text} onPress={evt => onApagar(anexo.id)}>Remover</Button>
                        }
                        {onEditar !== null &&
                            <Button mode="contained" dark={true} color={colors.text} onPress={evt => onEditar(anexo)}>Editar</Button>
                        }
                    </View>
                </View>
            }
            </>
        );
    };

    const flatListFooter = () => {
        return (
            <View style={{height:40}}/>
        );
    };

    const onSelect = React.useCallback(
        id => {
            const newSelected = new Map(selected);
            newSelected.set(id, !selected.get(id));
            setSelected(newSelected);
        },
        [selected]
    );

    const novoDocumento = () => {
        const routeParams = {
            newAnexo: true,
            currentUser: state.currentUser,
            isPTTMode: isPTTModeVar()
        };
        navigation.navigate('AnexoForm', routeParams);
    }

    const onVisualizarItem = async (anexo) => {
        const appFileAnexo = state.settings.appFiles.anexos[anexo.id];

        let downloadResult = null
        if (!appFileAnexo.file) {
            setState({carregando: true});
            const sourceUri = fazAnexoRemoteUri(anexo.arquivo);
            const destinationUri = fazAnexoLocalUri(anexo.arquivo);
            try {
                downloadResult = await FileSystem.downloadAsync(sourceUri, destinationUri, {});
                /*DEBUG*/ //console.log({sourceUri, destinationUri});
            } catch (err) {
                setState({carregando: false});
                console.log('Não foi possível baixar o arquivo!', err);
                return;
            }

            setState({carregando: false});

            if (Math.floor(downloadResult.status / 100) !== 2) {
                console.log("Não foi possível baixar este arquivo.", downloadResult);
                return;
            }

            let newAppFiles = {
                ...state.settings.appFiles,
                anexos: {
                    ...state.settings.appFiles.anexos
                }
            };
            newAppFiles.anexos[anexo.id].file = downloadResult.uri;
            persisteSettings({appFiles: newAppFiles}).then((newSettings) => {
                /*DEBUG*/ //console.log(newSettings);
                dadosMudaram.current = true;
                snackBarMessage = "Arquivo salvo no seu celular com sucesso";
                setSnackBarVisible(true);
                setState({
                    settings: newSettings,
                });
                FileSystem.getContentUriAsync(downloadResult.uri).then(cUri => {
                    /*DEBUG*/ //console.log(cUri);
                    IntentLauncher.startActivityAsync('android.intent.action.VIEW', {
                        data: cUri.uri,
                        flags: 1,
                    }).then((res) => {
                        console.loc(res);
                    }).catch((err) => {
                        snackBarMessage = "Seu celular não tem nenhum programa para abrir este tipo de arquivo.";
                        setSnackBarVisible(true);
                    });
                }).catch((e) => {console.log(`Não foi possível pegar o conteúdo remoteo. ${e.message}`)});
            });
            //Linking.openURL(downloadResult.uri)
        } else {
            FileSystem.getContentUriAsync(appFileAnexo.file).then(cUri => {
                /*DEBUG*/ //console.log(cUri);
                IntentLauncher.startActivityAsync('android.intent.action.VIEW', {
                    data: cUri.uri,
                    flags: 1,
                });
            });
        }
    };

    const onEditarItem = (anexo) => {
        navigation.navigate('AnexoForm', {
            currentAnexo: anexo,
            file: state.settings.appFiles.anexos[anexo.id].file,
            thumb: state.settings.appFiles.anexos[anexo.id].thumb,
            fileRemoteUri: anexo.arquivo,
            currentUser: state.currentUser
        });
    };

    const onApagarItem = (id) => {
        Alert.alert(
            'Apagar arquivo',
            "Não é possível desfazer esta ação. Deseja mesmo apagar este arquivo ou imagem?",
            [
                {
                    text: 'Não, manter', onPress: () => {
                    }
                },
                {
                    text: 'Sim, apagar', onPress: () => {
                        apagarFn(id);
                    }
                },
            ],
            {cancelable: true},
        );
    }

    return (
        <React.Fragment>
        {(state.currentUser && !state.carregando) &&
            <View style={Layout.containerStretched}>
                <AppbarTonomapa navigation={navigation} title={!isPTTModeVar() ? "Arquivos e imagens" : "Anexos PTT"} customGoBack={exitScreen}/>
                <View>
                    <View style={Layout.innerBody}>
                        {state.currentUser.currentTerritorio.anexos.length == 0 &&
                            <View style={Anexos.introTextWrapper}>
                                <Text style={Anexos.introText}>
                                    Fotos, boletins, relatórios, documentos e outros arquivos anexos com mais informações sobre a comunidade <Text style={Layout.bold}>{state.currentUser.currentTerritorio.nome}</Text>.
                                </Text>
                            </View>
                        }
                        {state.currentUser.currentTerritorio.anexos.length > 0 &&
                            <FlatList
                            data={state.currentUser.currentTerritorio.anexos}
                            style={Anexos.flatList}
                            ItemSeparatorComponent={({leadingItem}) => {
                                if ((isPTTModeVar() && isAnexoPTT(leadingItem.tipoAnexo)) ||
                                    (!isPTTModeVar() && !isAnexoPTT(leadingItem.tipoAnexo))
                                ) {
                                    return (
                                        <View style={Anexos.itemSeparatorComponent} />
                                    );
                                }

                                return null;
                            }}
                            ListHeaderComponent={mostraStats}
                            ListFooterComponent={flatListFooter}
                            stickyHeaderIndices={[0]}
                            renderItem={({item: anexo}) => {
                                return <Item
                                    anexo={anexo}
                                    id={anexo.id}
                                    appFiles={state.settings.appFiles}
                                    isVisible={(isPTTModeVar() && isAnexoPTT(anexo.tipoAnexo)) || (!isPTTModeVar() && !isAnexoPTT(anexo.tipoAnexo))}
                                    isPTTMode={isPTTModeVar()}
                                    onVisualizar={onVisualizarItem}
                                    onEditar={isCurrentTerritorioEditableVar() ? onEditarItem : null}
                                    onApagar={isCurrentTerritorioEditableVar() ? onApagarItem : null}
                                />
                            }}
                            />
                        }
                    </View>
                </View>
                {isCurrentTerritorioEditableVar() &&
                    <FAB
                    style={Anexos.fab}
                    color={colors.white}
                    icon="plus"
                    onPress={novoDocumento}
                    />
                }
                <Snackbar visible={snackBarMessage && snackBarVisible == true} style={{position: 'absolute', bottom: 0}} duration={2000} onDismiss={() => {setSnackBarVisible(false)}}>
                    {snackBarMessage}
                </Snackbar>
            </View>
        }
        {(!state.currentUser || state.carregando) &&
            <View style={Layout.appOverlay}>
                <View style={{backgroundColor: colors.primary, padding: 40, alignItems: 'center', borderRadius: 8}}>
                    <ActivityIndicator size="large" animating={true} color={colors.white} />
                    <Text style={{color: colors.white, marginTop: 30}}>Carregando...</Text>
                </View>
            </View>
        }
        </React.Fragment>
    );

}
