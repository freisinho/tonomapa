import React, {useReducer, useState} from 'react';
import {Platform, Alert, StyleSheet, View, BackHandler} from "react-native";
import {Button, Caption, Headline, Text, TextInput, Title} from "react-native-paper";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Inputs, Layout} from "../styles";
import colors from "../assets/colors";
import AppbarTonomapa from "../partials/AppbarTonomapa";
import TELAS from "../bases/telas";
import {useFocusEffect} from '@react-navigation/native';
import RadioButton from "react-native-paper/src/components/RadioButton/RadioButton";
import TipoConflitoSelect from "../components/TipoConflitoSelect";
import TipoAreaDeUsoSelect from "../components/TipoAreaDeUsoSelect";
import { parseLocaleNumber, toFixedLocale, toCleanLocale } from "../utils/utils";

const styles = StyleSheet.create({
    buttonContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        marginBottom: 20,
        marginTop: 20
    },
});

export default function EditarUsoConflitoScreen({navigation, route}) {
    let initialObjectState = {
        id: null,
        __typename: 'AreaDeUso',
        nome: null,
        posicao: null,
        descricao: null,
        area: null,
        tipoConflitoId: null,
        tipoAreaDeUsoId: null
    };
    if (route.params && "usoOuConflito" in route.params) {
        initialObjectState = {
            ...initialObjectState,
            ...route.params.usoOuConflito,
            area: toFixedLocale(route.params.usoOuConflito.area)
        };
    }

    const LABELS_USO = {
        title: 'Marcar local de uso',
        editTitle: 'Editar local de uso',
        snackBarMessage: "Local de uso adicionado com sucesso!",
        snackBarMessageEdited: "Local de uso alterado com sucesso!",
        snackBarMessageDelete: "Ponto apagado com sucesso!",
        nome: 'Descrição',
        descricao: 'Informações adicionais',
        area: 'Área do local (em metros quadrados)',
        tipo: 'Tipo de uso deste local'
    };

    const LABELS_CONFLITO = {
        title: 'Marcar conflito',
        editTitle: 'Editar conflito',
        snackBarMessage: "Conflito adicionado com sucesso!",
        snackBarMessageEdited: "Conflito alterado com sucesso!",
        snackBarMessageDelete: "Ponto apagado com sucesso!",
        nome: 'Descrição',
        descricao: 'Observações',
        area: '',
        tipo: 'Tipo de conflito'
    };

    const reducer = (state, newState) => {
        return {...state, ...newState};
    };

    const [objectState, setObjectState] = useReducer(reducer, initialObjectState);

    const emEdicao = (objectState.id !== null);
    const labels = (objectState.__typename === 'Conflito')
        ? LABELS_CONFLITO
        : LABELS_USO;
    const title = emEdicao
        ? labels.editTitle
        : labels.title;
    const msg = emEdicao
        ? labels.snackBarMessageEdited
        : labels.snackBarMessage;

    //Android Back button
    useFocusEffect(
        React.useCallback(() => {
            const onBackPress = () => {
                navigation.navigate('Home', {tela: TELAS.MAPEAR_USOSECONFLITOS});
                return true;
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);
            return () =>
                BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        }, [route])
    );

    const validarUsoOuConflito = () => {
        const errors = [];
        if (!objectState.nome || 0 === objectState.nome.trim().length) {
            errors.push(['nome', "· " + labels.nome]);
        } else if (objectState.nome.length > 45) {
            errors.push(['nome', "· " + labels.nome + " está muito grande. Favor reduzir para menos de 45 letras"]);
        }

        const campo = objectState.__typename === 'AreaDeUso' ? 'tipoAreaDeUsoId' : 'tipoConflitoId';
        if (!objectState[campo]) {
            errors.push(['tipo', "· " + labels.tipo]);
        }
        if (errors.length > 0) {
            let message = "Os seguintes dados precisam ser fornecidos:\n\n";
            for (let erro of errors) {
                message += erro[1] + "\n"
            }

            Alert.alert(
                'Dados incompletos',
                message,
                [
                    {
                        text: 'OK', onPress: async () => {
                        }
                    },
                ],
                {cancelable: false},
            );
            return false;
        }

        return true;
    };

    const apagarFn = () => {
        const usoOuConflitoEditado = {
            ...objectState
        };
        let titleAlert = 'Tem certeza que quer apagar ' + ((usoOuConflitoEditado.__typename === 'AreaDeUso') ? 'este local de uso?' : 'este conflito?');
        Alert.alert(
            titleAlert,
            'Você não poderá desfazer esta ação. Tem certeza?',
            [
                {
                    text: 'Não'
                },
                {
                    text: 'Sim, quero apagar',
                    onPress: async () => {
                        if (usoOuConflitoEditado.__typename === 'AreaDeUso') {
                            delete usoOuConflitoEditado.tipoConflitoId;
                        } else {
                            delete usoOuConflitoEditado.tipoAreaDeUsoId;
                        }
                        navigation.navigate('Home', {
                            tela: TELAS.MAPEAR_USOSECONFLITOS,
                            apagarUsoOuConflito: true,
                            usoOuConflitoEditado,
                            snackBarMessage: labels.snackBarMessageDelete
                        });
                    }
                },
            ],
            {cancelable: true},
        );
    };

    const salvarFn = () => {
        if (validarUsoOuConflito()) {
            const usoOuConflitoEditado = {
                ...objectState
            };

            if (usoOuConflitoEditado.__typename === 'AreaDeUso') {
                delete usoOuConflitoEditado.tipoConflitoId;
                if (usoOuConflitoEditado.area !== null) {
                    usoOuConflitoEditado.area = parseLocaleNumber(usoOuConflitoEditado.area);
                    if (usoOuConflitoEditado.area == 0) {
                        usoOuConflitoEditado.area = null;
                    }
                }
            } else {
                delete usoOuConflitoEditado.tipoAreaDeUsoId;
                delete usoOuConflitoEditado.area;
            }

            navigation.navigate('Home', {
                usoOuConflitoEditado,
                tela: TELAS.MAPEAR_USOSECONFLITOS,
                snackBarMessage: msg
            });
        }
    };

    const cancelarFn = () => {
        navigation.navigate('Home');
    };

    return (
        <View style={Layout.containerStretched}>
            <AppbarTonomapa navigation={navigation} title={title} tela={TELAS.MAPEAR_USOSECONFLITOS}/>
            <View style={Layout.body}>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={true} extraScrollHeight={120}>
                    <View style={{...Layout.bodyFlexEnd, paddingTop: 20}}>
                        <RadioButton.Group
                            onValueChange={typename => setObjectState({__typename: typename})}
                            value={objectState.__typename}
                        >
                            <View style={Inputs.checkBoxes}>
                                <RadioButton value="AreaDeUso" />
                                <Text>Local de uso</Text>
                            </View>
                            <View style={Inputs.checkBoxes}>
                                <RadioButton value="Conflito" />
                                <Text>Local de conflito</Text>
                            </View>
                        </RadioButton.Group>

                        { objectState.__typename == 'Conflito' ?
                            <TipoConflitoSelect selectedValue={objectState.tipoConflitoId} onValueChange={(value) => setObjectState({tipoConflitoId: value})}/>
                            :
                            <TipoAreaDeUsoSelect selectedValue={objectState.tipoAreaDeUsoId} onValueChange={(value) => setObjectState({tipoAreaDeUsoId: value})}/>
                        }

                        <TextInput
                            label={labels.nome + " (até 45 letras)"}
                            style={Inputs.textInput}
                            labelStyle={Inputs.textInputLabel}
                            value={objectState.nome}
                            onChangeText={
                                (novoNome) => setObjectState({nome: novoNome})
                            }
                        />

                        <TextInput
                            label={labels.descricao}
                            style={Inputs.textInput}
                            labelStyle={Inputs.textInputLabel}
                            value={objectState.descricao}
                            onChangeText={
                                (novaDescricao) => setObjectState({descricao: novaDescricao})
                            }
                            multiline={true}
                        />

                        { objectState.__typename == 'AreaDeUso' &&
                            <TextInput
                                label={labels.area}
                                style={Inputs.textInput}
                                labelStyle={Inputs.textInputLabel}
                                value={objectState.area}
                                onChangeText={
                                    (novaArea) => setObjectState({
                                        area: toCleanLocale(novaArea)
                                    })
                                }
                                keyboardType='decimal-pad'
                            />
                        }

                        <View style={Layout.buttonsBottomWrapperScroll}>
                            <View style={(emEdicao) ? {...Layout.row} : Layout.buttonRight}>
                                {(emEdicao) &&
                                    <Button mode="contained" style={Layout.buttonLeft} color={colors.danger} onPress={apagarFn}>Apagar</Button>
                                }
                                <Button mode="contained" dark={true} style={Layout.buttonRight} color={colors.primary} onPress={salvarFn}>Salvar</Button>
                            </View>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        </View>
    );
}
