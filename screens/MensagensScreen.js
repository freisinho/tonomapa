import React, {useReducer, useRef} from 'react';
import {Platform, BackHandler, StyleSheet, View, Alert} from "react-native";
import {Button, Text, ActivityIndicator} from "react-native-paper";
import {Inputs, Layout} from "../styles";
import colors from "../assets/colors";
import AppbarTonomapa from "../partials/AppbarTonomapa";
import {carregaTudoLocal} from "../db/api";
import {pluralize, formataNumeroBR} from "../utils/utils";
import { Bubble, GiftedChat } from 'react-native-gifted-chat'
import { pegaMensagem } from '../bases/status';
import 'dayjs/locale/pt-br';

const fazBubble = (props) => {
    return (
        <Bubble
            {...props}
            wrapperStyle={{
                left: {backgroundColor: colors.white},
                right: {backgroundColor: colors.warning},
            }}
            textStyle={{
                left: {color: colors.text},
                right: {color: colors.white},
            }}
        />
    );
}

export default function MensagensScreen({navigation, route}) {
    const initialState = {
        currentUser: null,
        mensagens: null,
        settings: null,
        loading: null
    };

    const reducer = (state, newState) => {
        return {...state, ...newState};
    };
    const [state, setState] = useReducer(reducer, initialState);
    const giftedChatRef = useRef(null);
    const qtdeMensagens = useRef(null);

    if (route.params.atualizarDados) {
        delete route.params.atualizarDados;
        setState({loading: true});
        carregaTudoLocal().then(({currentUser, settings}) => {
            /*DEBUG*/ //console.log(currentUser.currentTerritorio.mensagens);
            let mensagens = [];
            for (let mensagemRaw of currentUser.currentTerritorio.mensagens) {
                const user = (mensagemRaw.originIsDevice)
                    ? {_id: currentUser.id}
                    : {_id: -1, name: 'Equipe Tô no Mapa', avatar: require('../assets/images/logo/colorida/512/logo.png')};
                const createdAt = (mensagemRaw.originIsDevice)
                    ? mensagemRaw.dataEnvio
                    : mensagemRaw.dataRecebimento;
                let text = mensagemRaw.extra.texto;
                if (!mensagemRaw.originIsDevice && mensagemRaw.extra.statusId) {
                    const statusMessage = pegaMensagem(mensagemRaw.extra.statusId);
                    const preText = `${statusMessage.nome}: ${statusMessage.mensagem}`;
                    text = (text)
                        ? `${preText}.\\n\\n${text}`
                        : preText;
                }
                if (text) {
                    mensagens.push({
                        _id: (mensagemRaw.id) ? mensagemRaw.id : new Date().getTime(),
                        text: text.replace(/\\n/g, '\n'),
                        createdAt: createdAt,
                        user: user
                    });
                }
            }

            const loading = (mensagens.length != qtdeMensagens.current);

            if  (loading) {
                const intervalo = (qtdeMensagens.current) ? 200 : 2000;
                qtdeMensagens.current = mensagens.length;
                setTimeout(() => {
                    giftedChatRef.current._messageContainerRef.current.scrollToEnd({animated: false});
                    setState({loading: false});
                }, intervalo);
            }

            setState({
                mensagens: (mensagens.length > 0) ? mensagens : null,
                currentUser: currentUser,
                settings: settings,
                loading: loading
            });
        }).catch(err => {
            setState({loading: false});
            console.log("Usuário não existe na base local!", err);
            // TODO: O que fazer num caso como estes? Tratar. O ideal é efetuar logout...
        });
    }

    /*DEBUG*/ //console.log(state);

    return (
            <View style={Layout.containerStretched}>
                <AppbarTonomapa
                    navigation={navigation}
                    title="Mensagens"
                    rightAction={{
                        icon: 'help-circle',
                        color: colors.secondary,
                        fn: () => {
                            Alert.alert(
                                'Diálogo com a equipe Tô no Mapa',
                                'Aqui estão armazenadas as conversas com a equipe Tô no mapa. Você pode enviar observações na tela "Dados da Comunidade".',
                                [
                                    {
                                        text: 'Entendi'
                                    }
                                ],
                                {cancelable: true},
                            );
                        }
                    }}
                />
                <GiftedChat
                    ref={giftedChatRef}
                    locale='pt-br'
                    messages={state.mensagens ? state.mensagens : []}
                    onSend={messages => { return }}
                    user={{
                        _id: (state.currentUser && state.currentUser.id) ? state.currentUser.id : 0,
                    }}
                    renderInputToolbar={() => { return; }}
                    bottomOffset={0}
                    minInputToolbarHeight={0}
                    scrollToBottom={false}
                    dateFormat='LL'
                    timeFormat='H[h]mm'
                    inverted={false}
                    alignTop={true}
                    renderBubble={props => { return fazBubble(props); }}
                />
                {state.loading &&
                    <View style={Layout.appOverlayOpaque}>
                        <ActivityIndicator size="large" animating={true} color={colors.primary} />
                    </View>
                }
            </View>
    );
}
