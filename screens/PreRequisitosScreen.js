import React from 'react';
import {Image, StyleSheet, View, ScrollView} from "react-native";
import {Button, Paragraph, Surface, Text, Title} from "react-native-paper";
import {Alignments, Layout, Messages} from "../styles";
import {SafeAreaView} from "react-native-safe-area-context";
import TextoSobre from '../partials/TextoSobre';
import AppbarTonomapa from "../partials/AppbarTonomapa";
import colors from '../assets/colors';

export default function PreRequisitosScreen({navigation}) {
    const telefoneNaoCadastrado = false;
    return (
        <View style={Layout.containerCentered}>
            <AppbarTonomapa navigation={navigation} title="Cadastro: o que é o Tô no Mapa?" goBack={true} />
            <View style={Layout.body}>
                <ScrollView keyboardShouldPersistTaps="handled">
                    <SafeAreaView style={Layout.innerBody}>
                        <Image source={require('../assets/images/logo/main/tonomapa_logo.png')} style={Layout.logoImageCadastro}
                                   resizeMode="contain"/>
                        {telefoneNaoCadastrado &&
                            <Button color={colors.warning} style={{...Layout.buttonCenter, marginTop:8, marginBottom:20}} mode="outlined">
                                Seu telefone não está cadastrado
                            </Button>
                        }

                        <TextoSobre />

                        <Button mode="contained" dark={true} style={Layout.buttonCenter} onPress={() => {
                                navigation.navigate('Cadastro');
                            }}>Quero me cadastrar</Button>
                    </SafeAreaView>
                </ScrollView>
            </View>
        </View>
    );
}
