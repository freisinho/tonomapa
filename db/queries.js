import gql from 'graphql-tag';

/* REMOTE QUERIES */

const LISTA_TERRITORIOS_FIELDS = `
    id,
    nome,
    municipioReferenciaId,
`;

export const getListaTerritoriosData = (idUsuaria) => gql`
    {
        territorios(idUsuaria:${idUsuaria}) {
            ${LISTA_TERRITORIOS_FIELDS}
        }
    }
`;

const USUARIA_FIELDS = `
    __typename,
    id,
    username,
    fullName,
    codigoUsoCelular,
    cpf,
    email,
    organizacoes {
        __typename,
        nome
    },
`;

const TERRITORIO_FIELDS = `
    __typename,
    id,
    nome,
    anoFundacao,
    qtdeFamilias,
    statusId,
    tiposComunidade {
        __typename,
        id
    },
    municipioReferenciaId,
    poligono,
    conflitos {
        __typename,
        id,
        nome,
        posicao,
        descricao,
        tipoConflitoId
    },
    areasDeUso {
        __typename,
        id,
        nome,
        posicao,
        descricao,
        area,
        tipoAreaDeUsoId
    },
    anexos {
        __typename,
        id,
        nome,
        descricao,
        tipoAnexo,
        mimetype,
        fileSize,
        arquivo,
        thumb,
        criacao,
        publico,
    },
    mensagens {
        __typename,
        id,
        texto,
        extra,
        dataEnvio,
        dataRecebimento,
        originIsDevice
    },
    publico,
    enviadoPtt,
    cep,
    descricaoAcesso,
    descricaoAcessoPrivacidade,
    zonaLocalizacao,
    outraZonaLocalizacao,
    autoIdentificacao,
    segmentoId,
    historiaDescricao,
    ecNomeContato,
    ecLogradouro,
    ecNumero,
    ecComplemento,
    ecBairro,
    ecCep,
    ecCaixaPostal,
    ecMunicipioId,
    ecEmail,
    ecTelefone
`;

export const getTerritorio = (idTerritorio) => gql`
    {
        currentUser {
            ${USUARIA_FIELDS},
            currentTerritorio(id:${idTerritorio}) {
                ${TERRITORIO_FIELDS}
            }
        }
    }
`;

export const getTerritorioStatusId = (idTerritorio) => gql `
    {
        territorioStatusId(territorioId:${idTerritorio})
    }
`;

export const GET_USER_DATA_QUERY = `
    currentUser {
        ${USUARIA_FIELDS},
        currentTerritorio {
            ${TERRITORIO_FIELDS}
        }
    }
`;

export const GET_USER_DATA = gql`{ ${GET_USER_DATA_QUERY} }`;

export const SEND_USER_DATA = gql`
    mutation updateAppData($currentUser: UsuariaInput){
        updateAppData (input: {currentUser: $currentUser}) {
            token,
            errors,
            success,
            ${GET_USER_DATA_QUERY}
        }
    }
`;

const GET_SETTINGS_QUERY = `
    settings {
        appIntro,
        useGPS,
        mapType,
        editaPoligonos,
        appFiles,
        editing,
        territorios,
        unidadeArea
    }
`;
export const GET_SETTINGS = gql`{ ${GET_SETTINGS_QUERY} }`;
