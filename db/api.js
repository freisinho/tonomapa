import React from 'react';
import { ApolloClient, ApolloLink } from 'apollo-boost'
import { InMemoryCache } from 'apollo-cache-inmemory';
import { onError } from 'apollo-link-error'
import { HttpLink } from 'apollo-link-http';
import { RetryLink } from 'apollo-link-retry';
import { AsyncStorage } from 'react-native';
import { setContext } from "apollo-link-context";
import { persistCache } from 'apollo-cache-persist';
import {
    getTerritorio,
    getListaTerritoriosData,
    getTerritorioStatusId,
    SEND_USER_DATA,
    GET_USER_DATA,
    GET_SETTINGS,
} from "./queries";
import {
    baixaThumbs,
    basename,
    renameAnexoToPermanent,
    limpaTodosAnexos,
    fetchRetry
} from "./arquivos";
import CURRENT_USER, { CURRENT_USER_DADOS_PTT } from "../bases/current_user";
import { DADOS_BASICOS_TERRITORIO_TONOMAPA } from "../bases/dadosbasicos";
import SETTINGS from "../bases/settings";
import colors from "../assets/colors";
import STATUS, { isPTTMode, isCurrentTerritorioEditable } from "../bases/status";
import { isAnexoPTT } from "../bases/anexo";
import { formataArea } from '../maps/mapsUtils';
import { pluralize, deepEqual, ehDadoLocal } from '../utils/utils';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import { PTT_TERRITORIO_VALIDADOR } from '../utils/schemas';

const resetApiErrors = () => {
    apiErrors = {
        graphQLErrors: [],
        networkError: ""
    };
};

let apiErrors;
resetApiErrors();

/**
 * Registro dos erros graphql
 */
const errorLink = onError(({operation, graphQLErrors, networkError}) => {
    if (graphQLErrors)
        graphQLErrors.map((error) => {
            // const msg = `[Erro no GraphQL]: Message: ${error.message}, Location: ${error.locations}, Path: ${error.path}`;
            const msg = `[Erro no GraphQL]: ${error.message}`;
            apiErrors.graphQLErrors.push(msg);
            console.log(error);
        });

    if (networkError) {
        apiErrors.networkError = networkError;
        console.log(`[Falha de conexão]: ${networkError}`);
    }
});

/** @type {String} */
export const DASHBOARD_ENDPOINT = Constants.manifest.extra.dashboardEndpoint;
/** @type {String} */
export const GRAPHQL_ENDPOINT = `${DASHBOARD_ENDPOINT}/graphql`;
/** @type {String} */
export const TOKEN_AUTH_ENDPOINT = `${DASHBOARD_ENDPOINT}/api-token-auth/`;
/** @type {String} */
export const ANEXO_UPLOAD_ENDPOINT = `${DASHBOARD_ENDPOINT}/anexos/`;

/** @type {Object} */
export const LOGIN_ERROR = {
    USER_NOT_FOUND: "USER_NOT_FOUND",
    NO_USER_AND_NO_PASSWORD: "NO_USER_AND_NO_PASSWORD",
    NO_PASSWORD: "NO_PASSWORD",
    MISFORMATTED_USER: "MISFORMATTED_USER",
    NETWORK: "NETWORK"
};


/** @type {Number} */
export const BRASIL_CENTRO_LATITUDE = -10.20;
/** @type {Number} */
export const BRASIL_CENTRO_LONGITUDE = -53.12;

/** @type {Number} */
let INITIAL_LATITUDE = BRASIL_CENTRO_LATITUDE;
/** @type {Number} */
let INITIAL_LONGITUDE = BRASIL_CENTRO_LONGITUDE;

/** @type {InMemoryCache} */
export const tonomapaCache = new InMemoryCache({
    addTypename : false
});

/** @type {HttpLink} */
const httpLink = new HttpLink({
    uri: GRAPHQL_ENDPOINT,
    fetch: (...pl) => {
        if (!Constants.manifest.extra.debugGraphQL) {
            return fetch(...pl);
        }
        const [_, options] = pl;
        const body = JSON.parse(options.body);
        console.log('GRAPHQL LOG', body);
        return fetch(...pl)
    }
});

const retryLink = new RetryLink({
    delay: {
        initial: 5000,
        max: Infinity
    },
    attempts: {
        max: 5,
        retryIf: (error, _operation) => {
            return (
                !!error &&
                typeof error.statusCode == 'undefined' &&
                apiErrors.graphQLErrors.length == 0
            );
        }
    }
});


//---------------------------------------------------------------------------//
// Auth token

/** @type {String} */
let cachedToken;

/**
 * Resgata o token da usuária
 * @method
 * @return {Promise} Token (string)
 */
export const getToken = async () => {
    if (cachedToken) {
        /*DEBUG*/ //console.log("has cachedToken", cachedToken);
        return cachedToken;
    }

    cachedToken = await AsyncStorage.getItem('token');

    return cachedToken;
};

/**
 * Retorna header e credenciais com o token
 * @type {Object}
 */
const withToken = setContext(async ({headers = {}}) => {
    // if you have a cached value, return it immediately
    const token = await getToken();

    const ret = {
        headers: {
            ...headers,
            authorization: `JWT ${token}`
        },
        credentials: 'same-origin'
    };
    /*DEBUG*/ //console.log('withToken', ret);

    return ret;
});

/**
 * Invalida o token, no caso de erro do servidor
 */
const resetTokenOnError = onError(({networkError}) => {
    if (networkError && networkError.name === 'ServerError' && networkError.statusCode === 401) {
        // remove cached token on 401 from the server
        console.log('Token foi invalidado');
        cachedToken = null;
    }
});

/**
 * Cria middlewareLink
 */
const middlewareLink = withToken.concat(resetTokenOnError);

/**
 * Communicate with the server
 * @method
 * @param username String
 * @param password String
 * @param onSuccess Function Função onSuccess(token)
 * @param onError Function Função onError(errorCode, message)
 */
export const serverLogin = (username, password, onSuccess, onError) => {
    let data = new FormData();
    data.append('username', username);
    data.append('password', password);

    fetch(TOKEN_AUTH_ENDPOINT, {
        method: 'POST',
        body: data,
    })
        .then(res => {
            res.json().then(res => {
                /*DEBUG*/ //console.log("login ok!");
                /*DEBUG*/ //console.log(res);
                if (res.token) {
                    onSuccess(res.token);
                } else {
                    let title, message, errorCode;
                    if (username=="") {
                        errorCode = LOGIN_ERROR.NO_USER_AND_NO_PASSWORD;
                    } else if (username.length != 11) {
                        errorCode = LOGIN_ERROR.MISFORMATTED_USER;
                    } else if (password=="") {
                        errorCode = LOGIN_ERROR.NO_PASSWORD;
                    } else {
                        errorCode = LOGIN_ERROR.USER_NOT_FOUND;
                    }
                    console.log(errorCode);
                    onError(errorCode);
                }
            })
        })
        .catch(err => {
            console.log(`login error ${LOGIN_ERROR.NETWORK}: ${err.message}`);
            onError(LOGIN_ERROR.NETWORK, "Não foi possível conectar ao servidor. Favor verificar sua conexão com a internet e tentar novamente.");
        })
};

//---------------------------------------------------------------------------//

const responseLogger = new ApolloLink((operation, forward) => {
    return forward(operation).map(result => {
        console.info(operation.getContext().response.headers)
        return result
    })
});

const logLink = new ApolloLink((operation, forward) => {
    console.info('request', operation.getContext());
    return forward(operation).map((result) => {
        // console.info('response', operation.getContext());
        return result;
    });
});

/**
 * Cliente graphQL. Parâmetros:
 * link
 * cache
 * connectToDevTools
 * onError
 * @type {ApolloClient}
 */
const client = new ApolloClient({
    link: ApolloLink.from([retryLink, errorLink, middlewareLink, httpLink]),
    cache: tonomapaCache,
    connectToDevTools: true
});

/**
 * Contexto para login/logout/signin/signout
 * @type {React.Context}
 */
export const AuthContext = React.createContext();

/** @type {Object} */
const initialData = {};

tonomapaCache.writeData({data: initialData});

persistCache({
    cache: tonomapaCache,
    storage: AsyncStorage
}).then(() => {
    client.onResetStore(async () => tonomapaCache.writeData({data: initialData}));
});

/**
 * Desconecta do sistema, apaga os arquivos locais e manda pra tela inicial
 * @method
 */
export const sair = () => {
    AsyncStorage.removeItem('token');
    client.resetStore();
    deleteCachedToken();
};

/**
 * Fornece informações a respeito da situação em que está o preenchimento do território
 * @method
 * @param  {Object} params {currentUser, markers, poligonos}
 * @return {Object}        {Array situacaoArray, Integer statusId}
 */
export const validaDados = async (params) => {
    /*DEBUG*/ //console.log('entrou validaDados');
    /*DEBUG*/ //console.log(params);
    const DADOS_BASICOS_NAO_PREENCHIDOS = 0;
    const DADOS_BASICOS_INCOMPLETOS = 1;
    const DADOS_BASICOS_COMPLETOS = 2;

    const {currentUser, markers, poligonos} = params;
    const result = [];

    let camposPreenchidos = 0;
    let statusFormularioBasico = DADOS_BASICOS_COMPLETOS;
    let statusNovo = {
        poligonos: null,
        usoOuConflito: null
    };
    let statusId;

    const territorio = currentUser.currentTerritorio;

    if (territorio != null) {
        //ver se poligono está criado
        let temPoligonos = (poligonos.length > 0);
        if (temPoligonos) {
            const area = poligonos.reduce((area, current) => area + current.area, 0);
            result.push({
                icon: 'check',
                text: 'Área definida (' + formataArea(area) + ')',
                iconStyle: {color: "#FFF", backgroundColor: colors.success}
            });
        } else {
            result.push({
                pttError: true,
                icon: 'minus',
                text: 'Nenhuma área definida',
                iconStyle: {color: "#FFF", backgroundColor: colors.danger}
            });
        }
        statusNovo.poligonos = temPoligonos;

        // Dados do PTT estão corretos?
        if (!isPTTMode(currentUser)) {
            // Dados básicos estão completos?
            for (let campo in DADOS_BASICOS_TERRITORIO_TONOMAPA) {
                switch (campo) {
                    case 'tiposComunidade':
                        if (territorio.tiposComunidade.length > 0) {
                            camposPreenchidos++;
                        } else {
                            statusFormularioBasico = DADOS_BASICOS_INCOMPLETOS;
                        }
                        break;
                    default:
                        if (!!territorio[campo]) {
                            camposPreenchidos++;
                        } else {
                            statusFormularioBasico = DADOS_BASICOS_INCOMPLETOS;
                        }
                }
            }
            if (camposPreenchidos === 0) {
                statusFormularioBasico = DADOS_BASICOS_NAO_PREENCHIDOS;
            }

            /*DEBUG*/ //console.log('statusFormularioBasico', statusFormularioBasico);
            switch (statusFormularioBasico) {
                case DADOS_BASICOS_NAO_PREENCHIDOS:
                    result.push({ pttError: true, icon: 'minus', text: 'Dados básicos não preenchidos', iconStyle: {color: "#FFF", backgroundColor: colors.danger}});
                    break;
                case DADOS_BASICOS_INCOMPLETOS:
                    result.push({icon: 'check', text: 'Dados básicos incompletos', iconStyle: {color: "#FFF", backgroundColor: colors.warning}});
                    break;
                case DADOS_BASICOS_COMPLETOS:
                    result.push({icon: 'check', text: 'Dados básicos completos', iconStyle: {color: "#FFF", backgroundColor: colors.success}});
                    break;
            }

            // Registrou locais de uso ou conflitos?
            let temUsoOuConflito = (markers.length > 0);
            result.push(temUsoOuConflito
                ? {
                    icon: 'check',
                    text: markers.length.toString() + pluralize(markers.length, " local de uso ou conflito", " locais de uso e conflitos"),
                    iconStyle: {color: "#FFF", backgroundColor: colors.success}
                }
                : {
                    icon: 'minus',
                    text: "Nenhum local de uso ou de conflito",
                    iconStyle: {color: "#FFF", backgroundColor: colors.warning}
                });
            statusNovo.usoOuConflito = temUsoOuConflito;

            // Anexou imagens/documentos?
            let temAnexos = territorio.anexos.filter(item => !isAnexoPTT(item.tipoAnexo));
            result.push(temAnexos.length > 0
                ? {
                    icon: 'check',
                    text: temAnexos.length.toString() + pluralize(temAnexos.length, " arquivo ou imagem", " arquivos e imagens"),
                    iconStyle: {color: "#FFF", backgroundColor: colors.success}
                }
                : {
                    icon: 'minus',
                    text: "Nenhuma imagem ou documento",
                    iconStyle: {color: "#FFF", backgroundColor: colors.warning}
                });
            statusNovo.anexos = (temAnexos.length > 0);

            // Anexou ata?
            statusNovo.temAta = false;
            if (temAnexos.length > 0) {
                let qtdeAtaDocs = 0;
                for (let anexo of territorio.anexos) {
                    if (anexo.tipoAnexo && anexo.tipoAnexo == 'ata') {
                        qtdeAtaDocs ++;
                    }
                }
                result.push((qtdeAtaDocs > 0)
                    ? {
                        icon: 'check',
                        text: `Ata anexada (${qtdeAtaDocs} ` + pluralize(qtdeAtaDocs, "arquivo", " arquivos") + ")",
                        iconStyle: {color: "#FFF", backgroundColor: colors.success}
                    }
                    : {
                        icon: 'minus',
                        text: "Não há ata anexada",
                        iconStyle: {color: "#FFF", backgroundColor: colors.warning}
                    });
                    statusNovo.temAta = (qtdeAtaDocs > 0);
            }
        } else {
            const isValid = await PTT_TERRITORIO_VALIDADOR.isValid(currentUser);

            result.push(isValid
                ? { icon: 'check', text: 'Dados obrigatórios preenchidos', iconStyle: { color: "#FFF", backgroundColor: colors.success } }
                : { pttError: true, icon: 'minus', text: 'Dados PTT incompletos', iconStyle: { color: "#FFF", backgroundColor: colors.danger }}
            );

            result.push(territorio.anexos.some(item => isAnexoPTT(item.tipoAnexo))
                ? { icon: 'check', text: 'Anexo(s) PTT inserido(s)', iconStyle: { color: "#FFF", backgroundColor: colors.success } }
                : { pttError: true,  icon: 'minus', text: 'Anexo(s) PTT não inserido(s)', iconStyle: { color: "#FFF", backgroundColor: colors.danger } }
            )
        }

        return result;
    }
};

/**
 * Define o cache
 * @method
 * @param  {String} token
 */
export const setCachedToken = (token) => {
    cachedToken = token;
};

/**
 * Apaga o token do cache
 * @method
 */
export const deleteCachedToken = () => {
    cachedToken = null;
};

/**
 * Define as coordenadas iniciais
 * @method
 * @param {Number} longitude
 * @param {Number} latitude
 */
export const setInitialCoordinates = (longitude, latitude) => {
    INITIAL_LATITUDE = latitude;
    INITIAL_LONGITUDE = longitude;
    /*DEBUG*/ //console.log("set Initial Coordinates "+ INITIAL_LATITUDE.toString() + " " + INITIAL_LONGITUDE.toString());
};

/**
 * Resgata as coordenadas iniciais
 * @method
 * @return {Array} Para lat/lng
 */
export const getInitialCoordinates = () => {
    /*DEBUG*/ //console.log("get Initial Coordinates "+ INITIAL_LATITUDE.toString() + " " + INITIAL_LONGITUDE.toString());
    return [INITIAL_LONGITUDE, INITIAL_LATITUDE];
};

/**
 * Carrega as configurações locais da usuária
 * @method
 * @return {Promise} Objeto de configurações definido em "../bases/settings"
 */
export const carregaSettings = async () => {
    const resultSettings = await client.query({
        fetchPolicy: 'cache-only',
        query: GET_SETTINGS
    });

    if (resultSettings.data) {
        let newSettings = {
            ...SETTINGS,
            ...resultSettings.data.settings,
        };
        if (resultSettings.data.settings && resultSettings.data.settings.appFiles) {
            newSettings.appFiles = {
                ...SETTINGS.appFiles,
                ...resultSettings.data.settings.appFiles,
            }
        }
        return newSettings;
    } else {
        return {SETTINGS}
    }
};

/**
 * Atualiza as configurações locais da usuária no aparelho
 * @method
 * @param  {Object}  newSettings Pode ser o objeto completo SETTINGS ou apenas um dos seus atributos
 * @return {Promise}             Objeto SETTINGS com as configurações atualizadas
 */
export const persisteSettings = async (newSettings) => {
    /*DEBUG*/ //console.log('entrou persisteSettings', newSettings);
    let gravaSettings = SETTINGS;
    if (newSettings) {
        if (!("settings" in newSettings)) {
            const settings = await carregaSettings();
            gravaSettings = {
                ...settings,
                ...newSettings
            };
        } else {
            gravaSettings = newSettings.settings;
        }
    }
    /*DEBUG*/ //console.log({newSettings, gravaSettings});
    client.writeQuery({
        data: {settings: gravaSettings},
        query: GET_SETTINGS
    });
    return gravaSettings;
};

/**
 * Carrega, do servidor, a lista de comunidades associadas à usuária (ou seja, das quais ela é cadastrante ou revisora)
 * @method
 * @param  {String}  idUsuaria
 * @return {Promise}           Array com a lista de territórios (objetos)
 */
export const carregaListaTerritorios = async (idUsuaria) => {
    if (!idUsuaria) {
        return null;
    }
    let res = await client.query({
        fetchPolicy: 'network-only',
        query: getListaTerritoriosData(idUsuaria)
    });
    if (res.data && res.data.territorios) {
        return res.data.territorios;
    }
    console.log('Erro ao recuperar os territórios da usuária do dashboard', res);
};

/**
 * Pega o índice da mensagem atual
 * @method
 * @param  {object} currentUser A usuária
 * @return {Integer}             Índice da mensagem atual
 */
export const getCurrentMensagemIndex = currentUser => {
    if (!currentUser || !currentUser.currentTerritorio || !currentUser.currentTerritorio.mensagens) {
        return -1;
    }
    let currentMensagem;
    let i = currentUser.currentTerritorio.mensagens.length - 1;
    while (
        i >= 0 &&
        (!currentUser.currentTerritorio.mensagens[i].originIsDevice ||
        currentUser.currentTerritorio.mensagens[i].id)
    ) {
        i --;
    }
    return i;
};

/**
 * Carrega os dados da usuária, do store local, ou do servidor na nuvem
 * @method
 * @param  {Object}  params idTerritorio: Caso se queira apenas mudar o currentTerritorio da usuária
 *                          fetchPolicy: Será da rede ou local (cache-only)?
 * @return {Promise}        Retorna os dados da usuária, estrutura do "bases/current_user.js"
 */
export const carregaCurrentUser = async (params) => {
    /*DEBUG*/ //console.log('entrou carregaCurrentUser', params);
    const query = (params && "idTerritorio" in params)
        ? getTerritorio(params.idTerritorio)
        : GET_USER_DATA;

    const res = await client.query({
        fetchPolicy: (params && "fetchPolicy" in params) ? params.fetchPolicy : 'cache-only',
        query: query
    });
    /*DEBUG*/ //console.log('carregaCurrentUser result', res);
    if (res.data && res.data.currentUser) {
        let newCurrentUser = {
            ...CURRENT_USER,
            ...res.data.currentUser
        };

        if (newCurrentUser.currentTerritorio == null) {
            /*DEBUG*/ //console.log('currentUser had no currentTerritorio. Loading');
            const newTerritorios = await carregaListaTerritorios(newCurrentUser.id);
            if (newTerritorios.length) {
                /*DEBUG*/ //console.log('currentUser loaded currentUser in recursion');
                const ret = await carregaCurrentUser({
                    fetchPolicy: 'network-only',
                    idTerritorio: newTerritorios[0].id
                });
                /*DEBUG*/ //console.log({ret});
                return ret;
            } else {
                newCurrentUser.currentTerritorio = CURRENT_USER.currentTerritorio;
            }
        } else {
            // Guarantee consistency if new attributes were created in app upgrade and data in device is old:
            for (let attr in CURRENT_USER.currentTerritorio) {
                if (!(attr in newCurrentUser.currentTerritorio)) {
                    newCurrentUser.currentTerritorio[attr] = CURRENT_USER.currentTerritorio[attr];
                }
            }
        }



        // Se os dados de usuária estão sendo carregados da rede, é preciso:
        // 1. Atualizar o settings.appFiles
        // 2. Atualizar o settings.territorios
        // 3. Colocar o status como ENVIADO_PENDENTE - provisório: no futuro, o dashboard é que tem que devolver o status.
        // TODO: o status tem que vir do DASHBOARD!!!!!!
        if (params && "fetchPolicy" in params && params.fetchPolicy == 'network-only') {
            salvaCurrentUserNoCache(newCurrentUser);

            const settings = await carregaSettings();
            /*DEBUG*/ //console.log({settings});

            const newAppFiles = await baixaThumbs({currentUser: newCurrentUser, appFiles: settings.appFiles, removeOrphanedFiles: true});
            /*DEBUG*/ //console.log({newAppFiles});

            const newTerritorios = await carregaListaTerritorios(newCurrentUser.id);

            const newSettings = {
                ...settings,
                appFiles: newAppFiles,
                territorios: newTerritorios
            };
            await persisteSettings({settings: newSettings});
            return {newCurrentUser, newSettings};
        }

        return newCurrentUser;
    }
    console.log('carregaCurrentUser Erro', res);
    throw new Error("Nenhum/a usuário/a encontrado/a. Erro: " + JSON.stringify(res));
};

const salvaCurrentUserNoCache = (currentUser) => {
    /*DEBUG*/ //console.log('salvaCurrentUserNoCache', currentUser);

    // Write to local storage
    try {
        client.writeQuery({
            data: {currentUser: currentUser},
            query: GET_USER_DATA
        });
    } catch(e) {
        console.log(e);
    }
}

const comparaESalvaCurrentUser = async (newCurrentUser, currentUser) => {
    /*DEBUG*/ //console.log('entrou comparaESalvaCurrentUser');
    /*DEBUG*/ //console.log({newCurrentUser, currentUser});
    let dadosMudaram = false;
    let newSettings = null;
    if (!deepEqual(newCurrentUser, currentUser)) {
        dadosMudaram = true;

        // If changed locally and is editable, the territory is no longer in sync with Dashboard:
        if (isCurrentTerritorioEditable(newCurrentUser)) {
            newCurrentUser.currentTerritorio.statusId = (isPTTMode(newCurrentUser))
                ? STATUS.EM_PREENCHIMENTO_PTT
                : STATUS.EM_PREENCHIMENTO;
        }

        // Persist!
        salvaCurrentUserNoCache(newCurrentUser);
    }
    const res = {newCurrentUser: newCurrentUser, dadosMudaram: dadosMudaram};
    /*DEBUG*/ //console.log('comparaESalvaCurrentUser', res);
    return res;
};

export const persisteCurrentUser = async (newCurrentUser) => {
    /*DEBUG*/ //console.log('Entrou persisteCurrentUser', newCurrentUser);
    let gravaCurrentUser;
    let currentUser = null;
    try {
        currentUser = await carregaCurrentUser();
        if ('currentUser' in newCurrentUser) {
            gravaCurrentUser = {...newCurrentUser.currentUser};
        } else {
            gravaCurrentUser = {
                ...currentUser,
                ...newCurrentUser,
                currentTerritorio: {
                    ...currentUser.currentTerritorio,
                    ...newCurrentUser.currentTerritorio
                }
            };
        }
    } catch (e) {
         console.log('Erro em persisteCurrentUser', e);
        if ('currentUser' in newCurrentUser) {
            gravaCurrentUser = {...newCurrentUser.currentUser};
        } else {
            return {error: true, msg: "Usuário não existe ainda na base local."};
        }
    }
    const res = await comparaESalvaCurrentUser(gravaCurrentUser, currentUser);
    return res;
};

export const persisteCurrentTerritorio = async (newCurrentTerritorio) => {
    /*DEBUG*/ //console.log('Entrou persisteCurrentTerritorio');
    try {
        const currentUser = await carregaCurrentUser();
        const gravaCurrentUser = {
            ...currentUser,
            currentTerritorio: {
                ...currentUser.currentTerritorio,
                ...newCurrentTerritorio
            }
        };
        const res = await comparaESalvaCurrentUser(gravaCurrentUser, currentUser);
        return res;
    } catch (e) {
        console.log(e);
        return {error: true, msg: "Erro no persisteCurrentTerritorio: Usuário não existe ainda na base local."};
    }
};

export const persisteCurrentTerritorioStatusId = async (newStatusId) => {
    /*DEBUG*/ //console.log('Entrou persisteCurrentTerritorioStatusId', newStatusId);
    newStatusId = parseInt(newStatusId);
    if (!newStatusId) {
        console.log('persisteCurrentTerritorioStatusId: não tem nada aqui, newStatusId é nulo!');
        return {dadosMudaram: false}
    }

    try {
        let currentUser = await carregaCurrentUser();
        /*DEBUG*/ //console.log('Entrou persisteCurrentTerritorioStatusId', currentUser.currentTerritorio.statusId, newStatusId);
        const currentStatusId = parseInt(currentUser.currentTerritorio.statusId);
        if (
            [STATUS.EM_PREENCHIMENTO, STATUS.EM_PREENCHIMENTO_PTT].includes(currentStatusId) ||
            currentStatusId == newStatusId
        ) {
            return {dadosMudaram: false};
        }

        if (newStatusId == STATUS.OK_PTT) {
            const ret = await carregaCurrentUser({ fetchPolicy: 'network-only' });
            currentUser = ret.currentUser;
        }

        const newCurrentUser = {
            ...currentUser,
            currentTerritorio: {
                ...currentUser.currentTerritorio,
                statusId: newStatusId
            }
        };

        salvaCurrentUserNoCache(newCurrentUser);

        return {newStatusId: newStatusId, dadosMudaram: true};
    } catch (e) {
        console.log(e);
        return {error: true, msg: "Erro no persisteCurrentTerritorio: Usuário não existe ainda na base local."};
    }
};

export const carregaTudoLocal = async (params) => {
    /*DEBUG*/ //console.log('carregaTudoLocal');
    const currentUser = await carregaCurrentUser();
    const settings = await carregaSettings();
    return {currentUser: currentUser, settings: settings};
};

/**
 * Atualiza o status Id do território atual
 * @method
 * @return {Promise}        Retorna o statusId no servidor
 */
export const updateTerritorioStatusId = async (territorioId) => {
    /*DEBUG*/ //console.log('entrou updateTerritorioStatusId', territorioId);

    if (!territorioId) {
        return {dadosMudaram: false};
    }

    const res = await client.query({
        fetchPolicy: 'network-only',
        query: getTerritorioStatusId(territorioId)
    });

    if (res.data) {
        const resPersisteStatusId = await persisteCurrentTerritorioStatusId(res.data.territorioStatusId);
        /*DEBUG*/ //console.log('updateTerritorioStatusId result', resPersisteStatusId);
        return resPersisteStatusId;
    }

    console.log('updateTerritorioStatusId Erro', res);
    throw new Error("Não foi possível atualizar o status do território. Erro: " + JSON.stringify(res));
};

/*
    Atenção: esta função deve ser alterada conforme muda o objeto currentUser. Ela retorna um clone não referenciado do objeto completo (deepshallowcopy)
*/
export const cloneCurrentUser = (currentUser) => {
    const coordinates = (currentUser.currentTerritorio.poligono && currentUser.currentTerritorio.poligono.coordinates)
        ? currentUser.currentTerritorio.poligono.coordinates.map(obj => ([...obj]))
        : CURRENT_USER.poligono;
    return {
        ...currentUser,
        organizacoes: currentUser.organizacoes.map(obj => ({...obj})),
        currentTerritorio: {
            ...currentUser.currentTerritorio,
            poligono: {
                ...currentUser.currentTerritorio.poligono,
                coordinates: coordinates
            },
            areasDeUso: currentUser.currentTerritorio.areasDeUso.map(obj => ({...obj})),
            conflitos: currentUser.currentTerritorio.conflitos.map(obj => ({...obj})),
            anexos: currentUser.currentTerritorio.anexos.map(obj => ({...obj})),
            mensagens: currentUser.currentTerritorio.mensagens.map(obj => ({...obj})),
            tiposComunidade: currentUser.currentTerritorio.tiposComunidade.map(obj => ({...obj}))
        }
    };
};

/*
    Atenção: esta função deve ser alterada conforme muda o objeto currentUser. Ela retorna um clone não referenciado do objeto completo (deepshallowcopy)
*/
export const cloneAppFiles = (appFiles) => {
    return {
        ...appFiles,
        anexos: {
            ...appFiles.anexos
        }
    };
};

const preparaDadosParaEnviar = async (params) => {
    const currentUser = params.currentUser;
    const isCadastro = params.isCadastro;
    const enviarParaRevisao = params.enviarParaRevisao;
    let newCurrentUser = cloneCurrentUser(currentUser);

    delete newCurrentUser.__typename;
    delete newCurrentUser.codigoUsoCelular;
    delete newCurrentUser.currentTerritorio.__typename;
    for (let conflito of newCurrentUser.currentTerritorio.conflitos) {
        delete conflito.__typename;
        if (ehDadoLocal(conflito)) {
            conflito.id = null;
        }
    }

    for (let areaDeUso of newCurrentUser.currentTerritorio.areasDeUso) {
        delete areaDeUso.__typename;
        if (ehDadoLocal(areaDeUso)) {
            areaDeUso.id = null;
        }
    }

    for (let anexo of newCurrentUser.currentTerritorio.anexos) {
        delete anexo.__typename;
        if (ehDadoLocal(anexo)) {
            anexo.id = null;
        }

        // TODO: REMOVE this part on September 2021. It's only here for compatibility purposes
        if (!("tipoAnexo" in anexo)) {
            anexo.tipoAnexo = null;
        }
    }

    for (let organizacao of newCurrentUser.organizacoes) {
        delete organizacao.__typename;
    }

    if (newCurrentUser.id == null) {
        newCurrentUser.currentTerritorio.mensagens = [];
    } else {
        for (let mensagem of newCurrentUser.currentTerritorio.mensagens) {
            delete mensagem.__typename;
            if (mensagem.originIsDevice && !mensagem.id) {
                mensagem.dataEnvio = new Date();
            }
        }
    }
    delete newCurrentUser.currentTerritorio.mensagens;

    for (let tipoComunidade of newCurrentUser.currentTerritorio.tiposComunidade) {
        delete tipoComunidade.__typename;
    }

    newCurrentUser.pushToken = await AsyncStorage.getItem('pushToken');

    const currentStatusId = newCurrentUser.currentTerritorio.statusId;
    if (isCadastro || (!enviarParaRevisao && !isPTTMode(currentUser))) {
        newCurrentUser.currentTerritorio.statusId = STATUS.EM_PREENCHIMENTO;
    } else {
        newCurrentUser.currentTerritorio.statusId = (isPTTMode(currentUser))
            ? STATUS.ENVIO_PTT_NAFILA
            : STATUS.ENVIADO_PENDENTE;
    }
    return newCurrentUser;
};

const sendAnexo = async (params) => {
    if (!params || !params.anexo) {
        return {ok: false, message: "Não foi enviado um anexo nos parâmetros!"}
    }
    /*DEBUG*/ //console.log('sendAnexo', params);
    var formData = new FormData();

    const anexo = params.anexo;
    if (anexo.id) {
        formData.append('id', parseInt(anexo.id));
    }
    if (params.fileUri) {
        const fileUri = params.fileUri;
        const fileName = basename({path: fileUri});
        formData.append('arquivo', {uri: fileUri, name: fileName, type: anexo.mimetype});
    }
    formData.append('nome', anexo.nome);
    formData.append('descricao', (anexo.descricao) ? anexo.descricao : '');
    formData.append('tipo_anexo', (anexo.tipoAnexo) ? anexo.tipoAnexo : '');
    if (params.territorioId) {
        formData.append('territorio', params.territorioId)
    }
    /*DEBUG*/ //console.log('sendAnexo');
    const token = await getToken();

    let firstRes;

    try {
        firstRes = await fetchRetry(ANEXO_UPLOAD_ENDPOINT, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': 'JWT ' + token
            },
            body: formData
        });
    } catch (e) {
        console.log('resultError', e);
        const errorMsg = `Falhou a internet! Não consegui enviar o anexo "${anexo.nome}". Favor tentar novamente com uma internet melhor.`;
        apiErrors.networkError = errorMsg;
        return {ok: false, msg: errorMsg};
    }

    /*DEBUG*/ //console.log(firstRes);
    if (!firstRes.ok) {
        let errorMsg = `Não foi possível enviar o anexo "${anexo.nome}". Pode ser algum problema com o arquivo ou internet. Status: ${firstRes.status}. Dados: ` + JSON.stringify(formData);
        console.log(firstRes.status);
        apiErrors.graphQLErrors.push(errorMsg);
        return {ok: false, msg: errorMsg};
    }

    // Enviado com sucesso
    const newAnexo = await firstRes.json();
    const msg = `anexo ${anexo.nome} enviado com sucesso.`;
    return {ok: true, newAnexo: newAnexo, message: msg};
};
/**
 * Envia todos os anexos novos
 * @return {Promise<unknown[]>}
 */
const sendNewAnexos = async (currentUser, appFiles, territorioId) => {
    /*DEBUG*/ //console.log('entrou sendNewanexos', appFiles);
    /*DEBUG*/ //console.log({currentUser, appFiles, territorioId});
    let erros = [];
    const clonedCurrentUser = cloneCurrentUser(currentUser);
    let newAnexos = clonedCurrentUser.currentTerritorio.anexos;
    const newAppFiles = cloneAppFiles(appFiles);
    for (let i in newAnexos) {
        const anexo = newAnexos[i];
        /*DEBUG*/ //console.log('Loop dos anexos do currentUser. i = ' + i, anexo);
        const createArquivo = (!anexo.arquivo && appFiles.anexos[anexo.id]);

        let sendAnexoParams = {
            anexo: anexo,
            territorioId: territorioId,
            createArquivo: createArquivo
        };

        if (createArquivo) {
            sendAnexoParams.fileUri = appFiles.anexos[anexo.id].file;
        } else if (!anexo.arquivo) {
            console.log('Envio de anexos: inconsistência de dados no arquivo ' + anexo.id + '. Removido do currentUser.');
            newAnexos.splice(i, 1);
            continue;
        }

        const res = await sendAnexo(sendAnexoParams);
        /*DEBUG*/ //console.log('resposta do sendAnexo', res);

        if (!res.ok) {
            throw new Error(res.message);
        }

        newAnexos[i] = {
            ...anexo,
            id: res.newAnexo.id.toString(),
            arquivo: res.newAnexo.arquivo,
            criacao: res.newAnexo.criacao,
            thumb: (res.newAnexo.thumb) ? res.newAnexo.thumb : null
        };

        /*DEBUG*/ //console.log('newAnexos[i]', newAnexos[i]);
        if (createArquivo) {
            newAppFiles.anexos[res.newAnexo.id] = {...newAppFiles.anexos[anexo.id]};
            delete newAppFiles.anexos[anexo.id];
        }
    }

    return {newAnexos, newAppFiles};
};

/**
 * Renomeia os anexos para o seu nome permanente, já de posse de id de usuária e de território
 * @return appFiles
*/
export const renameAppFiles = async (currentUser, appFiles) => {
    // Se o usuário está mandando dados pela primeira vez, só agora recebemos o id de usuário e de território. Neste caso, os anexos precisam ser renomeados para seu nome definitivo:
    // "idUsuario_idTerritorio_HoraAtualEmMilisegundos.ext"
    /*DEBUG*/ //console.log('renameAppFiles');
    let renamedAppFiles = cloneAppFiles(appFiles);
    for (let anexoId in appFiles.anexos) {
        if (appFiles.anexos[anexoId].file) {
            const {newFileUri, newThumbUri} = await renameAnexoToPermanent(appFiles.anexos[anexoId].file, appFiles.anexos[anexoId].thumb, currentUser);
            renamedAppFiles.anexos[anexoId] = {
                file: newFileUri,
                thumb: newThumbUri
            }
        } else {
            // remove rubish: a local appFiles without file must be some error:
            /*DEBUG*/ //console.log('Removed anexoId = ' + anexoId);
            delete renamedAppFiles.anexos[anexoId];
        }
    }
    /*DEBUG*/ //console.log('renameAppFiles - fim', renamedAppFiles);
    return renamedAppFiles;
};

/**
 * Envia dados (currentUser e anexos) para o servidor
 * @return {Promise<*>}: Promise que resolve com um array: [currentUser, appFiles]
 */
export const enviaParaDashboard = async (params) => {
    /*DEBUG*/ //console.log({enviaParaDashboard: params});

    const currentUser = params.currentUser
    let appFiles = params.appFiles
    const isCadastro = ('isCadastro' in params) ? params.isCadastro : false;
    const enviarParaRevisao = ('enviarParaRevisao' in params) ? params.enviarParaRevisao : false;

    resetApiErrors();

    let ret = {
        currentUser: null,
        settings: null,
        token: '',
        apiErrors: apiErrors
    };

    // Envia os dados do currentUser ao Dashboard (mutation)
    // OBS: Os dados do mutate voltam sem os anexos! Pois o dashboard só alimenta os anexos via envio dos arquivos, um a um (sendNewAnexos).

    /**
     * Dados normalizados para envio ao Dashboard
     * @type {object}
     */
    const userData = await preparaDadosParaEnviar(params);

    /*DEBUG*/ //console.log({anexos: userData.currentTerritorio.anexos, appFiles: appFiles}); return ret;
    /*DEBUG*/ //console.log({currentUser, userData});
    /*DEBUG*/ //console.log({currentStatusId: currentUser.currentTerritorio.statusId, userDataStatusId: userData.currentTerritorio.statusId});
    let result;

    try {
        result = await client.mutate({
            mutation: SEND_USER_DATA,
            variables: {
                currentUser: userData
            }
        });
    } catch(e) {
        console.log(`Erro no mutate: ${e.message}`, {apiErrors});
        return ret;
    }

    let newCurrentUser, newAppFiles, newTerritorios;
    /*DEBUG*/ //console.log('Mutate result:', result);

    if (result.data && result.data.updateAppData && result.data.updateAppData.success) {
        newCurrentUser = cloneCurrentUser(result.data.updateAppData.currentUser);
        /*DEBUG*/ //console.log('Mutate result - newCurrentUser:', newCurrentUser);
        /*DEBUG*/ //console.log('Mutate statusId result - newCurrentUser:', newCurrentUser.currentTerritorio.statusId);

        // Atualização da lista de territórios:
        newTerritorios = await carregaListaTerritorios(newCurrentUser.id);
        /*DEBUG*/ //console.log(newTerritorios);

        // Envio e tratamento dos anexos, se houver:
        if (currentUser.currentTerritorio.anexos.length > 0) {
            // Primeira vez que a usuária está mandando ao Dashboard:
            if (currentUser.id == null || currentUser.currentTerritorio.id == null) {
                appFiles = await renameAppFiles(newCurrentUser, appFiles);
            }

            // Envio dos anexos ao Dashboard:
            let sendNewAnexosRes;
            try {
                sendNewAnexosRes = await sendNewAnexos(currentUser, appFiles, newCurrentUser.currentTerritorio.id);
                /*DEBUG*/ //console.log({sendNewAnexosRes});
            } catch(e) {
                console.log('Erro enviando anexos', e);
                return ret;
            }

            newCurrentUser.currentTerritorio.anexos = sendNewAnexosRes.newAnexos;
            newAppFiles = sendNewAnexosRes.newAppFiles;
        } else {
            newAppFiles = await limpaTodosAnexos();
        }

        // Transação com a internet encerrada.

        /*DEBUG*/ //console.log('Persiste CurrentUser Local', newCurrentUser);
        const resPersisteCurrentUser = await persisteCurrentUser({currentUser: newCurrentUser});
        /*DEBUG*/ //console.log({resPersisteCurrentUser});
        /*DEBUG*/ //console.log({resPersisteCurrentUserStatusId: resPersisteCurrentUser.newCurrentUser.currentTerritorio.statusId});

        /*DEBUG*/ //console.log('Persiste Settings Local');

        // Persiste settings localmente: o appFiles e os territorios
        const resPersisteSettings = await persisteSettings({
            appFiles: newAppFiles,
            territorios: newTerritorios
        });
        /*DEBUG*/ //console.log({currentUser: resPersisteCurrentUser.newCurrentUser, resPersisteSettings});

        return {
            currentUser: resPersisteCurrentUser.newCurrentUser,
            settings: resPersisteSettings,
            token: result.data.updateAppData.token,
            apiErrors: apiErrors
        };

    } else {
        console.log('Erro gravando dados!', result);
        return {erros: "Erro(s): " + result.data.updateAppData.errors.join(', ')};
    }
};

export default client;
