//get the last name of an uri
import * as FileSystem from "expo-file-system";
import Constants from 'expo-constants';
import {formataNumeroBR} from '../utils/utils';
import SETTINGS from '../bases/settings';
import * as VideoThumbnails from 'expo-video-thumbnails';
import uuid from 'react-native-uuid';

/**
 * Returns only the filename, stripped from path
 * @params path, ext, stripExtension
 * @returns {*}
 */
export const basename = (params) => {
    if (!params || !params.path || typeof params.path != 'string') {
        return null;
    }
    const path = params.path;
    let fileName = path.split('/').reverse()[0];
    if (params.ext) {
        let n = path.split('.').reverse()[0].length;
        fileName = fileName.substring(0,fileName.length - n) + params.ext;
    } else if (params.stripExtension) {
        let n = path.split('.').reverse()[0].length + 1;
        fileName = fileName.substring(0,fileName.length - n);
    }
    return fileName;
};

/**
 * Returns only the filename, stripped from path
 * @param path
 * @returns {*}
 */
export const extension = (path) => {
    let file = basename({path: path});
    return file.split('.').reverse()[0].toLowerCase();
};

export const DIR_ANEXOS = FileSystem.documentDirectory + 'anexos/';
export const DIR_ANEXOS_THUMBS = DIR_ANEXOS + 'thumbs/';

/**
 * Cria diretório de anexos
 * @return {Promise<void>}
 */
export const createAnexosDir = async () => {
    let {exists, isDirectory} = await FileSystem.getInfoAsync(DIR_ANEXOS);
    if (exists && !isDirectory) {
        await FileSystem.deleteAsync(DIR_ANEXOS);
        exists = false;
    }
    if (!exists) {
        await FileSystem.makeDirectoryAsync(DIR_ANEXOS_THUMBS, {intermediates: true});
        let res = await FileSystem.getInfoAsync(DIR_ANEXOS);
    }
};
/**
 * Apaga todo o conteúdo de um diretório
 * @param diretorio URI do diretório
 * @returns {Promise<void>}
 */
const limpaDiretorio = async (diretorio) => {
    let dirContents = await FileSystem.readDirectoryAsync(diretorio);
    let deletePromises = [];
    for (let file of dirContents) {
        deletePromises.push(FileSystem.deleteAsync(diretorio + file, {idempotent: true}));
    }
    return Promise.all(deletePromises)
}
/**
 * Apaga arquivo do anexo e respectivo thumb (se houver)
 * @param nome do arquivo
 * @returns {Promise<void>}
 */
export const apagaAnexos = async (anexos) => {
    let deletePromises = [];
    for (let anexo of anexos) {
        if (anexo.arquivo) {
            deletePromises.push(FileSystem.deleteAsync(FileSystem.documentDirectory + anexo.arquivo, {idempotent: true}));
            deletePromises.push(FileSystem.deleteAsync(FileSystem.documentDirectory + 'thumb/' + anexo.arquivo, {idempotent: true}));
        }
    }
    return Promise.all(deletePromises)
};
/**
 * Apaga arquivo do anexo e respectivo thumb (se houver)
 * @param arquivos Array de URIs a apagar
 * @returns void
 */
export const apagaArquivos = async (files) => {
    for (let file of files) {
        if (file && typeof file == 'string') {
            const {exists} = await FileSystem.getInfoAsync(file);
            if (exists) {
                await FileSystem.deleteAsync(file);
            }
        }
    }
};
/**
 * Apaga todos os anexos armazenados localmente
 * @param None
 * @returns appFiles default - vazio
 */
export const limpaTodosAnexos = async () => {
    await limpaDiretorio(FileSystem.documentDirectory);
    await createAnexosDir();
    return SETTINGS.appFiles;
};
/**
 * Persiste anexo localmente
 */
export const persisteAnexo = async (params) => {
    const destinationUri = fazAnexoLocalUri(fazNomeAnexo(params.currentUser, params.filename));
    createAnexosDir();
    try {
        await FileSystem.copyAsync({
            from: params.from,
            to: destinationUri
        });
    } catch(err) {
        console.log('erro persistindo anexo baixado do dashboard', err);
        return {error: true};
    }
    return {destinationUri};
};
/**
 * Baixa os thumbs dos arquivos anexos, se já não estiverem baixados
 * @param currentUser
 * @param appFiles: appFiles atual. Se nulo, todos os anexos locais serão apagados
 * @returns {Promise<{anexos: {}}>} Um objeto appFiles com as uris dos arquivos thumb baixados, no formato para serem colocados nos settings
 */
export const baixaThumbs = async (params) => {
    /*DEBUG*/ //console.log("baixaThumbs: " + JSON.stringify(params));
    if (!('currentUser' in params)) {
        return null;
    }
    const currentUser = params.currentUser;
    const removeOrphanedFiles = params.removeOrphanedFiles;
    let appFiles = params.appFiles;

    if (removeOrphanedFiles && appFiles) {
        let toBeRemoved = [];
        for (let appFileAnexoId in appFiles.anexos) {
            let shouldRemove = true;
            for (let anexo of currentUser.currentTerritorio.anexos) {
                if (anexo.id == appFileAnexoId) {
                    shouldRemove = false;
                    continue;
                }
            }
            if (shouldRemove) {
                /*DEBUG*/ //console.log('Será eliminado um arquivo local', appFiles.anexos[appFileAnexoId]);
                toBeRemoved.push(appFiles.anexos[appFileAnexoId].file);
                toBeRemoved.push(appFiles.anexos[appFileAnexoId].thumb);
            }
        }

        apagaArquivos(toBeRemoved);
    }

    try {
        if (!appFiles) {
            appFiles = await limpaTodosAnexos();
        }
        await createAnexosDir();

        //Download thumbs
        let downloadPromises = [];
        let destinationUris = {};
        let erros = [];
        /*DEBUG*/ //console.log({anexos: currentUser.currentTerritorio.anexos, appFiles: appFiles.anexos});
        for (let anexo of currentUser.currentTerritorio.anexos) {
            if (!(anexo.id in appFiles.anexos)) {
                appFiles.anexos[anexo.id] = {file: null, thumb: null};
            }
            if (anexo.thumb) {
                let destinationUri = fazAnexoLocalUri(anexo.thumb, 'thumb');
                let fileInfo = await FileSystem.getInfoAsync(destinationUri);
                if (!fileInfo.exists) {
                    let sourceUrl = Constants.manifest.extra.dashboardEndpoint + anexo.thumb;
                    downloadPromises.push(FileSystem.downloadAsync(sourceUrl, destinationUri, {}));
                    destinationUris[destinationUri] = anexo.id;
                } else {
                    appFiles.anexos[anexo.id].thumb = destinationUri;
                }
            }
        }

        // Prepare response
        return Promise.all(downloadPromises).then(downloadResults => {
            /*DEBUG*/ //console.log({downloadResults});
            for (let result of downloadResults) {
                if (Math.floor(result.status / 100) !== 2) { //Download failed
                    console.log('Failed download thumb', result);
                    continue;
                }

                const anexoId = destinationUris[result.uri];
                if (anexoId in appFiles.anexos) {
                    appFiles.anexos[anexoId].thumb = result.uri
                } else {
                    console.log('Erro de inconsistência!', {
                        appFiles: appFiles,
                        anexoId: anexoId,
                        currentUser: currentUser,
                    });
                }
            }
            return appFiles;
        });
    } catch (err) {
        console.log("download thumbs error", err);
    }
};

export const iconeDoMimeType = (mimetype, iconSet = 'communityIcons') => {
    let icones = null;
    let iconeDefault = null;

    switch (iconSet) {
        case 'awesome':
            iconeDefault = 'file-o';
            icones = {
                // Media
                'image': 'file-image-o',
                'audio': 'file-audio-o',
                'video': 'file-video-o',
                // Documents
                'application/pdf': 'file-pdf-o',
                'application/msword': 'file-word-o',
                'application/vnd.ms-word': 'file-word-o',
                'application/vnd.oasis.opendocument.text': 'file-word-o',
                'application/vnd.openxmlformats-officedocument.wordprocessingml': 'file-word-o',
                'application/vnd.ms-excel': 'file-excel-o',
                'application/vnd.openxmlformats-officedocument.spreadsheetml': 'file-excel-o',
                'application/vnd.oasis.opendocument.spreadsheet': 'file-excel-o',
                'application/vnd.ms-powerpoint': 'file-powerpoint-o',
                'application/vnd.openxmlformats-officedocument.presentationml': 'file-powerpoint-o',
                'application/vnd.oasis.opendocument.presentation': 'file-powerpoint-o',
                'text/plain': 'file-text-o',
                'text/html': 'file-code-o',
                'xml': 'file-code-o',
                'application/json': 'file-code-o',
                // Archives
                'application/gzip': 'file-archive-o',
                'application/zip': 'file-archive-o',
            };
            break;
        case 'communityIcons':
        default:
            iconeDefault = 'file-outline'; // ou file-question-outline
            icones = {
                'file-pdf-outline': ['pdf'],
                'file-table-outline': ['xls', 'xlsx', 'ods', 'csv'], // ou file-chart-outline
                'file-document-outline': ['html', 'txt', 'odt', 'rtf'],
                'file-word': ['doc', 'docx'],
                'file-powerpoint': ['odp', 'pps', 'ppsx', 'ppt'],
                'folder-zip-outline': ['zip', 'tar', 'gz'],
                'file-image-outline': ['kml', 'shp', 'kmz']
            };
            break;
        }

    if (typeof mimetype !== 'string') {
        return iconeDefault;
    }

    let res = iconeDefault;
    for (let mime in icones) {
        if (mimetype.includes(mime)) {
            res = icones[mime];
            break;
        }
    }
    return res;
};

export const formataTamanhoArquivo = (tamanho) => {
    let un = '';
    let decimais = 0;
    if (tamanho >= (1024*1024)) {
        tamanho = tamanho / (1024*1024);
        un = 'Mb';
        decimais = (Math.abs((Math.round(tamanho) - tamanho)) < 0.1) ? 0 : 1;
    } else if (tamanho >= (1024)) {
        tamanho = tamanho / 1024;
        un = 'Kb';
        decimais = (Math.abs((Math.round(tamanho) - tamanho)) < 0.1) ? 0 : 1;
    }
    return formataNumeroBR(tamanho, decimais) + un;
}

export const fazNomeAnexo = (currentUser, oldFileUri) => {
    if (currentUser.id && currentUser.currentTerritorio.id) {
        const uuidHash = uuid.v4();
        const ext = extension(oldFileUri);
        return `${uuidHash}.${ext}`;
    }

    const nomeAnexo = basename({path: oldFileUri, stripExtension: true}) + '_' + new Date().getTime() + '.' + extension(oldFileUri);
    return nomeAnexo;
};

export const fazAnexoLocalUri = (file, dest = '') => {
    const destDir = (dest == 'thumb') ? DIR_ANEXOS_THUMBS : DIR_ANEXOS;
    return destDir + basename({path: file});
};

export const fazAnexoRemoteUri = (file) => {
    return Constants.manifest.extra.dashboardEndpoint + "/media/" + file;
}

export const renameAnexoToPermanent = async (oldFileUri, oldThumbUri, currentUser) => {
    const newFileName = fazNomeAnexo(currentUser, oldFileUri);

    let newThumbUri = null;
    if (oldThumbUri) {
        const res = await FileSystem.getInfoAsync(oldThumbUri);
        if (res.exists) {
            newThumbUri = DIR_ANEXOS_THUMBS + newFileName;
            await FileSystem.moveAsync({
                from: oldThumbUri,
                to: newThumbUri
            });
        }
    }

    let newFileUri = null;
    const res = await FileSystem.getInfoAsync(oldFileUri);
    if (res.exists) {
        newFileUri = DIR_ANEXOS + newFileName;
        await FileSystem.moveAsync({
            from: oldFileUri,
            to: newFileUri
        });
    }
    return {newFileUri, newThumbUri};
};

export const isImage = (mimetype) => {
    return typeof mimetype === 'string' && mimetype.includes('image');
};

export const isVideo = (mimetype) => {
    return typeof mimetype === 'string' && mimetype.includes('video');
};

export const geraThumbDeVideo = async (videoUri, destinationUri) => {
    try {
      const { uri } = await VideoThumbnails.getThumbnailAsync(
        videoUri,
        {
          time: 15000,
          compress: 0.75
        }
      );
      await FileSystem.moveAsync({
         from: uri,
         to: destinationUri
      });
      return {ok: true};
    } catch (e) {
      console.warn(e);
      return {ok: false, error: {msg: "Não foi possível gerar o thumb", error: e}};
    }
};

const wait = async (delay) => {
    return new Promise((resolve) => setTimeout(resolve, delay));
};

export const fetchRetry = (url, fetchOptions = {}, delay = 5000, tries = 5) => {
    function onError(err){
        let triesLeft = tries - 1;
        console.log(`Falha no envio. Nova tentativa (${triesLeft}).`);
        if(!triesLeft){
            throw new Error(err);
        }
        return wait(delay).then(() => fetchRetry(url, fetchOptions, delay, triesLeft));
    }
    return fetch(url, fetchOptions).catch(onError);
};
