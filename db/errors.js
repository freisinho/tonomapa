export class ErroGeral extends Error {
    constructor(message, data={}) {
        super(message);
        this.data = data;
    }
}
