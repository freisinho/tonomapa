import {formataNumeroBR} from '../utils/utils';
import TELAS from "../bases/telas";

export const UNIDADES_AREA = {
    METRO_QUADRADO: {
        sigla: 'm²',
        siglaMilhar: 'km²',
        nome: 'Metro quadrado',
        razao: 1
    },
    HECTARE: {
        sigla: 'h',
        siglaMilhar: '',
        nome: 'Hectare',
        razao: 1/10000
    }
};

const calculateAreaInSquareMeters = (x1, x2, y1, y2) => {
    let area = (y1 * x2 - x1 * y2) / 2;
    return area;
};

const calculateYSegment = (latitudeRef, latitude, circumference) => {
    return (latitude - latitudeRef) * circumference / 360.0;
};

const calculateXSegment = (longitudeRef, longitude, latitude, circumference) => {
    return (longitude - longitudeRef) * circumference * Math.cos((latitude * (Math.PI / 180))) / 360.0;
};

export const calculaAreaPoligono = (locations, unidadeArea = UNIDADES_AREA.HECTARE, formatada = false) => {
    if (!locations.length) {
      return 0;
    }
    if (locations.length < 3) {
      return 0;
    }
    let radius = 6371000;
    const diameter = radius * 2;
    const circumference = diameter * Math.PI;
    const listY = [];
    const listX = [];
    const listArea = [];
    // calculate segment x and y in degrees for each point

    const latitudeRef = locations[0].latitude;
    const longitudeRef = locations[0].longitude;
    for (let i = 1; i < locations.length; i++) {
      let latitude = locations[i].latitude;
      let longitude = locations[i].longitude;
      listY.push(calculateYSegment(latitudeRef, latitude, circumference));
      listX.push(calculateXSegment(longitudeRef, longitude, latitude, circumference));
    }

    // calculate areas for each triangle segment
    for (let i = 1; i < listX.length; i++) {
      let x1 = listX[i - 1];
      let y1 = listY[i - 1];
      let x2 = listX[i];
      let y2 = listY[i];
      listArea.push(calculateAreaInSquareMeters(x1, x2, y1, y2));
    }

    // sum areas of all triangle segments
    let areasSum = 0;
    listArea.forEach(area => areasSum = areasSum + area)

    // get absolute value of area
    let areaCalc = Math.abs(areasSum) * unidadeArea.razao;// Math.sqrt(areasSum * areasSum);

    return formatada ? formataArea(areaCalc, unidadeArea) : areaCalc;
};

export const formataArea = (areaCalc, unidadeArea = UNIDADES_AREA.HECTARE) => {
    let unidade = unidadeArea.sigla;
    let decimals = 1;
    if (unidadeArea.sigla == UNIDADES_AREA.HECTARE.sigla) {
        decimals = (areaCalc == Math.round(areaCalc)) ? 0 : 1;
    } else {
        if (areaCalc > 1050000000) {
            areaCalc = Math.round(areaCalc/100000000)/10;
            unidade = 'mil km²';
            decimals = (areaCalc == Math.round(areaCalc)) ? 0 : 1;
        } else if (areaCalc > 950000000) {
            areaCalc = '';
            unidade = 'mil km²';
        } else if (areaCalc > 1050000) {
            areaCalc = Math.round(areaCalc/100000)/10;
            unidade = 'km²';
            decimals = (areaCalc == Math.round(areaCalc)) ? 0 : 1;
        } else if (areaCalc > 950000) {
            areaCalc = '';
            unidade = '1 km²';
        } else if (areaCalc > 1050) {
            areaCalc = Math.round(areaCalc/100)/10;
            unidade = 'mil m²';
            decimals = (areaCalc == Math.round(areaCalc)) ? 0 : 1;
        } else if (areaCalc > 950) {
            areaCalc = '';
            unidade = 'mil m²';
        } else {
            areaCalc = Math.round(areaCalc);
            unidade = 'm²';
            decimals = 0;
        }
    }
    areaCalc = areaCalc
        ? '~' + formataNumeroBR(areaCalc, decimals) + ' ' + unidade
        : '~' + unidade;

    return areaCalc;
};

export const getRegionForCoordinates = (coords) => {
    let minX, maxX, minY, maxY;

    ((coord) => {
        minX = coord.latitude;
        maxX = coord.latitude;
        minY = coord.longitude;
        maxY = coord.longitude;
    })(coords[0]);

    // calculate rect
    coords.map((coord) => {
        minX = Math.min(minX, coord.latitude);
        maxX = Math.max(maxX, coord.latitude);
        minY = Math.min(minY, coord.longitude);
        maxY = Math.max(maxY, coord.longitude);
    });

    const midX = (minX + maxX) / 2;
    const midY = (minY + maxY) / 2;
    let deltaX = (maxX - minX);
    let deltaY = (maxY - minY);

    return {
        latitude: midX,
        longitude: midY,
        latitudeDelta: 0.002,
        longitudeDelta: 0.002
    };
};

export const isTelaMapeamento = (tela) => {
    if (!tela) {
        return false;
    }
    return [TELAS.MAPEAR_TERRITORIO, TELAS.MAPEAR_USOSECONFLITOS].includes(tela);
};
